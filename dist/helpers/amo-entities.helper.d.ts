import { AmoBaseEntityV4Model, FindEntitiesQueryParams } from '../contracts/amo-entity.model';
import { AmoCfV4Model } from '../contracts/amo-cf.model';
export declare const filterAmoEntitiesByQuery: <T extends AmoBaseEntityV4Model>(result: T[], { query, filter, forFieldID, forFieldCode }: FindEntitiesQueryParams<T>) => T[];
export declare const filterAmoEntityCFValues: (cf: AmoCfV4Model, query: string) => boolean;
