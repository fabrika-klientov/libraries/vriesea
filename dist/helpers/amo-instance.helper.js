import { MainVueApi } from '../modules/classes/main-vue-api';
import { getAPP, VRIESEA_CORE } from '../configs/core.config';
import { Widget } from '../modules/classes/widget';
export const ENTITY_CODES = {
    LEADS: 'leads',
    CONTACTS: 'contacts',
    COMPANIES: 'companies',
    CUSTOMERS: 'customers',
};
export const ENTITY_NUMBERS = {
    LEADS: 2,
    CONTACTS: 1,
    COMPANIES: 3,
    CUSTOMERS: 12,
};
export const LIVE_KEYS = {
    // main lead
    MAIN_USER: 'MAIN_USER',
    NAME: 'NAME',
    PIPELINE_ID: 'PIPELINE_ID',
    PRICE: 'PRICE',
    PIPELINE_STATUS: 'STATUS',
    // linked contacts || companies
    ELEMENT_TYPE: 'ELEMENT_TYPE',
    ID: 'ID',
    MAIN_ID: 'MAIN_ID',
    MAIN_USER_ID: 'MAIN_USER_ID',
    FN: 'FN',
    LN: 'LN',
};
export const entityCodeToEntityNumber = (code) => {
    switch (code) {
        case ENTITY_CODES.LEADS:
            return ENTITY_NUMBERS.LEADS;
        case ENTITY_CODES.CUSTOMERS:
            return ENTITY_NUMBERS.CUSTOMERS;
        case ENTITY_CODES.CONTACTS:
            return ENTITY_NUMBERS.CONTACTS;
        case ENTITY_CODES.COMPANIES:
            return ENTITY_NUMBERS.COMPANIES;
        default:
            return -1;
    }
};
export const entityNumberToEntityCode = (id) => {
    switch (id) {
        case ENTITY_NUMBERS.LEADS:
            return ENTITY_CODES.LEADS;
        case ENTITY_NUMBERS.CUSTOMERS:
            return ENTITY_CODES.CUSTOMERS;
        case ENTITY_NUMBERS.CONTACTS:
            return ENTITY_CODES.CONTACTS;
        case ENTITY_NUMBERS.COMPANIES:
            return ENTITY_CODES.COMPANIES;
        default:
            return null;
    }
};
export const entityConvert = (amoStr) => {
    switch (amoStr) {
        case 'lcard':
        case 'llist':
            return ENTITY_CODES.LEADS;
        case 'ccard':
        case 'clist':
            return ENTITY_CODES.CONTACTS;
        case 'comcard':
            return ENTITY_CODES.COMPANIES;
        case 'cucard':
        case 'culist':
            return ENTITY_CODES.CUSTOMERS;
        default:
            return null;
    }
};
export const entitySingleConvert = (amoStr) => {
    switch (amoStr) {
        case ENTITY_CODES.LEADS:
        case ENTITY_CODES.CONTACTS:
        case ENTITY_CODES.CUSTOMERS:
            return amoStr.slice(0, -1);
        case ENTITY_CODES.COMPANIES:
            return 'company';
        default:
            return null;
    }
};
export const getEntityName = (code, variant = 0, lang = 'RU') => {
    variant = variant < 0 ? 0 : (variant > 5 ? 5 : variant);
    switch (code) {
        case ENTITY_CODES.LEADS:
            return ['Сделка', 'сделка', 'Сделки', 'сделки', 'СДЕЛКА', 'СДЕЛКИ'][variant];
        case ENTITY_CODES.CUSTOMERS:
            return ['Покупатель', 'покупатель', 'Покупатели', 'покупатели', 'ПОКУПАТЕЛЬ', 'ПОКУПАТЕЛИ'][variant];
        case ENTITY_CODES.CONTACTS:
            return ['Контакт', 'контакт', 'Контакты', 'контакты', 'КОНТАКТ', 'КОНТАКТЫ'][variant];
        case ENTITY_CODES.COMPANIES:
            return ['Компания', 'компания', 'Компании', 'компании', 'КОМПАНИЯ', 'КОМПАНИИ'][variant];
        default:
            return 'Не определен';
    }
};
export const getAmoEntityLink = (entity, id) => {
    return MainVueApi.accountHost + VRIESEA_CORE().amoEntities[entity].detail + id;
};
export const getLiveKey = (model, id) => {
    if (model[id]) {
        return [id];
    }
    let each = (predicate) => {
        let results = [];
        for (let prop in model) {
            if (!model.hasOwnProperty(prop)) {
                continue;
            }
            if (predicate(prop, model[prop])) {
                results.push(prop);
            }
        }
        return results;
    };
    if (typeof id === 'number' || /^\d+$/.test(id)) {
        let key = `CFV[${id}]`;
        if (model[key]) {
            return [key];
        }
        let props = each(prop => prop.indexOf(key) === 0);
        if (props.length) {
            return props;
        }
    }
    return ['lead', 'contact', 'company'].reduce((result, item) => {
        let key = `${item}[${id}]`;
        if (model[key]) {
            result.push(key);
        }
        return result;
    }, []);
};
export const getLiveValue = (model, id) => {
    let keys = getLiveKey(model, id);
    if (!keys.length) {
        return null;
    }
    let control = value => (value && `${value}`.length ? value : null);
    if (keys.length === 1) {
        return control(model[keys[0]]);
    }
    // phones || emails
    if (keys.some(item => /\[VALUE]$/.test(`${item}`)) && keys.some(item => /\[DESCRIPTION]$/.test(`${item}`))) {
        return keys
            .filter(item => /\[VALUE]$/.test(item))
            .map(item => control(model[item]))
            .filter(item => item);
    }
    // add another types of fields
    return keys
        .map(item => control(model[item]))
        .filter(item => item);
};
export const getCardEntityId = () => {
    var _a;
    return +((_a = getAPP().data.current_card) === null || _a === void 0 ? void 0 : _a.id);
};
export const getCardEntityName = (entity) => {
    var _a, _b;
    if (!entity) {
        entity = entityConvert(Widget.context.area);
    }
    return hpCardName(((_b = (_a = getAPP().data.current_card) === null || _a === void 0 ? void 0 : _a.model) === null || _b === void 0 ? void 0 : _b.attributes) || {}, entity);
};
let hpCardName = (attr, entity) => {
    let keySingleEntity = entitySingleConvert(entity);
    switch (keySingleEntity) {
        case 'contact':
            return attr[`${keySingleEntity}[NAME]`] || attr[`${keySingleEntity}[FN]`] || attr[`${keySingleEntity}[LN]`];
        case 'customer':
            return attr.name;
        default:
            return attr[`${keySingleEntity}[NAME]`];
    }
};
export const isCustomersEnabled = () => {
    var _a;
    return ((_a = getAPP().constant('account')) === null || _a === void 0 ? void 0 : _a.customers_enabled) || false;
};
//# sourceMappingURL=amo-instance.helper.js.map