import { entityCodeToEntityNumber } from './amo-instance.helper';
export const CF_TYPES = {
    CF_TEXT: 1,
    CF_NUMERIC: 2,
    CF_CHECKBOX: 3,
    CF_SELECT: 4,
    CF_MULTISELECT: 5,
    CF_DATE: 6,
    CF_URL: 7,
    CF_MULTITEXT: 8,
    CF_TEXTAREA: 9,
    CF_RADIOBUTTON: 10,
    CF_STREETADDRESS: 11,
    CF_SMART_ADDRESS: 13,
    CF_BIRTHDAY: 14,
    CF_LEGAL_ENTITY: 15,
    CF_DATE_TIME: 19,
    CF_PRICE: 20,
};
export const CF_TYPE_NAMES = {
    CF_TEXT: 'text',
    CF_NUMERIC: 'numeric',
    CF_CHECKBOX: 'checkbox',
    CF_SELECT: 'select',
    CF_MULTISELECT: 'multiselect',
    CF_DATE: 'date',
    CF_URL: 'url',
    CF_MULTITEXT: 'multitext',
    CF_TEXTAREA: 'textarea',
    CF_RADIOBUTTON: 'radiobutton',
    CF_STREETADDRESS: 'streetaddress',
    CF_SMART_ADDRESS: 'smart_address',
    CF_BIRTHDAY: 'birthday',
    CF_LEGAL_ENTITY: 'legal_entity',
    CF_DATE_TIME: 'date_time',
    CF_PRICE: 'price',
};
export const CUSTOM_FIELDS_ID = {
    RESPONSIBLE: -1,
    PRICE: -2,
    NEXT_PRICE: -3,
    COMPANY_IN_CONTACT: -4,
};
export const getFrontCfEnumForField = (field, enumData) => {
    let enums = field.ENUMS;
    if (!enums) {
        return null;
    }
    for (let enumId in enums) {
        let item = enums[enumId];
        if (typeof enumData === 'number' || /^\d+$/.test(enumData)) {
            if (item.ID === +enumData) {
                return item;
            }
        }
        if (item.VALUE === enumData) {
            return item;
        }
    }
    return null;
};
export const getFrontCfEnumsForField = (field, enumsData) => {
    return enumsData.reduce((result, item) => {
        let data = getFrontCfEnumForField(field, item);
        if (data) {
            result.push(data);
        }
        return result;
    }, []);
};
export const cfAsResponsible = (entity, id) => {
    let element_type = 0;
    let element_code = entity || '';
    let entityWithId = (entity || '') + '_' + (id || '');
    if (entity) {
        element_type = entityCodeToEntityNumber(entity);
    }
    return {
        id: CUSTOM_FIELDS_ID.RESPONSIBLE,
        name: 'Ответственный',
        code: 'RESPONSIBLE',
        deletable: false,
        sort: 1,
        type_id: CF_TYPES.CF_TEXT,
        description: '',
        disabled: true,
        catalog_id: null,
        deleted_at: null,
        visible: true,
        required: false,
        element_type,
        element_code,
        sortable: false,
        groupable: false,
        entityWithId,
    };
};
export const cfAsPrice = (id) => {
    let entityWithId = 'leads_' + (id || '');
    return {
        id: CUSTOM_FIELDS_ID.PRICE,
        name: 'Бюджет',
        code: 'PRICE',
        deletable: false,
        sort: 1,
        type_id: CF_TYPES.CF_TEXT,
        description: '',
        disabled: true,
        catalog_id: null,
        deleted_at: null,
        visible: true,
        required: false,
        element_type: 2,
        element_code: 'leads',
        sortable: false,
        groupable: false,
        entityWithId,
    };
};
export const cfAsNextPrice = (id) => {
    let entityWithId = 'customers_' + (id || '');
    return {
        id: CUSTOM_FIELDS_ID.NEXT_PRICE,
        name: 'Ожидаемая сумма',
        code: 'NEXT_PRICE',
        deletable: false,
        sort: 1,
        type_id: CF_TYPES.CF_TEXT,
        description: '',
        disabled: true,
        catalog_id: null,
        deleted_at: null,
        visible: true,
        required: false,
        element_type: 12,
        element_code: 'customers',
        sortable: false,
        groupable: false,
        entityWithId,
    };
};
export const cfAsCompanyInContact = (id) => {
    let entityWithId = 'contacts_' + (id || '');
    return {
        id: CUSTOM_FIELDS_ID.COMPANY_IN_CONTACT,
        name: 'Компания',
        code: 'COMPANY_IN_CONTACT',
        deletable: false,
        sort: 1,
        type_id: CF_TYPES.CF_TEXT,
        description: '',
        disabled: true,
        catalog_id: null,
        deleted_at: null,
        visible: true,
        required: false,
        element_type: 1,
        element_code: 'contacts',
        sortable: false,
        groupable: false,
        entityWithId,
    };
};
//# sourceMappingURL=amo-cf.helper.js.map