import { AmoEntity } from '../contracts/amo-instances.types';
export declare const ENTITY_CODES: {
    LEADS: string;
    CONTACTS: string;
    COMPANIES: string;
    CUSTOMERS: string;
};
export declare const ENTITY_NUMBERS: {
    LEADS: number;
    CONTACTS: number;
    COMPANIES: number;
    CUSTOMERS: number;
};
export declare const LIVE_KEYS: {
    MAIN_USER: string;
    NAME: string;
    PIPELINE_ID: string;
    PRICE: string;
    PIPELINE_STATUS: string;
    ELEMENT_TYPE: string;
    ID: string;
    MAIN_ID: string;
    MAIN_USER_ID: string;
    FN: string;
    LN: string;
};
export declare const entityCodeToEntityNumber: (code: AmoEntity) => number;
export declare const entityNumberToEntityCode: (id: number) => AmoEntity | null;
export declare const entityConvert: (amoStr: string) => AmoEntity | null;
export declare const entitySingleConvert: (amoStr: AmoEntity) => string | null;
export declare const getEntityName: (code: AmoEntity | null, variant?: number, lang?: string) => string;
export declare const getAmoEntityLink: (entity: AmoEntity, id: number | string) => string;
export declare const getLiveKey: (model: any, id: number | string) => (string | number)[];
export declare const getLiveValue: (model: any, id: number | string) => any;
export declare const getCardEntityId: () => number;
export declare const getCardEntityName: (entity?: AmoEntity) => string;
export declare const isCustomersEnabled: () => boolean;
