export declare class ErrorHelper {
    defError: string;
    protected replaceMap: Map<string | RegExp, string>;
    constructor(replaceMap?: Map<string | RegExp, string> | {
        [key: string]: string;
    } | [string | RegExp, string][], defError?: string);
    handle(errorData: any, replaceMap?: Map<string | RegExp, string>, ignoreTopReplaceMap?: boolean): string[];
    protected doErrors(errorData: any): string[];
    protected doReplacing(errors: string[], replaceMap?: Map<string | RegExp, string>, ignoreTopReplaceMap?: boolean): string[];
}
