import { Subject } from 'rxjs';
export declare const hideInMenu: () => void;
export declare const hideOnListMultiActions: () => void;
export declare const dpSettingsBox: (code?: string) => any;
export declare const advancedSettingsLink: () => string;
export declare const isExpandedWidgetRight: () => boolean;
export declare const checkAmoListPaginateChange: (countCheckMs?: number, difCheckMs?: number) => Subject<boolean>;
