export class ErrorHelper {
    constructor(replaceMap, defError = 'Неизвестная ошибка') {
        this.defError = defError;
        this.replaceMap = new Map();
        if (!replaceMap) {
            return;
        }
        if (replaceMap instanceof Map) {
            this.replaceMap = replaceMap;
            return;
        }
        switch (typeof replaceMap) {
            case 'object':
                this.replaceMap = new Map(replaceMap.length ? replaceMap : Object.entries(replaceMap));
                return;
        }
    }
    handle(errorData, replaceMap, ignoreTopReplaceMap) {
        if (!errorData) {
            return [this.defError];
        }
        return this.doReplacing(this.doErrors(errorData), replaceMap, ignoreTopReplaceMap);
    }
    doErrors(errorData) {
        let result = [];
        switch (typeof errorData) {
            case 'string':
                result.push(errorData);
                break;
            case 'object':
                if (errorData.length) {
                    result.push(...errorData);
                    break;
                }
                for (let key in errorData) {
                    result.push(...errorData[key]);
                }
        }
        return result;
    }
    doReplacing(errors, replaceMap, ignoreTopReplaceMap) {
        if (!this.replaceMap.size && !(replaceMap === null || replaceMap === void 0 ? void 0 : replaceMap.size)) {
            return errors;
        }
        let localEntries = Array.from((replaceMap === null || replaceMap === void 0 ? void 0 : replaceMap.entries()) || []);
        let topEntries = Array.from(this.replaceMap.entries());
        return errors
            .map(item => {
            if (localEntries.length) {
                return localEntries.reduce((result, [pattern, value]) => result.replace(pattern, value), item);
            }
            return item;
        })
            .map(item => {
            if (!ignoreTopReplaceMap && topEntries.length) {
                return topEntries.reduce((result, [pattern, value]) => result.replace(pattern, value), item);
            }
            return item;
        });
    }
}
//# sourceMappingURL=error.helper.js.map