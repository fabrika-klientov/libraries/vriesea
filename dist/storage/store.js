import { getModule } from 'vuex-module-decorators';
import { modules } from './modules-state';
const options = {
    state: {},
    mutations: {},
    actions: {},
    getters: {},
    modules,
};
let $store;
function initializeStore(storeInstance) {
    $store = storeInstance;
}
function cleanStore() {
    if (!$store) {
        return;
    }
    for (let key in modules) {
        let module = getModule(modules[key], $store);
        if (module === null || module === void 0 ? void 0 : module.fullClean) {
            module.fullClean();
        }
    }
}
export function getVrieseaModule(moduleClass) {
    return getModule(moduleClass, $store);
}
export { $store, options, initializeStore, cleanStore };
//# sourceMappingURL=store.js.map