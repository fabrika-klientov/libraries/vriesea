import { Store, StoreOptions } from 'vuex';
import { VuexModule } from 'vuex-module-decorators';
import { StoreState } from './modules-state';
declare const options: StoreOptions<StoreState>;
declare let $store: Store<any>;
declare function initializeStore(storeInstance: Store<any>): void;
declare function cleanStore(): void;
export declare function getVrieseaModule<M extends VuexModule>(moduleClass: ConstructorOf<M>): M;
export { $store, options, initializeStore, cleanStore };
