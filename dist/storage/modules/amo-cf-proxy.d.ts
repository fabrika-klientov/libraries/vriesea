import { AmoCfGroupEntities, AmoCfModel, FactoryCF } from '../../contracts/amo-cf.model';
import AmoCf from './amo-cf';
export default class AmoCfProxy extends AmoCf {
    loadCF(force?: boolean): Promise<AmoCfGroupEntities>;
    storeCF(factory: FactoryCF): Promise<AmoCfModel[]>;
}
