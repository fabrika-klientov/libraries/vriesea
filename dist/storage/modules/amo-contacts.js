var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { VuexModule, Module, Action } from 'vuex-module-decorators';
import { lastValueFrom, map } from 'rxjs';
import { MainVueApi } from '../../modules/classes/main-vue-api';
import { VRIESEA_CORE as CORE } from '../../configs/core.config';
import { filterAmoEntitiesByQuery } from '../../helpers/amo-entities.helper';
let AmoContacts = class AmoContacts extends VuexModule {
    async findByIdApiV4(id) {
        return lastValueFrom(MainVueApi.context
            .get(MainVueApi.accountHost + CORE().amoPaths.contactsApiV4 + `/${id}`, {}, { 'X-Requested-With': 'XMLHttpRequest' })
            .pipe(map(response => {
            return response || null;
        })));
    }
    async findByIdsApiV4(id) {
        return lastValueFrom(MainVueApi.context
            .get(MainVueApi.accountHost + CORE().amoPaths.contactsApiV4, { id }, { 'X-Requested-With': 'XMLHttpRequest' })
            .pipe(map(response => {
            var _a;
            return ((_a = response === null || response === void 0 ? void 0 : response._embedded) === null || _a === void 0 ? void 0 : _a.contacts) || [];
        })));
    }
    async findByQueryApiV4(data) {
        return lastValueFrom(MainVueApi.context
            .get(MainVueApi.accountHost + CORE().amoPaths.contactsApiV4, { query: data.query }, { 'X-Requested-With': 'XMLHttpRequest' })
            .pipe(map(response => {
            var _a;
            return filterAmoEntitiesByQuery(((_a = response === null || response === void 0 ? void 0 : response._embedded) === null || _a === void 0 ? void 0 : _a.contacts) || [], data);
        })));
    }
};
__decorate([
    Action
], AmoContacts.prototype, "findByIdApiV4", null);
__decorate([
    Action
], AmoContacts.prototype, "findByIdsApiV4", null);
__decorate([
    Action
], AmoContacts.prototype, "findByQueryApiV4", null);
AmoContacts = __decorate([
    Module({
        name: 'AmoContacts',
        namespaced: true,
    })
], AmoContacts);
export default AmoContacts;
//# sourceMappingURL=amo-contacts.js.map