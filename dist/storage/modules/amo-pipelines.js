var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators';
import { catchError, lastValueFrom, map, of } from 'rxjs';
import { MainVueApi } from '../../modules/classes/main-vue-api';
import { VRIESEA_CORE as CORE } from '../../configs/core.config';
let AmoPipelines = class AmoPipelines extends VuexModule {
    constructor() {
        super(...arguments);
        this.$leadPipelines = [];
        this.$customersStatuses = [];
        this.$customersSegments = [];
    }
    setLeadPipelines(data) {
        let result = [];
        for (let key in data) {
            if (!data.hasOwnProperty(key)) {
                continue;
            }
            let statuses = [];
            for (let id in data[key].statuses || {}) {
                if (!data[key].statuses.hasOwnProperty(id)) {
                    continue;
                }
                let status = data[key].statuses[id];
                status.unique = `${key}_${id}`;
                statuses.push(status);
            }
            result.push({ ...data[key], statuses });
        }
        this.$leadPipelines = result;
    }
    setCustomersStatuses(data) {
        this.$customersStatuses = data || [];
    }
    setCustomersSegments(data) {
        this.$customersSegments = data || [];
    }
    async loadLeadPipelines() {
        if (this.$leadPipelines.length) {
            return this.$leadPipelines;
        }
        return lastValueFrom(MainVueApi.context
            .get(MainVueApi.accountHost + CORE().amoPaths.leadPipelines, {}, { 'X-Requested-With': 'XMLHttpRequest' })
            .pipe(map(response => {
            this.setLeadPipelines(response.response.pipelines);
            return this.$leadPipelines;
        })));
    }
    async loadCustomerStatuses() {
        if (this.$customersStatuses.length) {
            return this.$customersStatuses;
        }
        return lastValueFrom(MainVueApi.context
            .get(MainVueApi.accountHost + CORE().amoPaths.customersStatuses, {}, { 'X-Requested-With': 'XMLHttpRequest' })
            .pipe(map(response => {
            var _a;
            this.setCustomersStatuses((_a = response === null || response === void 0 ? void 0 : response._embedded) === null || _a === void 0 ? void 0 : _a.statuses);
            return this.$customersStatuses;
        }), catchError(() => of([]))));
    }
    async loadCustomerSegments() {
        if (this.$customersStatuses.length) {
            return this.$customersSegments;
        }
        return lastValueFrom(MainVueApi.context
            .get(MainVueApi.accountHost + CORE().amoPaths.customersSegments, {}, { 'X-Requested-With': 'XMLHttpRequest' })
            .pipe(map(response => {
            var _a;
            this.setCustomersSegments((_a = response === null || response === void 0 ? void 0 : response._embedded) === null || _a === void 0 ? void 0 : _a.segments);
            return this.$customersSegments;
        }), catchError(() => of([]))));
    }
    get leadPipelines() {
        return this.$leadPipelines;
    }
    get leadStatusesPipelineFor() {
        return (pipelineId, withUnsorted = false) => {
            let pipeline = this.$leadPipelines.find(item => +item.id === pipelineId);
            return ((pipeline === null || pipeline === void 0 ? void 0 : pipeline.statuses) || []).filter(item => (withUnsorted || item.type !== 1));
        };
    }
    get statusForLeadIdStatusId() {
        return (statusId, pipelineId) => {
            let pipelines = this.$leadPipelines;
            if (pipelineId) {
                pipelines = pipelines.filter(item => +item.id === pipelineId);
            }
            return pipelines
                .reduce((result, item) => [...result, ...item.statuses], [])
                .find(item => +item.id === statusId) || null;
        };
    }
    get customerStatuses() {
        return this.$customersStatuses;
    }
    get customerSegments() {
        return this.$customersSegments;
    }
};
__decorate([
    Mutation
], AmoPipelines.prototype, "setLeadPipelines", null);
__decorate([
    Mutation
], AmoPipelines.prototype, "setCustomersStatuses", null);
__decorate([
    Mutation
], AmoPipelines.prototype, "setCustomersSegments", null);
__decorate([
    Action
], AmoPipelines.prototype, "loadLeadPipelines", null);
__decorate([
    Action
], AmoPipelines.prototype, "loadCustomerStatuses", null);
__decorate([
    Action
], AmoPipelines.prototype, "loadCustomerSegments", null);
AmoPipelines = __decorate([
    Module({
        name: 'AmoPipelines',
        namespaced: true,
    })
], AmoPipelines);
export default AmoPipelines;
//# sourceMappingURL=amo-pipelines.js.map