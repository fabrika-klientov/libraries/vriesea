import { VuexModule } from 'vuex-module-decorators';
import { AmoTagModel } from '../../contracts/amo-tag.model';
export default class AmoTags extends VuexModule {
    private $leadTags;
    private $customerTags;
    private $contactTags;
    setLeadTags(data: any): void;
    setCustomerTags(data: any): void;
    setContactTags(data: any): void;
    loadLeadTags(): Promise<AmoTagModel[]>;
    loadCustomerTags(): Promise<AmoTagModel[]>;
    loadContactTags(): Promise<AmoTagModel[]>;
    loadTags(): Promise<AmoTagModel[]>;
    get leadTags(): AmoTagModel[];
    get customerTags(): AmoTagModel[];
    get contactTags(): AmoTagModel[];
}
