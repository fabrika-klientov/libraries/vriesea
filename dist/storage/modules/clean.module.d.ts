import { VuexModule } from 'vuex-module-decorators';
import { Subject } from 'rxjs';
import { FullCleanContract } from '../../contracts/full-clean.contract';
export declare abstract class CleanModule extends VuexModule implements FullCleanContract {
    protected unsubscribeEvent: Subject<void>;
    fullClean(): void;
}
