import { VuexModule } from 'vuex-module-decorators';
import { AmoContactV4Model } from '../../contracts/amo-contact.model';
import { FindEntitiesQueryParams } from '../../contracts/amo-entity.model';
export default class AmoContacts extends VuexModule {
    findByIdApiV4(id: number): Promise<AmoContactV4Model | null>;
    findByIdsApiV4(id: number[]): Promise<AmoContactV4Model[]>;
    findByQueryApiV4(data: FindEntitiesQueryParams<AmoContactV4Model>): Promise<AmoContactV4Model[]>;
}
