var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators';
import { lastValueFrom, map } from 'rxjs';
import { MainVueApi } from '../../modules/classes/main-vue-api';
import { getAPP, VRIESEA_CORE as CORE } from '../../configs/core.config';
let AmoAccount = class AmoAccount extends VuexModule {
    constructor() {
        super(...arguments);
        this.$account = null;
    }
    setAccount(account) {
        this.$account = account;
    }
    async loadAccount() {
        if (this.$account) {
            return this.$account;
        }
        return lastValueFrom(MainVueApi.context
            .get(MainVueApi.accountHost + CORE().amoPaths.account, {
            with: [
                'task_types',
                'amojo_id',
                'amojo_rights',
                'users_groups',
                'version',
                'entity_names',
                'datetime_settings',
            ].join(','),
        }, { 'X-Requested-With': 'XMLHttpRequest' })
            .pipe(map(response => {
            this.setAccount(response);
            return this.$account;
        })));
    }
    get account() {
        return this.$account;
    }
    get taskTypes() {
        var _a, _b;
        return ((_b = (_a = this.$account) === null || _a === void 0 ? void 0 : _a._embedded) === null || _b === void 0 ? void 0 : _b.task_types) || [];
    }
    get frontAccount() {
        return getAPP().constant('account');
    }
    get accountCurrency() {
        var _a;
        return ((_a = this.frontAccount) === null || _a === void 0 ? void 0 : _a.currency) || null;
    }
    get cf() {
        var _a;
        return (_a = this.frontAccount) === null || _a === void 0 ? void 0 : _a.cf;
    }
    get cfForId() {
        return id => { var _a; return (((_a = this.cf) === null || _a === void 0 ? void 0 : _a[id]) || null); };
    }
    get predefinedCF() {
        var _a;
        return (_a = this.frontAccount) === null || _a === void 0 ? void 0 : _a.predefined_cf;
    }
    get predefinedCFForCode() {
        return code => { var _a; return (((_a = this.predefinedCF) === null || _a === void 0 ? void 0 : _a[code]) || null); };
    }
};
__decorate([
    Mutation
], AmoAccount.prototype, "setAccount", null);
__decorate([
    Action
], AmoAccount.prototype, "loadAccount", null);
AmoAccount = __decorate([
    Module({
        name: 'AmoAccount',
        namespaced: true,
    })
], AmoAccount);
export default AmoAccount;
//# sourceMappingURL=amo-account.js.map