var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Action, VuexModule } from 'vuex-module-decorators';
import { Subject } from 'rxjs';
export class CleanModule extends VuexModule {
    constructor() {
        super(...arguments);
        this.unsubscribeEvent = new Subject(); // ex.: use as takeUntil
    }
    fullClean() {
        this.unsubscribeEvent.next();
    }
}
__decorate([
    Action
], CleanModule.prototype, "fullClean", null);
//# sourceMappingURL=clean.module.js.map