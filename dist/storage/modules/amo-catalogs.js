var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { VuexModule, Module, Mutation, Action, getModule } from 'vuex-module-decorators';
import { lastValueFrom, map } from 'rxjs';
import { MainVueApi } from '../../modules/classes/main-vue-api';
import { VRIESEA_CORE as CORE } from '../../configs/core.config';
import { $store } from '../store';
import AmoCf from './amo-cf';
let AmoCatalogs = class AmoCatalogs extends VuexModule {
    constructor() {
        super(...arguments);
        this.$catalogs = [];
        this.$elements = {};
    }
    setCatalogs(data) {
        let catalogs = [];
        for (let key in data) {
            if (!data.hasOwnProperty(key)) {
                continue;
            }
            catalogs.push(data[key]);
        }
        this.$catalogs = catalogs;
    }
    addCatalogElements({ idCatalog, list }) {
        this.$elements[idCatalog] = list;
    }
    clearCatalogElements(idCatalog) {
        if (idCatalog) {
            delete this.$elements[idCatalog];
            return;
        }
        this.$elements = {};
    }
    async loadCatalogs() {
        if (this.$catalogs.length) {
            return this.$catalogs;
        }
        return lastValueFrom(MainVueApi.context
            .get(MainVueApi.accountHost + CORE().amoPaths.catalogs, {}, { 'X-Requested-With': 'XMLHttpRequest' })
            .pipe(map(response => {
            var _a;
            this.setCatalogs(((_a = response.response) === null || _a === void 0 ? void 0 : _a.catalogs) || {});
            return this.$catalogs;
        })));
    }
    async loadCatalogElements(catalog_id) {
        var _a;
        if ((_a = this.$elements[catalog_id]) === null || _a === void 0 ? void 0 : _a.length) {
            return this.$elements[catalog_id];
        }
        let page = 1;
        let first = await this.loadPaginateCatalogElements({ catalog_id, page });
        let result = [];
        result.push(...first.response.catalog_elements);
        if (first.response.pagination.pages.total > page) {
            let list = await Promise.all([...Array(first.response.pagination.pages.total).keys()]
                .slice(2)
                .map(item => this.loadPaginateCatalogElements({ catalog_id, page: item })));
            result.push(...list
                .map(item => item.response.catalog_elements)
                .reduce((acc, item) => [...acc, ...item], []));
        }
        this.addCatalogElements({ idCatalog: catalog_id, list: result });
        return this.$elements[catalog_id] || [];
    }
    loadPaginateCatalogElements({ catalog_id, page }) {
        return lastValueFrom(MainVueApi.context
            .get(MainVueApi.accountHost + CORE().amoPaths.catalogElements, { catalog_id, json: 1, page }, { 'X-Requested-With': 'XMLHttpRequest' })
            .pipe(map(response => {
            return response;
        })));
    }
    get catalogs() {
        return this.$catalogs;
    }
    get cfFor() {
        let amoCf = getModule(AmoCf, $store);
        return catalog => amoCf.customFieldsFor(catalog.id);
    }
    get elementsFor() {
        return catalogId => (this.$elements[catalogId] || []);
    }
};
__decorate([
    Mutation
], AmoCatalogs.prototype, "setCatalogs", null);
__decorate([
    Mutation
], AmoCatalogs.prototype, "addCatalogElements", null);
__decorate([
    Mutation
], AmoCatalogs.prototype, "clearCatalogElements", null);
__decorate([
    Action
], AmoCatalogs.prototype, "loadCatalogs", null);
__decorate([
    Action
], AmoCatalogs.prototype, "loadCatalogElements", null);
__decorate([
    Action
], AmoCatalogs.prototype, "loadPaginateCatalogElements", null);
AmoCatalogs = __decorate([
    Module({
        name: 'AmoCatalogs',
        namespaced: true,
    })
], AmoCatalogs);
export default AmoCatalogs;
//# sourceMappingURL=amo-catalogs.js.map