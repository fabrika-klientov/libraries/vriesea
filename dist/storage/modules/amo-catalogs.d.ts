import { VuexModule } from 'vuex-module-decorators';
import { AmoCatalogElementModel, AmoCatalogModel } from '../../contracts/amo-catalog.model';
import { AmoCfModel } from '../../contracts/amo-cf.model';
interface ResponseCatalogElements {
    response: {
        catalog_elements: AmoCatalogElementModel[];
        pagination: {
            pages: {
                current: number;
                page_size: number;
                total: number;
            };
            total: number;
            available_rows_count: number[];
            selected_rows_count_value: number;
        };
        errors: any[];
        server_time: number;
    };
}
export default class AmoCatalogs extends VuexModule {
    protected $catalogs: AmoCatalogModel[];
    protected $elements: {
        [key: number]: AmoCatalogElementModel[];
    };
    setCatalogs(data: AmoCatalogModel[]): void;
    addCatalogElements({ idCatalog, list }: {
        idCatalog: number;
        list: AmoCatalogElementModel[];
    }): void;
    clearCatalogElements(idCatalog?: number): void;
    loadCatalogs(): Promise<AmoCatalogModel[]>;
    loadCatalogElements(catalog_id: number): Promise<AmoCatalogElementModel[]>;
    protected loadPaginateCatalogElements({ catalog_id, page }: {
        catalog_id: number;
        page: number;
    }): Promise<ResponseCatalogElements>;
    get catalogs(): AmoCatalogModel[];
    get cfFor(): (catalog: AmoCatalogModel) => AmoCfModel[];
    get elementsFor(): (catalogId: number) => AmoCatalogElementModel[];
}
export {};
