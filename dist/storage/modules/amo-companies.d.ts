import { VuexModule } from 'vuex-module-decorators';
import { AmoCompanyV4Model } from '../../contracts/amo-company.model';
import { FindEntitiesQueryParams } from '../../contracts/amo-entity.model';
export default class AmoCompanies extends VuexModule {
    findByIdApiV4(id: number): Promise<AmoCompanyV4Model | null>;
    findByIdsApiV4(id: number[]): Promise<AmoCompanyV4Model[]>;
    findByQueryApiV4(data: FindEntitiesQueryParams<AmoCompanyV4Model>): Promise<AmoCompanyV4Model[]>;
}
