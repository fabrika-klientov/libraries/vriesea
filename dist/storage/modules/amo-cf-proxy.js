var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Module, Action } from 'vuex-module-decorators';
import { lastValueFrom, map } from 'rxjs';
import { MainVueApi } from '../../modules/classes/main-vue-api';
import { VRIESEA_CORE as CORE } from '../../configs/core.config';
import AmoCf from './amo-cf';
let AmoCfProxy = class AmoCfProxy extends AmoCf {
    async loadCF(force = false) {
        if (!force && (this.$cf.leads.length || this.$cf.contacts.length || this.$cf.companies.length)) {
            return this.$cf;
        }
        return lastValueFrom(MainVueApi.context
            .get(CORE().proxyAmoServer + CORE().amoPaths.linkCustomFields)
            .pipe(map(response => {
            this.setCF(response.response.params.fields);
            this.setGroups(response.response.params.groups);
            return this.$cf;
        })));
    }
    async storeCF(factory) {
        if (!factory.list.length) {
            return [];
        }
        throw new Error('Not implement in Crimson proxy');
    }
};
__decorate([
    Action
], AmoCfProxy.prototype, "loadCF", null);
__decorate([
    Action
], AmoCfProxy.prototype, "storeCF", null);
AmoCfProxy = __decorate([
    Module({
        name: 'AmoCfProxy',
        namespaced: true,
    })
], AmoCfProxy);
export default AmoCfProxy;
//# sourceMappingURL=amo-cf-proxy.js.map