var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import Vue from 'vue';
import { Module, Mutation, Action } from 'vuex-module-decorators';
import { filter, lastValueFrom, map, Subject, takeUntil } from 'rxjs';
import { Widget } from '../../modules/classes/widget';
import { getAPP, VRIESEA_CORE as CORE } from '../../configs/core.config';
import { MainVueApi } from '../../modules/classes/main-vue-api';
import { CleanModule } from './clean.module';
import { ENTITY_CODES, entityConvert, getCardEntityId, getCardEntityName, getLiveValue } from '../../helpers/amo-instance.helper';
let AmoInstance = class AmoInstance extends CleanModule {
    constructor() {
        super(...arguments);
        this.$entity = null;
        this.$entityName = null;
        this.$entityId = null;
        this.$linked = {};
        this.$linkedElements = {};
        this.$live = {};
        this.$prevLive = {};
        this.$immediate = false;
        this.$liveSubject = new Subject();
    }
    setEntity(data) {
        this.$entity = data;
    }
    setEntityName(data) {
        this.$entityName = data;
    }
    setEntityId(data) {
        this.$entityId = data;
    }
    setLinked(data) {
        this.$linked = data;
    }
    addLinkedElements({ catalogId, data }) {
        var _a;
        let result = [];
        for (let key in data) {
            let item = data[key];
            item.element = ((_a = item._embedded) === null || _a === void 0 ? void 0 : _a.catalog_element) || null;
            delete item._embedded;
            result.push(item);
        }
        this.$linkedElements[catalogId] = result;
    }
    addLinkedElement({ catalogId, data }) {
        if (!this.$linkedElements[catalogId]) {
            this.$linkedElements[catalogId] = [];
        }
        this.$linkedElements[catalogId].push(data);
    }
    removeLinkedElement({ catalogId, data }) {
        if (!this.$linkedElements[catalogId]) {
            this.$linkedElements[catalogId] = [];
        }
        let index = this.$linkedElements[catalogId].indexOf(data);
        if (index !== -1) {
            this.$linkedElements[catalogId].splice(index, 1);
        }
    }
    pushLive(data) {
        let oldVal = this.$prevLive;
        this.$prevLive = JSON.parse(JSON.stringify(this.$live));
        if (!this.$immediate) {
            this.$immediate = true;
            return;
        }
        this.$liveSubject.next({ oldVal, newVal: data.newVal });
    }
    clean() {
        this.$entity = null;
        this.$entityId = null;
        this.$linked = {};
        this.$linkedElements = {};
    }
    cleanLive() {
        this.$live = {};
        this.$prevLive = {};
        this.$immediate = false;
    }
    async initInstance(withLinked = true) {
        this.setEntity(entityConvert(Widget.context.area));
        try {
            this.setEntityId(getCardEntityId());
            this.setEntityName(getCardEntityName(this.$entity));
            if (!withLinked) {
                return true;
            }
            let linked = await this.loadLinked();
            let linkedFor = type => {
                let list = linked[type] || [];
                return list.map(({ id, name }) => ({ id, name }));
            };
            this.setLinked({
                leads: linkedFor(ENTITY_CODES.LEADS),
                contacts: linkedFor(ENTITY_CODES.CONTACTS),
                companies: linkedFor(ENTITY_CODES.COMPANIES),
                customers: linkedFor(ENTITY_CODES.CUSTOMERS),
            });
        }
        catch (e) {
            console.warn(CORE().codeLogging, e);
            return false;
        }
        return true;
    }
    loadTimeline(props = {}) {
        return lastValueFrom(MainVueApi.context
            .get(MainVueApi.accountHost + `/v3/${this.$entity}/${this.$entityId}/timeline`, props)
            .pipe(map(result => {
            return (result === null || result === void 0 ? void 0 : result._embedded) || null;
        })));
    }
    async loadLinked() {
        let result = await this.loadTimeline();
        let data = {};
        if (result) {
            for (let type in result) {
                if ([ENTITY_CODES.LEADS, ENTITY_CODES.CONTACTS, ENTITY_CODES.COMPANIES, ENTITY_CODES.CUSTOMERS].includes(type)) {
                    data[type] = [];
                    for (let id in result[type]) {
                        if (!result[type].hasOwnProperty(id))
                            continue;
                        data[type].push(result[type][id]);
                    }
                }
            }
        }
        return data;
    }
    async loadLinkedElements({ catalogId, force }) {
        if (!force && this.$linkedElements[catalogId]) {
            return this.$linkedElements[catalogId];
        }
        return lastValueFrom(MainVueApi.context
            .get(MainVueApi.accountHost +
            CORE().amoPaths.linkedCatalogElements
                .replace('{lead}', `${this.$entityId}`)
                .replace('{catalog}', `${catalogId}`), { before_id: 0, limit: 100, with: 'catalog_element' }, { 'X-Requested-With': 'XMLHttpRequest' })
            .pipe(map(result => {
            var _a;
            this.addLinkedElements({ catalogId, data: ((_a = result === null || result === void 0 ? void 0 : result._embedded) === null || _a === void 0 ? void 0 : _a.links) || [] });
            return this.$linkedElements[catalogId];
        })));
    }
    initLive() {
        // TODO: patch this -> if current card is not `lead`
        const hp = type => {
            var _a, _b, _c;
            return (((_c = (_b = (_a = getAPP().data.current_card.linked_forms) === null || _a === void 0 ? void 0 : _a.form_models) === null || _b === void 0 ? void 0 : _b.models.find(item => { var _a, _b; return ((_a = item.attributes) === null || _a === void 0 ? void 0 : _a.ELEMENT_TYPE) == type && ((_b = item.attributes) === null || _b === void 0 ? void 0 : _b.ID); })) === null || _c === void 0 ? void 0 : _c.attributes) || null);
        };
        Vue.set(this.$live, 'leads', getAPP().data.current_card.model.attributes);
        Vue.set(this.$live, 'contacts', hp(1));
        Vue.set(this.$live, 'companies', hp(3));
    }
    fullClean() {
        super.fullClean();
        this.clean();
        this.cleanLive();
    }
    get entity() {
        return this.$entity;
    }
    get entityName() {
        return this.$entityName;
    }
    get entityId() {
        return this.$entityId;
    }
    get linked() {
        return this.$linked;
    }
    get linkedFor() {
        return entity => (this.$linked[entity] || []);
    }
    get linkedFirstFor() {
        return entity => (this.$linked[entity].slice(0, 1).shift() || null);
    }
    get linkedElements() {
        return this.$linkedElements;
    }
    get linkedElementsFor() {
        return catalogId => (this.$linkedElements[catalogId] || []);
    }
    get live() {
        return this.$liveSubject
            .asObservable()
            .pipe(takeUntil(this.unsubscribeEvent));
    }
    get liveField() {
        return (entity, id) => {
            return this.live
                .pipe(filter(({ oldVal, newVal }) => oldVal[entity] && newVal[entity]), filter(({ oldVal, newVal }) => {
                let current = getLiveValue(newVal[entity], id);
                let old = getLiveValue(oldVal[entity], id);
                if (!current || !old) {
                    return current !== old;
                }
                return JSON.stringify(current) !== JSON.stringify(old);
            }), map(({ oldVal, newVal }) => getLiveValue(newVal[entity], id)));
        };
    }
    get fieldValue() {
        return (entity, id) => {
            let model = this.$live[entity];
            if (!model) {
                return null;
            }
            return getLiveValue(model, id);
        };
    }
};
__decorate([
    Mutation
], AmoInstance.prototype, "setEntity", null);
__decorate([
    Mutation
], AmoInstance.prototype, "setEntityName", null);
__decorate([
    Mutation
], AmoInstance.prototype, "setEntityId", null);
__decorate([
    Mutation
], AmoInstance.prototype, "setLinked", null);
__decorate([
    Mutation
], AmoInstance.prototype, "addLinkedElements", null);
__decorate([
    Mutation
], AmoInstance.prototype, "addLinkedElement", null);
__decorate([
    Mutation
], AmoInstance.prototype, "removeLinkedElement", null);
__decorate([
    Mutation
], AmoInstance.prototype, "pushLive", null);
__decorate([
    Mutation
], AmoInstance.prototype, "clean", null);
__decorate([
    Mutation
], AmoInstance.prototype, "cleanLive", null);
__decorate([
    Action
], AmoInstance.prototype, "initInstance", null);
__decorate([
    Action
], AmoInstance.prototype, "loadTimeline", null);
__decorate([
    Action
], AmoInstance.prototype, "loadLinked", null);
__decorate([
    Action
], AmoInstance.prototype, "loadLinkedElements", null);
__decorate([
    Action
], AmoInstance.prototype, "initLive", null);
__decorate([
    Action
], AmoInstance.prototype, "fullClean", null);
AmoInstance = __decorate([
    Module({
        name: 'AmoInstance',
        namespaced: true,
    })
], AmoInstance);
export default AmoInstance;
//# sourceMappingURL=amo-instance.js.map