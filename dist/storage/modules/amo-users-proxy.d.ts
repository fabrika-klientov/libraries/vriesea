import { AmoManagerModel, AmoUserModel, AmoUserRoleModel } from '../../contracts/amo-user.model';
import AmoUsers from './amo-users';
export default class AmoUsersProxy extends AmoUsers {
    loadUsers(): Promise<AmoUserModel[]>;
    loadRoles(): Promise<AmoUserRoleModel[]>;
    loadManagersWithGroups(): Promise<AmoManagerModel[]>;
}
