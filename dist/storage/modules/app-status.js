var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { VuexModule, Module, Mutation, Action, getModule } from 'vuex-module-decorators';
import { lastValueFrom, map, ReplaySubject } from 'rxjs';
import { MainVueApi } from '../../modules/classes/main-vue-api';
import { VRIESEA_CORE as CORE } from '../../configs/core.config';
import TextLocales from './text-locales';
import { $store } from '../store';
export const APP_STATUS = {
    API_KEY_INVALID: 'api_key_invalid',
    API_KEY_NOT_ACTIVE: 'api_key_not_active',
    CUSTOMERS_SERVICE_NOT_EXIST: 'customers_service_not_exist',
    CUSTOMERS_SERVICE_CODE_INVALID: 'customers_service_code_invalid',
    PERIODS_NOT_EXIST: 'periods_not_exist',
    PERIODS_REFUSED: 'periods_refused',
    PERIODS_TRIAL_LESS: 'periods_trial_less',
    PERIODS_TRIAL_LIFETIME: 'periods_trial_lifetime',
    PERIODS_BEFORE_REFUSED: 'periods_before_refused',
    OK: 'ok',
    STATUS_SUCCESS_RESERVED_1: 'status_success_1',
    STATUS_SUCCESS_RESERVED_2: 'status_success_2',
    STATUS_SUCCESS_RESERVED_3: 'status_success_3',
    STATUS_FAIL_RESERVED_1: 'status_fail_1',
    STATUS_FAIL_RESERVED_2: 'status_fail_2',
    STATUS_FAIL_RESERVED_3: 'status_fail_3',
};
export const SUCCESS_STATUSES = [
    APP_STATUS.OK,
    APP_STATUS.PERIODS_BEFORE_REFUSED,
    APP_STATUS.PERIODS_REFUSED,
    APP_STATUS.PERIODS_NOT_EXIST,
    APP_STATUS.PERIODS_TRIAL_LESS,
    APP_STATUS.PERIODS_TRIAL_LIFETIME,
    APP_STATUS.STATUS_SUCCESS_RESERVED_1,
    APP_STATUS.STATUS_SUCCESS_RESERVED_2,
    APP_STATUS.STATUS_SUCCESS_RESERVED_3,
];
let AppStatus = class AppStatus extends VuexModule {
    constructor() {
        super(...arguments);
        this.$appStatus = null;
        this.$errorMessage = null;
        this.$todo = new ReplaySubject(1);
    }
    setStatusApp(status) {
        this.$appStatus = status;
    }
    setErrorMessage(data) {
        this.$errorMessage = data;
    }
    loadStatus() {
        return lastValueFrom(MainVueApi.context
            .get(CORE().server.replace(/\/?$/, '') + CORE().paths.appStatus + `/${CORE().userToken}`)
            .pipe(map(result => {
            this.setStatusApp(result);
            return result;
        })));
    }
    async messageError(type) {
        let response = await getModule(TextLocales, $store).messageError(type);
        this.setErrorMessage(response);
        return response;
    }
    pushTodo(data) {
        this.$todo.next(data);
    }
    get statusApp() {
        return this.$appStatus;
    }
    get isResolved() {
        return !!this.$appStatus && SUCCESS_STATUSES.includes(this.$appStatus.code);
    }
    get errorMessage() {
        return this.$errorMessage;
    }
    get todo() {
        return this.$todo.asObservable();
    }
};
__decorate([
    Mutation
], AppStatus.prototype, "setStatusApp", null);
__decorate([
    Mutation
], AppStatus.prototype, "setErrorMessage", null);
__decorate([
    Action
], AppStatus.prototype, "loadStatus", null);
__decorate([
    Action
], AppStatus.prototype, "messageError", null);
__decorate([
    Action
], AppStatus.prototype, "pushTodo", null);
AppStatus = __decorate([
    Module({
        name: 'AppStatus',
        namespaced: true,
    })
], AppStatus);
export default AppStatus;
//# sourceMappingURL=app-status.js.map