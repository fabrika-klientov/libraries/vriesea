var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators';
import { lastValueFrom, map } from 'rxjs';
import { VRIESEA_CORE as CORE } from '../../configs/core.config';
import { MainVueApi } from '../../modules/classes/main-vue-api';
let AmoTags = class AmoTags extends VuexModule {
    constructor() {
        super(...arguments);
        this.$leadTags = [];
        this.$customerTags = [];
        this.$contactTags = [];
    }
    setLeadTags(data) {
        let list = [];
        for (let key in data) {
            if (!data.hasOwnProperty(key)) {
                continue;
            }
            list.push(data[key]);
        }
        this.$leadTags = list;
    }
    setCustomerTags(data) {
        let list = [];
        for (let key in data) {
            if (!data.hasOwnProperty(key)) {
                continue;
            }
            list.push(data[key]);
        }
        this.$customerTags = list;
    }
    setContactTags(data) {
        let list = [];
        for (let key in data) {
            if (!data.hasOwnProperty(key)) {
                continue;
            }
            list.push(data[key]);
        }
        this.$contactTags = list;
    }
    async loadLeadTags() {
        if (this.$leadTags.length) {
            return this.$leadTags;
        }
        return lastValueFrom(MainVueApi.context
            .get(MainVueApi.accountHost + CORE().amoPaths.leadTags, { limit: '-1' }, {
            'X-Requested-With': 'XMLHttpRequest',
        })
            .pipe(map(result => {
            var _a;
            if ((_a = result === null || result === void 0 ? void 0 : result._embedded) === null || _a === void 0 ? void 0 : _a.items) {
                this.setLeadTags(result._embedded.items);
                return this.$leadTags;
            }
            return [];
        })));
    }
    async loadCustomerTags() {
        if (this.$customerTags.length) {
            return this.$customerTags;
        }
        return lastValueFrom(MainVueApi.context
            .get(MainVueApi.accountHost + CORE().amoPaths.customerTags, { limit: '-1' }, {
            'X-Requested-With': 'XMLHttpRequest',
        })
            .pipe(map(result => {
            var _a;
            if ((_a = result === null || result === void 0 ? void 0 : result._embedded) === null || _a === void 0 ? void 0 : _a.items) {
                this.setCustomerTags(result._embedded.items);
                return this.$customerTags;
            }
            return [];
        })));
    }
    async loadContactTags() {
        if (this.$contactTags.length) {
            return this.$contactTags;
        }
        return lastValueFrom(MainVueApi.context
            .get(MainVueApi.accountHost + CORE().amoPaths.contactTags, { limit: '-1' }, {
            'X-Requested-With': 'XMLHttpRequest',
        })
            .pipe(map(result => {
            var _a;
            if ((_a = result === null || result === void 0 ? void 0 : result._embedded) === null || _a === void 0 ? void 0 : _a.items) {
                this.setContactTags(result._embedded.items);
                return this.$contactTags;
            }
            return [];
        })));
    }
    async loadTags() {
        return Promise.all([
            this.loadLeadTags(),
            this.loadCustomerTags(),
            this.loadContactTags(),
        ])
            .then(([leadT, customerT, contactT]) => [
            ...leadT,
            ...customerT,
            ...contactT,
        ]);
    }
    get leadTags() {
        return this.$leadTags;
    }
    get customerTags() {
        return this.$customerTags;
    }
    get contactTags() {
        return this.$contactTags;
    }
};
__decorate([
    Mutation
], AmoTags.prototype, "setLeadTags", null);
__decorate([
    Mutation
], AmoTags.prototype, "setCustomerTags", null);
__decorate([
    Mutation
], AmoTags.prototype, "setContactTags", null);
__decorate([
    Action
], AmoTags.prototype, "loadLeadTags", null);
__decorate([
    Action
], AmoTags.prototype, "loadCustomerTags", null);
__decorate([
    Action
], AmoTags.prototype, "loadContactTags", null);
__decorate([
    Action
], AmoTags.prototype, "loadTags", null);
AmoTags = __decorate([
    Module({
        name: 'AmoTags',
        namespaced: true,
    })
], AmoTags);
export default AmoTags;
//# sourceMappingURL=amo-tags.js.map