import { VuexModule } from 'vuex-module-decorators';
import { AmoCfGroupEntities, AmoCfGroupGroups, AmoCfGroupModel, AmoCfModel, FactoryCF } from '../../contracts/amo-cf.model';
export default class AmoCf extends VuexModule {
    protected $cf: AmoCfGroupEntities;
    protected $groups: AmoCfGroupGroups;
    setCF(data: any): void;
    setGroups(data: any): void;
    loadCF(force?: boolean): Promise<AmoCfGroupEntities>;
    storeCF(factory: FactoryCF): Promise<AmoCfModel[]>;
    get customFields(): AmoCfGroupEntities;
    get customFieldsFor(): (entity: keyof AmoCfGroupEntities, typeId?: number) => AmoCfModel[];
    get groups(): AmoCfGroupGroups;
    get groupsFor(): (entity: keyof AmoCfGroupEntities) => AmoCfGroupModel[];
    get customFieldsForEntityGroup(): (entity: keyof AmoCfGroupEntities, group: AmoCfGroupModel) => AmoCfModel[];
}
