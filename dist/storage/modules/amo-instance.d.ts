import { Observable } from 'rxjs';
import { AmoEntity } from '../../contracts/amo-instances.types';
import { CleanModule } from './clean.module';
import { AmoCfGroupEntities } from '../../contracts/amo-cf.model';
import { AmoLinkedElementModel } from '../../contracts/amo-catalog.model';
import { AmoTimelineModel } from '../../contracts/amo-timeline.model';
type LinkedAmo = {
    leads?: Linked[];
    contacts?: Linked[];
    companies?: Linked[];
    customers?: Linked[];
};
interface Linked {
    id: number;
    name: string;
}
export default class AmoInstance extends CleanModule {
    private $entity;
    private $entityName;
    private $entityId;
    private $linked;
    private $linkedElements;
    private $live;
    private $prevLive;
    private $immediate;
    private $liveSubject;
    setEntity(data: AmoEntity | null): void;
    setEntityName(data: string): void;
    setEntityId(data: number): void;
    setLinked(data: LinkedAmo): void;
    addLinkedElements({ catalogId, data }: {
        catalogId: number;
        data: any[];
    }): void;
    addLinkedElement({ catalogId, data }: {
        catalogId: number;
        data: AmoLinkedElementModel;
    }): void;
    removeLinkedElement({ catalogId, data }: {
        catalogId: number;
        data: AmoLinkedElementModel;
    }): void;
    pushLive(data: {
        oldVal: any;
        newVal: any;
    }): void;
    clean(): void;
    cleanLive(): void;
    initInstance(withLinked?: boolean): Promise<boolean>;
    loadTimeline(props?: any): Promise<AmoTimelineModel>;
    protected loadLinked(): Promise<any>;
    loadLinkedElements({ catalogId, force }: {
        catalogId: number;
        force?: boolean;
    }): Promise<AmoLinkedElementModel[]>;
    initLive(): void;
    fullClean(): void;
    get entity(): AmoEntity | null;
    get entityName(): string | null;
    get entityId(): number | null;
    get linked(): LinkedAmo;
    get linkedFor(): (entity: keyof LinkedAmo) => Linked[];
    get linkedFirstFor(): (entity: keyof LinkedAmo) => Linked | null;
    get linkedElements(): {
        [key: number]: AmoLinkedElementModel[];
    };
    get linkedElementsFor(): (catalogId: number) => AmoLinkedElementModel[];
    get live(): Observable<{
        oldVal: any;
        newVal: any;
    }>;
    get liveField(): <T>(entity: keyof AmoCfGroupEntities, id: number | string) => Observable<T>;
    get fieldValue(): <T>(entity: keyof AmoCfGroupEntities, id: number | string) => T | null;
}
export {};
