import { VuexModule } from 'vuex-module-decorators';
import { AmoAccountModel, AmoFrontAccountModel, AmoFrontCfModel, TaskTypeModel } from '../../contracts/amo-account.model';
export default class AmoAccount extends VuexModule {
    private $account;
    setAccount(account: AmoAccountModel): void;
    loadAccount(): Promise<AmoAccountModel | null>;
    get account(): AmoAccountModel | null;
    get taskTypes(): TaskTypeModel[];
    get frontAccount(): AmoFrontAccountModel;
    get accountCurrency(): string | null;
    get cf(): {
        [key: number]: AmoFrontCfModel;
    };
    get cfForId(): (id: number) => AmoFrontCfModel;
    get predefinedCF(): {
        [key: string]: AmoFrontCfModel;
    };
    get predefinedCFForCode(): (code: string) => AmoFrontCfModel;
}
