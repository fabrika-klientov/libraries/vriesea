import { VuexModule } from 'vuex-module-decorators';
import { AmoGroupModel, AmoManagerModel, AmoUserModel, AmoUserRoleModel } from '../../contracts/amo-user.model';
export default class AmoUsers extends VuexModule {
    protected $users: AmoUserModel[];
    protected $roles: AmoUserRoleModel[];
    protected $managers: AmoManagerModel[];
    protected $groups: AmoGroupModel[];
    setUsers(users: AmoUserModel[]): void;
    setRoles(roles: AmoUserRoleModel[]): void;
    setManagers(managers: any): void;
    setGroups(groups: any): void;
    loadUsers(): Promise<AmoUserModel[]>;
    loadRoles(): Promise<AmoUserRoleModel[]>;
    loadManagersWithGroups(): Promise<AmoManagerModel[]>;
    get users(): AmoUserModel[];
    get roles(): AmoUserRoleModel[];
    get managers(): AmoManagerModel[];
    get groups(): AmoGroupModel[];
    get managersForGroup(): (id: string) => AmoManagerModel[];
}
