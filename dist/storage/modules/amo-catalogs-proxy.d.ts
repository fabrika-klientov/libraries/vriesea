import { AmoCatalogElementModel, AmoCatalogModel } from '../../contracts/amo-catalog.model';
import AmoCatalogs from './amo-catalogs';
export default class AmoCatalogsProxy extends AmoCatalogs {
    loadCatalogs(): Promise<AmoCatalogModel[]>;
    loadCatalogElements(catalog_id: number): Promise<AmoCatalogElementModel[]>;
}
