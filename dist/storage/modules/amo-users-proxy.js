var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Module, Action } from 'vuex-module-decorators';
import { lastValueFrom, map } from 'rxjs';
import { MainVueApi } from '../../modules/classes/main-vue-api';
import { VRIESEA_CORE as CORE } from '../../configs/core.config';
import AmoUsers from './amo-users';
let AmoUsersProxy = class AmoUsersProxy extends AmoUsers {
    async loadUsers() {
        if (this.$users.length) {
            return this.$users;
        }
        return lastValueFrom(MainVueApi.context
            .get(CORE().proxyAmoServer + CORE().amoPaths.users, { limit: -1, page: 1, 'filter[active]': 'Y' })
            .pipe(map(response => {
            this.setUsers(response._embedded.items);
            return this.$users;
        })));
    }
    async loadRoles() {
        if (this.$roles.length) {
            return this.$roles;
        }
        throw new Error('Not implement in Crimson proxy');
    }
    async loadManagersWithGroups() {
        if (this.$managers.length) {
            return this.$managers;
        }
        return lastValueFrom(MainVueApi.context
            .get(CORE().proxyAmoServer + CORE().amoPaths.managersWithGroups, { free_users: 'Y' })
            .pipe(map(response => {
            this.setManagers(response.managers);
            this.setGroups(response.groups);
            return this.$managers;
        })));
    }
};
__decorate([
    Action
], AmoUsersProxy.prototype, "loadUsers", null);
__decorate([
    Action
], AmoUsersProxy.prototype, "loadRoles", null);
__decorate([
    Action
], AmoUsersProxy.prototype, "loadManagersWithGroups", null);
AmoUsersProxy = __decorate([
    Module({
        name: 'AmoUsersProxy',
        namespaced: true,
    })
], AmoUsersProxy);
export default AmoUsersProxy;
//# sourceMappingURL=amo-users-proxy.js.map