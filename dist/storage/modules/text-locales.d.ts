import { VuexModule } from 'vuex-module-decorators';
export default class TextLocales extends VuexModule {
    $lang: string;
    $texts: {
        [key: string]: string;
    };
    initText(): Promise<any>;
    messageError(type: string): Promise<any>;
    get link(): (type: string) => string;
    get sharedLink(): (code: string, type: string) => string;
    get texts(): {
        [key: string]: string;
    };
    get text(): (key: string) => string;
}
