var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators';
import { lastValueFrom, map } from 'rxjs';
import { MainVueApi } from '../../modules/classes/main-vue-api';
import { VRIESEA_CORE as CORE } from '../../configs/core.config';
let AmoUsers = class AmoUsers extends VuexModule {
    constructor() {
        super(...arguments);
        this.$users = [];
        this.$roles = [];
        this.$managers = [];
        this.$groups = [];
    }
    setUsers(users) {
        this.$users = users;
    }
    setRoles(roles) {
        this.$roles = roles || [];
    }
    setManagers(managers) {
        let result = [];
        for (let id in managers) {
            if (!managers.hasOwnProperty(id)) {
                continue;
            }
            let manager = managers[id];
            manager.id = +manager.id;
            result.push(manager);
        }
        this.$managers = result;
    }
    setGroups(groups) {
        let result = [];
        for (let id in groups) {
            if (!groups.hasOwnProperty(id)) {
                continue;
            }
            result.push({ id, title: groups[id] });
        }
        this.$groups = result;
    }
    async loadUsers() {
        if (this.$users.length) {
            return this.$users;
        }
        return lastValueFrom(MainVueApi.context
            .get(MainVueApi.accountHost + CORE().amoPaths.users, { limit: -1, page: 1, 'filter[active]': 'Y' }, { 'X-Requested-With': 'XMLHttpRequest' })
            .pipe(map(response => {
            this.setUsers(response._embedded.items);
            return this.$users;
        })));
    }
    async loadRoles() {
        if (this.$roles.length) {
            return this.$roles;
        }
        return lastValueFrom(MainVueApi.context
            .get(MainVueApi.accountHost + CORE().amoPaths.usersRoles, {}, { 'X-Requested-With': 'XMLHttpRequest' })
            .pipe(map(response => {
            var _a;
            this.setRoles((_a = response === null || response === void 0 ? void 0 : response._embedded) === null || _a === void 0 ? void 0 : _a.roles);
            return this.$roles;
        })));
    }
    async loadManagersWithGroups() {
        if (this.$managers.length) {
            return this.$managers;
        }
        return lastValueFrom(MainVueApi.context
            .get(MainVueApi.accountHost + CORE().amoPaths.managersWithGroups, { free_users: 'Y' }, { 'X-Requested-With': 'XMLHttpRequest' })
            .pipe(map(response => {
            this.setManagers(response.managers);
            this.setGroups(response.groups);
            return this.$managers;
        })));
    }
    get users() {
        return this.$users;
    }
    get roles() {
        return this.$roles;
    }
    get managers() {
        return this.$managers;
    }
    get groups() {
        return this.$groups;
    }
    get managersForGroup() {
        return id => this.$managers.filter(item => item.group === id);
    }
};
__decorate([
    Mutation
], AmoUsers.prototype, "setUsers", null);
__decorate([
    Mutation
], AmoUsers.prototype, "setRoles", null);
__decorate([
    Mutation
], AmoUsers.prototype, "setManagers", null);
__decorate([
    Mutation
], AmoUsers.prototype, "setGroups", null);
__decorate([
    Action
], AmoUsers.prototype, "loadUsers", null);
__decorate([
    Action
], AmoUsers.prototype, "loadRoles", null);
__decorate([
    Action
], AmoUsers.prototype, "loadManagersWithGroups", null);
AmoUsers = __decorate([
    Module({
        name: 'AmoUsers',
        namespaced: true,
    })
], AmoUsers);
export default AmoUsers;
//# sourceMappingURL=amo-users.js.map