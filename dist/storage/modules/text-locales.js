var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { VuexModule, Module, Action } from 'vuex-module-decorators';
import { lastValueFrom, map, zip } from 'rxjs';
import { VRIESEA_CORE as CORE } from '../../configs/core.config';
import { MainVueApi } from '../../modules/classes/main-vue-api';
let TextLocales = class TextLocales extends VuexModule {
    constructor() {
        super(...arguments);
        this.$lang = 'ru';
        this.$texts = {
            info_description: '<p>Описание</p>',
        };
    }
    initText() {
        return lastValueFrom(zip(MainVueApi.context
            .get(this.link('description'))
            .pipe(map(result => {
            this.$texts.info_description = result;
            return true;
        }))));
    }
    messageError(type) {
        return lastValueFrom(MainVueApi.context.get(this.sharedLink('messages', type)));
    }
    get link() {
        return (type) => CORE().textServer + CORE().linkDescription
            .replace('{{code}}', CORE().codeWidget)
            .replace('{{type}}', type)
            .replace('{{lang}}', this.$lang);
    }
    get sharedLink() {
        return (code, type) => CORE().textServer + CORE().linkShared
            .replace('{{code}}', code)
            .replace('{{type}}', type)
            .replace('{{lang}}', this.$lang);
    }
    get texts() {
        return this.$texts;
    }
    get text() {
        return key => (this.$texts[key] || '');
    }
};
__decorate([
    Action
], TextLocales.prototype, "initText", null);
__decorate([
    Action
], TextLocales.prototype, "messageError", null);
TextLocales = __decorate([
    Module({
        name: 'TextLocales',
        namespaced: true,
    })
], TextLocales);
export default TextLocales;
//# sourceMappingURL=text-locales.js.map