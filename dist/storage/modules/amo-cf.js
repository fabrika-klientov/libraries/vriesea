var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators';
import { from, lastValueFrom, map, of, switchMap } from 'rxjs';
import { MainVueApi } from '../../modules/classes/main-vue-api';
import { VRIESEA_CORE as CORE } from '../../configs/core.config';
const cfRejectedId = ['budget', 'comp_name', 'cont_name', 'catalog_element_name'];
let AmoCf = class AmoCf extends VuexModule {
    constructor() {
        super(...arguments);
        this.$cf = {
            leads: [],
            contacts: [],
            companies: [],
            customers: [],
            segments: [],
        };
        this.$groups = {
            leads: [],
            contacts: [],
            companies: [],
            customers: [],
            segments: [],
        };
    }
    setCF(data) {
        let result = {};
        for (let key in data) {
            if (!data.hasOwnProperty(key)) {
                continue;
            }
            result[key] = [];
            for (let id in data[key]) {
                if (!data[key].hasOwnProperty(id)) {
                    continue;
                }
                let item = data[key][id];
                if (cfRejectedId.indexOf(`${item.id}`) !== -1) {
                    continue;
                }
                if (item.enums) {
                    let enums = [];
                    for (let j in item.enums) {
                        if (!item.enums.hasOwnProperty(j)) {
                            continue;
                        }
                        enums.push(item.enums[j]);
                    }
                    item.enums = enums;
                }
                item.element_code = key;
                item.entityWithId = `${key}_${id}`;
                result[key].push(item);
            }
        }
        this.$cf = result;
    }
    setGroups(data) {
        var _a;
        let result = {};
        for (let key in data) {
            if (!data.hasOwnProperty(key)) {
                continue;
            }
            result[key] = [];
            if (!((_a = data[key]) === null || _a === void 0 ? void 0 : _a.length)) {
                continue;
            }
            result[key] = data[key].map(item => {
                switch (item.id) {
                    case 'default':
                        item.name = 'Основное';
                        break;
                    case 'statistic':
                        item.name = 'Статистика';
                }
                // amo bag if c.field deleted or moved
                if (item.fields && !item.fields.length) {
                    const fields = [];
                    for (let index in item.fields) {
                        if (!item.fields.hasOwnProperty(index)) {
                            continue;
                        }
                        fields.push(item.fields[index]);
                    }
                    item.fields = fields;
                }
                item.element_code = key;
                item.entityWithId = `${key}_${item.id}`;
                return item;
            });
        }
        this.$groups = result;
    }
    async loadCF(force = false) {
        if (!force && (this.$cf.leads.length || this.$cf.contacts.length || this.$cf.companies.length)) {
            return this.$cf;
        }
        return lastValueFrom(MainVueApi.context
            .get(MainVueApi.accountHost + CORE().amoPaths.linkCustomFields, {}, { 'X-Requested-With': 'XMLHttpRequest' })
            .pipe(map(response => {
            this.setCF(response.response.params.fields);
            this.setGroups(response.response.params.groups);
            return this.$cf;
        })));
    }
    async storeCF(factory) {
        if (!factory.list.length) {
            return [];
        }
        return lastValueFrom(MainVueApi.context
            .post(MainVueApi.accountHost + CORE().amoPaths.v4Cf.replace('{entity}', factory.entity), factory.list)
            .pipe(switchMap(response => {
            var _a;
            let stored = ((_a = response._embedded) === null || _a === void 0 ? void 0 : _a.custom_fields) || [];
            if (!stored.length) {
                return of([]);
            }
            return from(this.loadCF(true))
                .pipe(map(() => this.customFieldsFor(factory.entity).filter(item => stored.some(one => one.id === item.id))));
        })));
    }
    get customFields() {
        return this.$cf;
    }
    get customFieldsFor() {
        return (entity, typeId) => {
            let list = this.$cf[entity];
            if (typeId && list) {
                return list.filter(item => item.type_id === typeId);
            }
            return list;
        };
    }
    get groups() {
        return this.$groups;
    }
    get groupsFor() {
        return (entity) => (this.$groups[entity] || []);
    }
    get customFieldsForEntityGroup() {
        return (entity, group) => {
            let cfList = this.customFieldsFor(entity);
            if (!(cfList === null || cfList === void 0 ? void 0 : cfList.length)) {
                return [];
            }
            return cfList.filter(item => group.fields.some(one => +one === +item.id));
        };
    }
};
__decorate([
    Mutation
], AmoCf.prototype, "setCF", null);
__decorate([
    Mutation
], AmoCf.prototype, "setGroups", null);
__decorate([
    Action
], AmoCf.prototype, "loadCF", null);
__decorate([
    Action
], AmoCf.prototype, "storeCF", null);
AmoCf = __decorate([
    Module({
        name: 'AmoCf',
        namespaced: true,
    })
], AmoCf);
export default AmoCf;
//# sourceMappingURL=amo-cf.js.map