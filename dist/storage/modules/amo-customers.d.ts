import { VuexModule } from 'vuex-module-decorators';
import { FindEntitiesQueryParams } from '../../contracts/amo-entity.model';
import { AmoCustomerAjaxV2Model, AmoCustomerV4Model } from '../../contracts/amo-customer.model';
export default class AmoCustomers extends VuexModule {
    findById(id: number): Promise<AmoCustomerAjaxV2Model | null>;
    findByIdApiV4(id: number): Promise<AmoCustomerV4Model | null>;
    findByIdsApiV4(id: number[]): Promise<AmoCustomerV4Model[]>;
    findByQueryApiV4(data: FindEntitiesQueryParams<AmoCustomerV4Model>): Promise<AmoCustomerV4Model[]>;
}
