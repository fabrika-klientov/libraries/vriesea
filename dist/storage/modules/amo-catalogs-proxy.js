var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Module, Action } from 'vuex-module-decorators';
import { lastValueFrom, map } from 'rxjs';
import { MainVueApi } from '../../modules/classes/main-vue-api';
import { VRIESEA_CORE as CORE } from '../../configs/core.config';
import AmoCatalogs from './amo-catalogs';
let AmoCatalogsProxy = class AmoCatalogsProxy extends AmoCatalogs {
    async loadCatalogs() {
        if (this.$catalogs.length) {
            return this.$catalogs;
        }
        return lastValueFrom(MainVueApi.context
            .get(CORE().proxyAmoServer + CORE().amoPaths.catalogs)
            .pipe(map(response => {
            var _a;
            this.setCatalogs(((_a = response.response) === null || _a === void 0 ? void 0 : _a.catalogs) || {});
            return this.$catalogs;
        })));
    }
    async loadCatalogElements(catalog_id) {
        var _a;
        if ((_a = this.$elements[catalog_id]) === null || _a === void 0 ? void 0 : _a.length) {
            return this.$elements[catalog_id];
        }
        throw new Error('Not implement in Crimson proxy');
    }
};
__decorate([
    Action
], AmoCatalogsProxy.prototype, "loadCatalogs", null);
__decorate([
    Action
], AmoCatalogsProxy.prototype, "loadCatalogElements", null);
AmoCatalogsProxy = __decorate([
    Module({
        name: 'AmoCatalogsProxy',
        namespaced: true,
    })
], AmoCatalogsProxy);
export default AmoCatalogsProxy;
//# sourceMappingURL=amo-catalogs-proxy.js.map