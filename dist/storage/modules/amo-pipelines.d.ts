import { VuexModule } from 'vuex-module-decorators';
import { AmoCustomerSegmentModel, AmoCustomerStatusModel, AmoLeadPipelineModel, AmoLeadStatusPipelineModel } from '../../contracts/amo-pipelines.model';
export default class AmoPipelines extends VuexModule {
    private $leadPipelines;
    private $customersStatuses;
    private $customersSegments;
    setLeadPipelines(data: any): void;
    setCustomersStatuses(data: any): void;
    setCustomersSegments(data: any): void;
    loadLeadPipelines(): Promise<AmoLeadPipelineModel[]>;
    loadCustomerStatuses(): Promise<AmoCustomerStatusModel[]>;
    loadCustomerSegments(): Promise<AmoCustomerSegmentModel[]>;
    get leadPipelines(): AmoLeadPipelineModel[];
    get leadStatusesPipelineFor(): (pipelineId: number, withUnsorted?: boolean) => AmoLeadStatusPipelineModel[];
    get statusForLeadIdStatusId(): (statusId: number, pipelineId?: number) => AmoLeadStatusPipelineModel | null;
    get customerStatuses(): AmoCustomerStatusModel[];
    get customerSegments(): AmoCustomerSegmentModel[];
}
