import { VuexModule } from 'vuex-module-decorators';
import { Observable } from 'rxjs';
export type StatusAppType = 'api_key_invalid' | 'api_key_not_active' | 'customers_service_not_exist' | 'customers_service_code_invalid' | 'periods_not_exist' | 'periods_refused' | 'periods_trial_less' | 'periods_trial_lifetime' | 'periods_before_refused' | 'status_success_1' | 'status_success_2' | 'status_success_3' | 'status_fail_1' | 'status_fail_2' | 'status_fail_3' | 'ok';
export declare const APP_STATUS: {
    API_KEY_INVALID: string;
    API_KEY_NOT_ACTIVE: string;
    CUSTOMERS_SERVICE_NOT_EXIST: string;
    CUSTOMERS_SERVICE_CODE_INVALID: string;
    PERIODS_NOT_EXIST: string;
    PERIODS_REFUSED: string;
    PERIODS_TRIAL_LESS: string;
    PERIODS_TRIAL_LIFETIME: string;
    PERIODS_BEFORE_REFUSED: string;
    OK: string;
    STATUS_SUCCESS_RESERVED_1: string;
    STATUS_SUCCESS_RESERVED_2: string;
    STATUS_SUCCESS_RESERVED_3: string;
    STATUS_FAIL_RESERVED_1: string;
    STATUS_FAIL_RESERVED_2: string;
    STATUS_FAIL_RESERVED_3: string;
};
export declare const SUCCESS_STATUSES: string[];
export interface StatusAppModel {
    type: 'success' | 'warning' | 'info' | 'error';
    code: StatusAppType;
    additional?: {
        period_start?: string;
        period_end?: string;
    };
}
export interface TodoAction {
    type: string;
    tab: string;
    additional?: any;
}
export default class AppStatus extends VuexModule {
    private $appStatus;
    private $errorMessage;
    private $todo;
    setStatusApp(status: StatusAppModel): void;
    setErrorMessage(data: string): void;
    loadStatus(): Promise<StatusAppModel>;
    messageError(type: string): Promise<any>;
    pushTodo(data: TodoAction | null): void;
    get statusApp(): StatusAppModel | null;
    get isResolved(): boolean;
    get errorMessage(): string | null;
    get todo(): Observable<TodoAction | null>;
}
