import { PushBox } from './core/push-box';
import { Widget } from '../modules/classes/widget';
import { AdvancedWidgetSettingsContract, WidgetContract, WidgetSettingsContract } from '../contracts/widget.contract';
export declare abstract class CoreBase implements WidgetContract, WidgetSettingsContract, AdvancedWidgetSettingsContract {
    protected _status: boolean;
    protected _widget: Widget;
    protected _pushBox: PushBox;
    protected constructor(instance: any);
    protected abstract code(): string;
    render(): void;
    init(): void;
    bindActions(): void;
    destroy(): void;
    settings(data: any): void;
    onSave(data: any): void;
    advancedSettings(): void;
    protected doFront(): void;
    protected doDp(): void;
    protected initWidget(): void;
    protected loadingDp(): void;
    protected miniSettings(): void;
}
