import { CoreBase } from './core-base';
export declare const factoryCallbacks: <T extends CoreBase>(factory: () => T) => any;
