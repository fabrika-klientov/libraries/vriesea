import { MainVueApi } from '../../modules/classes/main-vue-api';
import { VRIESEA_CORE as CORE } from '../../configs/core.config';
export class InjectTextsService {
    constructor() {
        this.lang = 'ru';
    }
    settingsInject() {
        this.injectDescription();
        this.injectShortDescription();
        this.injectName();
    }
    injectDescription() {
        MainVueApi.context.get(this.getLink('description')).subscribe(data => {
            if (data && data.length) {
                $('.widget_settings_block__descr').html(data);
            }
        });
    }
    injectShortDescription() {
        MainVueApi.context.get(this.getLink('short_description')).subscribe(data => {
            if (data && data.length) {
                $('.widget_settings_block__head-desc').html(data);
            }
        });
    }
    injectName() {
        MainVueApi.context.get(this.getLink('name')).subscribe(data => {
            if (data && data.length) {
                $('.widget_settings_block__title').html(data);
            }
        });
    }
    getLink(type) {
        return CORE().textServer + CORE().linkDescription
            .replace('{{code}}', CORE().codeWidget)
            .replace('{{type}}', type)
            .replace('{{lang}}', this.lang);
    }
}
//# sourceMappingURL=inject-texts.service.js.map