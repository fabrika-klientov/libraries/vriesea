export declare class InjectTextsService {
    protected lang: string;
    settingsInject(): void;
    injectDescription(): void;
    injectShortDescription(): void;
    injectName(): void;
    protected getLink(type: string): string;
}
