export const factoryCallbacks = (factory) => {
    let $core;
    return {
        render: () => {
            if (!$core) {
                $core = factory();
            }
            $core.render();
            return true;
        },
        init: () => {
            $core.init();
            return true;
        },
        bind_actions: () => {
            $core.bindActions();
            return true;
        },
        settings: (data) => {
            if ($core.settings) {
                $core.settings(data);
            }
            return true;
        },
        onSave: (data) => {
            if ($core.onSave) {
                $core.onSave(data);
            }
            return true;
        },
        destroy: () => {
            if ($core.destroy) {
                $core.destroy();
            }
        },
        contacts: {
            selected: () => {
                if ($core.onSelectContacts) {
                    $core.onSelectContacts();
                }
            },
        },
        leads: {
            selected: () => {
                if ($core.onSelectLeads) {
                    $core.onSelectLeads();
                }
            },
        },
        customers: {
            selected: () => {
                if ($core.onSelectCustomers) {
                    $core.onSelectCustomers();
                }
            }
        },
        tasks: {
            selected: () => {
                if ($core.onSelectTasks) {
                    $core.onSelectTasks();
                }
            },
        },
        dpSettings: () => {
            if ($core.dpSettings) {
                $core.dpSettings();
            }
            return true;
        },
        advancedSettings: () => {
            $core.advancedSettings();
        },
        loadPreloadedData: async () => {
            if ($core.loadPreloadedData) {
                return $core.loadPreloadedData();
            }
            return [];
        },
        loadElements: async () => {
            if ($core.loadElements) {
                return $core.loadElements();
            }
            return [];
        },
        linkCard: async () => {
            if ($core.linkCard) {
                return $core.linkCard();
            }
            return [];
        },
        searchDataInCard: async () => {
            if ($core.searchDataInCard) {
                return $core.searchDataInCard();
            }
            return [];
        },
    };
};
//# sourceMappingURL=callbacks.js.map