export declare class PushBox {
    private _widget;
    private _script;
    constructor();
    protected script(): void;
    init(): void;
    dp(): void;
    another(entry: string, ...params: any): void;
}
