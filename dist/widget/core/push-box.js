import { VRIESEA_CORE as CORE } from '../../configs/core.config';
import { Widget } from '../../modules/classes/widget';
export class PushBox {
    constructor() {
        this._widget = Widget.context;
        this.script();
    }
    script() {
        let scriptVersion = CORE().scriptVersion;
        // @MERCURY(START_REPLACE_SEARCH)
        if (CORE().publicEnv) {
            this._script = this._widget.params.path + `/dist/front/js/app.js?v=${scriptVersion}`;
        }
        else {
            this._script = CORE().frontPath + '/js/app.js';
        }
        // @MERCURY(START_REPLACE_VALUE)
        // this._script = this._widget.params.path + `/dist/front/js/app.js?v=${scriptVersion}`;
        // @MERCURY(END_REPLACE_VALUE)
        // @MERCURY(END_REPLACE_SEARCH)
    }
    init() {
        requirejs([this._script], (module) => {
            module.Front.run();
        });
    }
    dp() {
        requirejs([this._script], (module) => {
            module.Front.dp();
        });
    }
    another(entry, ...params) {
        requirejs([this._script], (module) => {
            module.Front[entry](...params);
        });
    }
}
//# sourceMappingURL=push-box.js.map