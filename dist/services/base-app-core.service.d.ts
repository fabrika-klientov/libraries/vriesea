import { BaseAppCoreStatusesService } from './base-app-core-statuses.service';
export declare abstract class BaseAppCoreService extends BaseAppCoreStatusesService {
    protected renderTypeHook: string;
    protected accessTypeHooks: string[];
    protected renderDeepWorkflowTypeHook: string | null;
    protected accessDeepWorkflowTypeHooks: string[];
    init(): void;
    protected successStatus(): Promise<void>;
    protected errorStatus(message: string): void;
    protected _init(event?: {
        type: string;
    } | null): void;
    protected _initDeny(event?: {
        type: string;
    } | null): void;
    protected _initDeepWorkflow(event?: {
        type: string;
    } | null): void;
    protected _initDenyDeepWorkflow(event?: {
        type: string;
    } | null): void;
    /**
     * Initialize once >> only for success status
    * */
    protected _instanceSuccessNext(): void;
    /**
     * Initialize once >> only for error status
     * */
    protected _instanceErrorNext(): void;
    /**
     * For main flow
     * Initialized (every times) if call hook includes in the `accessTypeHooks` >> only for success status
     * */
    protected _initNext(): void;
    /**
     * For main flow
     * Initialized (every times) if call hook includes in the `accessTypeHooks` >> only for error status
     * */
    protected _initDenyNext(): void;
    /**
     * For deep flow
     * Initialized (every times) if call hook includes in the `accessDeepWorkflowTypeHooks` >> only for success status
     * */
    protected _initDeepWorkflowNext(): void;
    /**
     * For deep flow
     * Initialized (every times) if call hook includes in the `accessDeepWorkflowTypeHooks` >> only for error status
     * */
    protected _initDenyDeepWorkflowNext(): void;
    /**
     * Async top loaders before ready all flows
     * */
    protected abstract loadAccess(): Promise<void>;
    /**
     * Top level clean
     * */
    protected clean(): void;
    /**
     * For main flow
     * Initialized (every times) before `_initNext` method >> only for success status
     * */
    protected deepClean(): void;
    /**
     * For deep flow
     * Initialized (every times) before `_initDeepWorkflowNext` method >> only for success status
     * */
    protected deepWorkflowClean(): void;
}
