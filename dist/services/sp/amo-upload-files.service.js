import { getAPP } from "../../configs/core.config";
export class AmoUploadFilesService {
    constructor() {
        this._fnWarning = () => { };
        this._fnError = () => { };
    }
    async down() {
    }
    async up() {
    }
    set fnWarning(value) {
        this._fnWarning = value;
    }
    set fnError(value) {
        this._fnError = value;
    }
    async push(chunks, fnLoadFile, fnResolveName, type = 'sendMail') {
        if (!chunks.length) {
            this._fnWarning('Внимание', 'Сначала выберите один или несколько файлов');
            return;
        }
        let switcher;
        switch (type) {
            case 'sendMail':
                switcher = document.querySelector('.js-switcher-email:not(.hidden)');
                break;
            default:
            case 'sendChat':
                switcher = document.querySelector('.js-switcher-chat:not(.hidden)');
        }
        if (!switcher) {
            this._fnWarning('Внимание', 'Отсутствуют или не выбраны каналы для отправки файлов');
            return;
        }
        try {
            for (let chunk of chunks) {
                await this.executeFiles(chunk, switcher, fnLoadFile, fnResolveName);
            }
        }
        catch (e) {
            this._fnError('Ошибка', e.message);
        }
    }
    async executeFiles(files, switcher, fnLoadFile, fnResolveName) {
        var _a;
        let results = await Promise.all(files.map(fnLoadFile));
        let feed = (_a = document.querySelector('.notes-wrapper')) === null || _a === void 0 ? void 0 : _a.querySelector('.feed-compose');
        if (feed === null || feed === void 0 ? void 0 : feed.classList.contains('minimized')) {
            switcher.click();
        }
        this.executeToEmail(results
            .map((blob, index) => ({ blob, file: files[index] }))
            .filter(({ blob, file }) => {
            if (blob) {
                blob.name = fnResolveName(file);
                return true;
            }
            this._fnError('Ошибка', `Не удалось загрузить файл ${fnResolveName(file)} с Google drive`);
            return false;
        }));
    }
    executeToEmail(data) {
        var _a, _b, _c, _d, _e;
        if (!data.length) {
            return;
        }
        let active_view = (_e = (_d = (_c = (_b = (_a = getAPP()) === null || _a === void 0 ? void 0 : _a.data) === null || _b === void 0 ? void 0 : _b.card_page) === null || _c === void 0 ? void 0 : _c.notes) === null || _d === void 0 ? void 0 : _d._compose) === null || _e === void 0 ? void 0 : _e._active_view;
        if (!(active_view === null || active_view === void 0 ? void 0 : active_view.file_uploader)) {
            throw new Error('Не удалось прикрепить файлы');
        }
        let attachments = {};
        if (active_view._attachments && typeof active_view._attachments === 'object') {
            for (let key in active_view._attachments) {
                if (active_view._attachments.hasOwnProperty(key) &&
                    active_view._attachments[key].name &&
                    data.some(file => active_view._attachments[key].name != file.blob.name)) {
                    attachments[key] = active_view._attachments[key];
                }
            }
        }
        active_view._attachments = attachments;
        let files_queue = [];
        if (active_view.file_uploader._files_queue && Array.isArray(active_view.file_uploader._files_queue)) {
            active_view.file_uploader._files_queue.forEach(item => {
                if (item.name && data.some(file => item.name != file.blob.name)) {
                    files_queue.push(item);
                }
            });
            active_view.file_uploader._files_queue = files_queue;
        }
        if (!active_view.file_uploader.onFileApiElDrop) {
            throw new Error('Не удалось прикрепить файлы');
        }
        active_view.file_uploader.onFileApiElDrop(data.map(item => item.blob));
    }
}
//# sourceMappingURL=amo-upload-files.service.js.map