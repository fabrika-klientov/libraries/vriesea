import { Observable, Subject } from 'rxjs';
import Echo from 'laravel-echo';
import { Channel, PresenceChannel } from 'laravel-echo/dist/channel';
import { LifeServiceContract } from '../../contracts/life-service.contract';
export type channelType = 'presence' | 'private' | 'public';
export declare class PusherService implements LifeServiceContract {
    private readonly $pusher;
    private $contextChannels;
    private $globalSubjects;
    private $contextSubjects;
    protected unsubscribeEvent: Subject<void>;
    protected unsubscribeContextEvent: Subject<void>;
    constructor();
    down(): Promise<void>;
    up(): Promise<void>;
    fullDown(): Promise<void>;
    get pusher(): Echo;
    channel(channel: string, type?: channelType): Channel | PresenceChannel | null;
    leaveChannel(channel: any): void;
    listen<T>(channel: string, event: string, type?: channelType): Observable<T>;
    contextChannel(channel: string, type?: channelType): Channel | PresenceChannel | null;
    contextListen<T>(channel: string, event: string, type?: channelType): Observable<T>;
    private _listen;
    private getObservable;
    protected static addToSubjectsList(list: Map<string, Subject<any>[]>, key: string, subject: Subject<any>): void;
}
