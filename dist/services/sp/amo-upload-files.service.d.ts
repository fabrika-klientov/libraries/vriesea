import { LifeServiceContract } from '../../contracts/life-service.contract';
export declare class AmoUploadFilesService implements LifeServiceContract {
    private _fnWarning;
    private _fnError;
    constructor();
    down(): Promise<void>;
    up(): Promise<void>;
    set fnWarning(value: (title: string, description: string) => void);
    set fnError(value: (title: string, description: string) => void);
    push<T>(chunks: T[][], fnLoadFile: (file: T) => Promise<any>, fnResolveName: (file: T) => string, type?: 'sendMail' | 'sendChat'): Promise<void>;
    protected executeFiles<T>(files: T[], switcher: HTMLElement, fnLoadFile: (file: T) => Promise<any>, fnResolveName: (file: T) => string): Promise<void>;
    protected executeToEmail(data: any[]): void;
}
