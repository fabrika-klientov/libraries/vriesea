import { Subject, takeUntil } from 'rxjs';
import { entityConvert, getCardEntityName } from '../../helpers/amo-instance.helper';
import { Widget } from '../../modules/classes/widget';
import { getAPP } from "../../configs/core.config";
export class EntitiesMergingService {
    constructor() {
        this.$merge = new Subject();
        this.$unmerge = new Subject();
        this.unsubscribeEvent = new Subject();
        this.mergeListener = () => {
            var _a;
            let modal = document.querySelector('.modal-entity-merge');
            if (!modal) {
                return;
            }
            let form = modal.querySelector('.merge-form__container form');
            if (!form) {
                return;
            }
            let listMerging = Array.from(form.querySelectorAll('input[name="id[]"]'))
                .map(item => +item.value);
            let resultId = Array.from(form.querySelectorAll('input[name="result_element[NAME]"]'))
                .filter(item => item.checked)
                .map(item => item.closest('div[data-field-code="NAME"]'))
                .filter(item => item)
                .map(item => item.getAttribute('data-contact-id'))
                .map(item => +item)
                .shift();
            let resultName = (_a = form.querySelector('.merge-form__table-headcol .form-result[data-for="NAME"] span')) === null || _a === void 0 ? void 0 : _a.innerText;
            let entity = entityConvert(Widget.context.area);
            entity = entity === 'contacts' ? (modal.classList.contains('modal-contacts-merge') ? entity : 'companies') : entity;
            if (listMerging.length && resultId && entity && resultName) {
                this.$merge.next({
                    type: 'merge',
                    entity,
                    listMerging,
                    resultId,
                    resultName,
                });
            }
        };
        this.unmergeListener = () => {
            let saveBtn = document.querySelector('.modal-body .modal-body__actions .modal-body__actions__save');
            if (!saveBtn) {
                return;
            }
            saveBtn.addEventListener('click', () => {
                let entity = entityConvert(Widget.context.area);
                let resultId = +getAPP().data.current_card.id;
                if (resultId && entity) {
                    this.$unmerge.next({
                        type: 'unmerge',
                        entity,
                        resultId,
                        resultName: getCardEntityName(),
                    });
                }
            });
        };
    }
    async down() {
        this.unsubscribeEvent.next();
        let jBody = $('body');
        jBody.off('click', '.modal-entity-merge .modal-body__actions__save', this.mergeListener);
        jBody.off('click', '.feed-note-wrapper-merge .feed-note__action-button', this.unmergeListener);
    }
    async up() {
        this.doMergeListen();
        this.doUnmergeListen();
    }
    mergeObserver(contextual = true) {
        if (contextual) {
            return this.getObservable(this.$merge);
        }
        return this.$merge.asObservable();
    }
    unmergeObserver(contextual = true) {
        if (contextual) {
            return this.getObservable(this.$unmerge);
        }
        return this.$unmerge.asObservable();
    }
    doMergeListen() {
        $('body').on('click', '.modal-entity-merge .modal-body__actions__save', this.mergeListener);
    }
    ;
    doUnmergeListen() {
        $('body').on('click', '.feed-note-wrapper-merge .feed-note__action-button', this.unmergeListener);
    }
    getObservable(subject) {
        return subject
            .asObservable()
            .pipe(takeUntil(this.unsubscribeEvent));
    }
}
//# sourceMappingURL=entities-merging.service.js.map