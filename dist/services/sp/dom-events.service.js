import { VRIESEA_CORE } from '../../configs/core.config';
const MAP_EVENT = {
    BODY_CLICK: 'body.click',
    RIGHT_WIDGET_HEAD_CLICK: 'right_widget_head.click',
};
export class DomEventsService {
    constructor() {
        this.$singletons = new Map();
        this.$contexts = new Map();
        this.bodyClickSingletonListener = ev => this.work(MAP_EVENT.BODY_CLICK, ev, null, true);
        this.bodyClickContextListener = ev => this.work(MAP_EVENT.BODY_CLICK, ev);
        this.rightWidgetHeadClickContextListener = ev => {
            let arrow = ev.currentTarget.querySelector('.card-widgets__widget__caption__arrow');
            setTimeout(() => {
                if (arrow) {
                    this.work(MAP_EVENT.RIGHT_WIDGET_HEAD_CLICK, ev, { expanded: arrow.classList.contains('widgets__widget__caption__arrow_top') });
                    return;
                }
                this.work(MAP_EVENT.RIGHT_WIDGET_HEAD_CLICK, ev);
            });
        };
        this.initialize();
    }
    initialize() {
        this.singletonListeners();
    }
    async down() {
        this.removeContextualListeners();
        this.$contexts.clear();
    }
    async up() {
        this.contextualListeners();
    }
    singletonListeners() {
        document.body.addEventListener('click', this.bodyClickSingletonListener, false);
    }
    contextualListeners() {
        document.body.addEventListener('click', this.bodyClickContextListener, false);
        let headOfRightWidget = document.querySelector(`.head-${VRIESEA_CORE().codeWidget}`);
        if (headOfRightWidget) {
            headOfRightWidget.addEventListener('click', this.rightWidgetHeadClickContextListener, false);
        }
    }
    removeSingletonListeners() {
        document.body.removeEventListener('click', this.bodyClickSingletonListener, false);
    }
    removeContextualListeners() {
        document.body.removeEventListener('click', this.bodyClickContextListener, false);
        let headOfRightWidget = document.querySelector(`.head-${VRIESEA_CORE().codeWidget}`);
        if (headOfRightWidget) {
            headOfRightWidget.removeEventListener('click', this.rightWidgetHeadClickContextListener, false);
        }
    }
    addClickBodyListener(callback, singleton = false) {
        this.registerListener(MAP_EVENT.BODY_CLICK, callback, singleton);
    }
    removeClickBodyListener(callback, singleton = false) {
        this.unregisterListener(MAP_EVENT.BODY_CLICK, callback, singleton);
    }
    addClickHeadWidgetRightListener(callback) {
        this.registerListener(MAP_EVENT.RIGHT_WIDGET_HEAD_CLICK, callback, false);
    }
    removeClickHeadWidgetRightListener(callback) {
        this.unregisterListener(MAP_EVENT.RIGHT_WIDGET_HEAD_CLICK, callback, false);
    }
    registerListener(code, callback, singleton) {
        let map = this.getMap(singleton);
        if (!map.has(code)) {
            map.set(code, []);
        }
        map.get(code).push(callback);
    }
    unregisterListener(code, callback, singleton) {
        let map = this.getMap(singleton);
        if (!map.has(code)) {
            return;
        }
        let list = map.get(code);
        let index = list.indexOf(callback);
        if (index !== -1) {
            list.splice(index, 1);
        }
    }
    work(code, ev, additional = null, singleton = false) {
        let map = this.getMap(singleton);
        if (map.has(code)) {
            map.get(code).forEach(callback => callback(ev, additional));
        }
    }
    getMap(singleton) {
        switch (singleton) {
            case true:
                return this.$singletons;
            case false:
            default:
                return this.$contexts;
        }
    }
}
//# sourceMappingURL=dom-events.service.js.map