import { Observable, Subject } from 'rxjs';
import { LifeServiceContract } from '../../contracts/life-service.contract';
import { AmoEntity } from '../../contracts/amo-instances.types';
export interface EntitiesMergeEvent {
    type: 'merge';
    entity: AmoEntity;
    listMerging: number[];
    resultId: number;
    resultName: string;
}
export interface EntitiesUnmergeEvent {
    type: 'unmerge';
    entity: AmoEntity;
    resultId: number;
    resultName: string;
}
export declare class EntitiesMergingService implements LifeServiceContract {
    private $merge;
    private $unmerge;
    protected unsubscribeEvent: Subject<void>;
    protected mergeListener: () => void;
    protected unmergeListener: () => void;
    constructor();
    down(): Promise<void>;
    up(): Promise<void>;
    mergeObserver(contextual?: boolean): Observable<EntitiesMergeEvent>;
    unmergeObserver(contextual?: boolean): Observable<EntitiesUnmergeEvent>;
    private doMergeListen;
    private doUnmergeListen;
    private getObservable;
}
