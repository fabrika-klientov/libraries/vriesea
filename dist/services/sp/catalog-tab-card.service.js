import { Subject, takeUntil, throwError } from 'rxjs';
export class CatalogTabCardService {
    constructor() {
        this.$observers = new Map();
        this.unsubscribeEvent = new Subject();
    }
    async down() {
        this.unsubscribeEvent.next();
        this.$observers.clear();
    }
    async up() {
    }
    getObserver(id) {
        let strId = `${id}`;
        if (this.$observers.has(strId)) {
            return this.getObservable(this.$observers.get(strId));
        }
        let subject = new Subject();
        this.$observers.set(strId, subject);
        let tabArea = document.getElementById(strId);
        if (tabArea) {
            this.doListen(subject, id, tabArea);
        }
        else {
            let tab = document.querySelector(`.card-tabs__item[data-id="${id}"]`);
            if (!tab) {
                return throwError(() => new Error(`Add list [${id}] to this card.`));
            }
            let listener = () => {
                tab.removeEventListener('click', listener, false);
                let tabArea = document.getElementById(strId);
                this.doListen(subject, id, tabArea);
            };
            tab.addEventListener('click', listener, false);
        }
        return this.getObservable(subject);
    }
    doListen(subject, catalogId, tabArea) {
        // delete
        $(tabArea).on('mouseup', '.fields_wrapper .linked-form__field__more', ev => {
            subject.next({
                type: 'remove',
                catalogId: +catalogId,
                elementId: +ev.target.closest('.catalog-fields__container').getAttribute('data-id'),
            });
        });
        // change quantity
        $(tabArea).on('change', '.fields_wrapper input.js-change-quantity', ev => {
            subject.next({
                type: 'quantity',
                catalogId: +catalogId,
                elementId: +ev.target.closest('.catalog-fields__container').getAttribute('data-id'),
                quantity: +ev.target.value,
            });
        });
        // add
        $(tabArea).on('click', '.add_new_element .catalog-fields__search ul .catalog-fields__container', ev => {
            subject.next({
                type: 'add',
                catalogId: +catalogId,
                elementId: +ev.target.closest('.catalog-fields__container').getAttribute('data-value-id'),
                quantity: 1,
            });
        });
    }
    ;
    getObservable(subject) {
        return subject
            .asObservable()
            .pipe(takeUntil(this.unsubscribeEvent));
    }
}
//# sourceMappingURL=catalog-tab-card.service.js.map