import { Subject, takeUntil, throwError } from 'rxjs';
import Echo from 'laravel-echo';
import clientSocketIO from 'socket.io-client';
import clientPusher from 'pusher-js';
import { VRIESEA_CORE as CORE } from '../../configs/core.config';
export class PusherService {
    constructor() {
        this.$contextChannels = [];
        this.$globalSubjects = new Map();
        this.$contextSubjects = new Map();
        this.unsubscribeEvent = new Subject();
        this.unsubscribeContextEvent = new Subject();
        let config = CORE().pusher;
        let authEndpoint = `${CORE().server.replace(/\/?$/, '')}/broadcasting/auth`;
        let client = null;
        switch (config.broadcaster) {
            case 'pusher':
                client = new clientPusher(config.key, { ...config, authEndpoint });
                break;
            case 'socket.io':
                client = clientSocketIO;
        }
        this.$pusher = new Echo({ client, authEndpoint, ...config });
    }
    async down() {
        this.$contextChannels.forEach(item => this.$pusher.leave(item));
        this.$contextSubjects.forEach(item => item.forEach(one => one.unsubscribe()));
        this.unsubscribeContextEvent.next();
        this.$contextChannels = [];
        this.$contextSubjects.clear();
    }
    async up() {
    }
    async fullDown() {
        await this.down();
        this.$globalSubjects.forEach(item => item.forEach(one => one.unsubscribe()));
        this.unsubscribeEvent.next();
        this.$globalSubjects.clear();
    }
    get pusher() {
        return this.$pusher;
    }
    channel(channel, type = 'public') {
        switch (type) {
            case 'public':
                return this.$pusher.channel(channel);
            case 'presence':
                return this.$pusher.join(channel);
            case 'private':
                return this.$pusher.private(channel);
            default:
                return null;
        }
    }
    leaveChannel(channel) {
        this.$pusher.leave(channel);
    }
    listen(channel, event, type = 'public') {
        return this._listen(channel, event, type, false);
    }
    contextChannel(channel, type = 'public') {
        let regChannel = this.channel(channel, type);
        if (!regChannel) {
            return null;
        }
        if (!this.$contextChannels.includes(channel)) {
            this.$contextChannels.push(channel);
        }
        return regChannel;
    }
    contextListen(channel, event, type = 'public') {
        return this._listen(channel, event, type, true);
    }
    _listen(channel, event, type, context) {
        let regChannel = this.channel(channel, type);
        if (!regChannel) {
            return throwError(() => new Error(`The channel [${channel}] can\'t register.`));
        }
        switch (type) {
            case 'public':
            case 'presence':
            case 'private':
                let subject = new Subject();
                PusherService.addToSubjectsList(context ? this.$contextSubjects : this.$globalSubjects, `${channel}::${event}`, subject);
                let cb = data => {
                    if (subject.closed) {
                        regChannel.stopListening(event, cb);
                        return;
                    }
                    subject.next(data);
                };
                regChannel.listen(event, cb);
                return this.getObservable(subject, context);
            default:
                return throwError(() => new Error(`The type of channel [${type}] can\'t resolve.`));
        }
    }
    getObservable(subject, context) {
        return subject
            .asObservable()
            .pipe(takeUntil(context ? this.unsubscribeContextEvent : this.unsubscribeEvent));
    }
    static addToSubjectsList(list, key, subject) {
        if (!list.has(key)) {
            list.set(key, []);
        }
        list.get(key).push(subject);
    }
}
//# sourceMappingURL=pusher.service.js.map