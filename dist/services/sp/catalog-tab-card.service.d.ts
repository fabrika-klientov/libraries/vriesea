import { Observable, Subject } from 'rxjs';
import { LifeServiceContract } from '../../contracts/life-service.contract';
export interface ChangeCatElEvent {
    type: 'add' | 'remove' | 'quantity';
    catalogId: number;
    elementId: number;
    quantity?: number;
}
export declare class CatalogTabCardService implements LifeServiceContract {
    private $observers;
    protected unsubscribeEvent: Subject<void>;
    constructor();
    down(): Promise<void>;
    up(): Promise<void>;
    getObserver(id: number | string): Observable<ChangeCatElEvent>;
    private doListen;
    private getObservable;
}
