import { LifeServiceContract } from '../../contracts/life-service.contract';
export declare class DomEventsService implements LifeServiceContract {
    private $singletons;
    private $contexts;
    protected bodyClickSingletonListener: (ev: any) => void;
    protected bodyClickContextListener: (ev: any) => void;
    protected rightWidgetHeadClickContextListener: (ev: any) => void;
    constructor();
    protected initialize(): void;
    down(): Promise<void>;
    up(): Promise<void>;
    protected singletonListeners(): void;
    protected contextualListeners(): void;
    protected removeSingletonListeners(): void;
    protected removeContextualListeners(): void;
    addClickBodyListener(callback: (event?: Event) => any, singleton?: boolean): void;
    removeClickBodyListener(callback: (event?: Event) => any, singleton?: boolean): void;
    addClickHeadWidgetRightListener(callback: (event?: Event, additional?: {
        expanded: boolean;
    }) => any): void;
    removeClickHeadWidgetRightListener(callback: (event?: Event, additional?: {
        expanded: boolean;
    }) => any): void;
    protected registerListener(code: string, callback: (event?: Event, additional?: any) => any, singleton: boolean): void;
    protected unregisterListener(code: string, callback: (event?: Event, additional?: any) => any, singleton: boolean): void;
    private work;
    private getMap;
}
