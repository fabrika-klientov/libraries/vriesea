import AppStatus from '../storage/modules/app-status';
export declare abstract class BaseAppCoreStatusesService {
    protected appStatus: AppStatus;
    protected checkStatus(): Promise<void>;
    protected successStatus(): void;
    protected errorStatus(message: string): void;
    protected beforeCheckStatus(): Promise<void>;
}
