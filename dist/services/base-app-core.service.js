import Vue from 'vue';
import { Widget } from '../modules/classes/widget';
import { cleanStore } from '../storage/store';
import { BaseAppCoreStatusesService } from './base-app-core-statuses.service';
export class BaseAppCoreService extends BaseAppCoreStatusesService {
    constructor() {
        super(...arguments);
        this.renderTypeHook = 'render';
        this.accessTypeHooks = ['render'];
        this.renderDeepWorkflowTypeHook = null;
        this.accessDeepWorkflowTypeHooks = [];
    }
    init() {
        this.clean();
        this.checkStatus();
    }
    async successStatus() {
        await this.loadAccess();
        Widget.$hooks.subscribe(this._init.bind(this));
        this._init({ type: this.renderTypeHook });
        if (this.renderDeepWorkflowTypeHook) {
            Widget.$hooks.subscribe(this._initDeepWorkflow.bind(this));
            this._initDeepWorkflow({ type: this.renderDeepWorkflowTypeHook });
        }
        this._instanceSuccessNext();
    }
    errorStatus(message) {
        Widget.$hooks.subscribe(this._initDeny.bind(this));
        this._initDeny({ type: this.renderTypeHook });
        if (this.renderDeepWorkflowTypeHook) {
            Widget.$hooks.subscribe(this._initDenyDeepWorkflow.bind(this));
            this._initDenyDeepWorkflow({ type: this.renderDeepWorkflowTypeHook });
        }
        this._instanceErrorNext();
    }
    _init(event = null) {
        if (!this.accessTypeHooks.includes(event === null || event === void 0 ? void 0 : event.type)) {
            return;
        }
        this.deepClean();
        this._initNext();
    }
    _initDeny(event = null) {
        if (!this.accessTypeHooks.includes(event === null || event === void 0 ? void 0 : event.type)) {
            return;
        }
        this._initDenyNext();
    }
    _initDeepWorkflow(event = null) {
        if (!this.accessDeepWorkflowTypeHooks.includes(event === null || event === void 0 ? void 0 : event.type)) {
            return;
        }
        this.deepWorkflowClean();
        this._initDeepWorkflowNext();
    }
    _initDenyDeepWorkflow(event = null) {
        if (!this.accessDeepWorkflowTypeHooks.includes(event === null || event === void 0 ? void 0 : event.type)) {
            return;
        }
        this._initDenyDeepWorkflowNext();
    }
    /**
     * Initialize once >> only for success status
    * */
    _instanceSuccessNext() {
        // TODO: override
    }
    /**
     * Initialize once >> only for error status
     * */
    _instanceErrorNext() {
        // TODO: override
    }
    /**
     * For main flow
     * Initialized (every times) if call hook includes in the `accessTypeHooks` >> only for success status
     * */
    _initNext() {
        // TODO: override
    }
    /**
     * For main flow
     * Initialized (every times) if call hook includes in the `accessTypeHooks` >> only for error status
     * */
    _initDenyNext() {
        // TODO: override
    }
    /**
     * For deep flow
     * Initialized (every times) if call hook includes in the `accessDeepWorkflowTypeHooks` >> only for success status
     * */
    _initDeepWorkflowNext() {
        // TODO: override
    }
    /**
     * For deep flow
     * Initialized (every times) if call hook includes in the `accessDeepWorkflowTypeHooks` >> only for error status
     * */
    _initDenyDeepWorkflowNext() {
        // TODO: override
    }
    /**
     * Top level clean
     * */
    clean() {
    }
    /**
     * For main flow
     * Initialized (every times) before `_initNext` method >> only for success status
     * */
    deepClean() {
        cleanStore();
        let sp = Vue.prototype.$vrieseaProvider;
        if (sp) {
            sp.up();
        }
    }
    /**
     * For deep flow
     * Initialized (every times) before `_initDeepWorkflowNext` method >> only for success status
     * */
    deepWorkflowClean() {
        // TODO: override if need
    }
}
//# sourceMappingURL=base-app-core.service.js.map