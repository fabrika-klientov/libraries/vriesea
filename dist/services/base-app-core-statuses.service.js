import { getModule } from 'vuex-module-decorators';
import { VRIESEA_CORE as CORE } from '../configs/core.config';
import AppStatus from '../storage/modules/app-status';
import { $store } from '../storage/store';
export class BaseAppCoreStatusesService {
    constructor() {
        this.appStatus = getModule(AppStatus, $store);
    }
    async checkStatus() {
        await this.beforeCheckStatus();
        let status = await this.appStatus.loadStatus();
        if (status.type === 'error') {
            let message = await this.appStatus.messageError(status.code);
            console.warn(CORE().codeLogging + message);
            this.errorStatus(message);
            return;
        }
        this.successStatus();
    }
    successStatus() {
    }
    errorStatus(message) {
    }
    async beforeCheckStatus() {
    }
}
//# sourceMappingURL=base-app-core-statuses.service.js.map