export class ServiceProvider {
    constructor(list) {
        this.$registered = [];
        this.$contexts = new Map();
        list.forEach(async (item) => {
            this.$registered.push(item.class);
            if (item.force) {
                let service = new item.class();
                await service.up();
                this.$contexts.set(item.class, service);
            }
        });
    }
    up() {
        this.$contexts.forEach(async (item) => {
            await item.down();
            await item.up();
        });
    }
    async getService(className, singleton = true) {
        if (!this.isRegistered(className)) {
            throw new Error(`Service class [${className}] did not register.`);
        }
        if (!singleton) {
            let service = new className();
            await service.up();
            return service;
        }
        if (this.$contexts.has(className)) {
            return this.$contexts.get(className);
        }
        let service = new className();
        await service.up();
        this.$contexts.set(className, service);
        return service;
    }
    isRegistered(className) {
        return this.$registered.includes(className);
    }
}
//# sourceMappingURL=service-provider.js.map