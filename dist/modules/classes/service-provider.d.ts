import { LifeServiceContract } from '../../contracts/life-service.contract';
export interface RegisterFactory {
    class: ConstructorOf<LifeServiceContract>;
    force?: boolean;
}
export declare class ServiceProvider {
    private $registered;
    private $contexts;
    constructor(list: RegisterFactory[]);
    up(): void;
    getService<T extends LifeServiceContract>(className: ConstructorOf<T>, singleton?: boolean): Promise<T>;
    protected isRegistered<T extends LifeServiceContract>(className: ConstructorOf<T>): boolean;
}
