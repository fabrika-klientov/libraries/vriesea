import { ParamsWidgetModel, WidgetModel } from '../../contracts/widget.model';
import { Observable } from 'rxjs';
export declare class Widget {
    private _instance;
    private static _CONTEXT;
    constructor(_instance: WidgetModel);
    get area(): string;
    get system(): any;
    get settings(): any;
    setSettings(data: any): void;
    get account(): any;
    get params(): ParamsWidgetModel;
    i18n(key: string): any;
    render(data: any, params: any): any;
    renderTemplate(config: any): any;
    get modal(): any;
    static get context(): Widget;
    static get $hooks(): Observable<{
        type: string;
        data?: any;
    }>;
}
