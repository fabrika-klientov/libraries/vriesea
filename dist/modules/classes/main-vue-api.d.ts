import { AxiosRequestConfig } from 'axios';
import { Observable } from 'rxjs';
import { HttpClientContract } from '../../contracts/http-client.contract';
export declare class MainVueApi implements HttpClientContract {
    protected readonly _backend: string;
    protected readonly _token: string;
    protected readonly _prefix: string;
    protected static CONTEXT: MainVueApi;
    constructor();
    get(link: string, params?: any, headers?: {}, config?: AxiosRequestConfig): Observable<any>;
    post(link: string, data: any, headers?: {}, config?: AxiosRequestConfig): Observable<any>;
    patch(link: string, data: any, headers?: {}, config?: AxiosRequestConfig): Observable<any>;
    delete(link: string, params?: any, headers?: {}, config?: AxiosRequestConfig): Observable<any>;
    link(suffix: string): string;
    headers(): any;
    handleResult(data: any): any;
    handleError(error: any): any;
    static get accountHost(): string;
    static get serverHost(): string;
    static get context(): MainVueApi;
}
