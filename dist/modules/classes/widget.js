export class Widget {
    constructor(_instance) {
        this._instance = _instance;
        Widget._CONTEXT = this;
    }
    get area() {
        return this.system.area;
    }
    get system() {
        return this._instance.system();
    }
    get settings() {
        return this._instance.get_settings();
    }
    setSettings(data) {
        return this._instance.set_settings(data);
    }
    get account() {
        return this._instance.get_accounts_current();
    }
    get params() {
        return this._instance.params;
    }
    i18n(key) {
        return this._instance.i18n(key);
    }
    render(data, params) {
        return this._instance.render(data, params);
    }
    renderTemplate(config) {
        return this._instance.render_template(config);
    }
    get modal() {
        return this._instance.aModal || null;
    }
    static get context() {
        return Widget._CONTEXT;
    }
    static get $hooks() {
        return Widget._CONTEXT._instance.$hooks.asObservable();
    }
}
//# sourceMappingURL=widget.js.map