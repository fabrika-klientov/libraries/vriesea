import axios from 'axios';
import { catchError, from, map, of } from 'rxjs';
import { VRIESEA_CORE as CORE } from '../../configs/core.config';
export class MainVueApi {
    constructor() {
        this._backend = CORE().server.replace(/\/?$/, '');
        this._prefix = CORE().api;
        this._token = CORE().userToken.trim();
        MainVueApi.CONTEXT = this;
    }
    get(link, params = {}, headers, config) {
        return from(axios.get(this.link(link), { headers: { ...this.headers(), ...headers }, params, ...config }))
            .pipe(map(this.handleResult), catchError(this.handleError));
    }
    post(link, data, headers, config) {
        return from(axios.post(this.link(link), data, { headers: { ...this.headers(), ...headers }, ...config }))
            .pipe(map(this.handleResult), catchError(this.handleError));
    }
    patch(link, data, headers, config) {
        return from(axios.patch(this.link(link), data, { headers: { ...this.headers(), ...headers }, ...config }))
            .pipe(map(this.handleResult), catchError(this.handleError));
    }
    delete(link, params = {}, headers, config) {
        return from(axios.delete(this.link(link), { headers: { ...this.headers(), ...headers }, params, ...config }))
            .pipe(map(this.handleResult), catchError(this.handleError));
    }
    link(suffix) {
        return (suffix.indexOf('http') === 0) ? suffix : (this._backend + this._prefix + suffix);
    }
    headers() {
        return {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + this._token,
        };
    }
    handleResult(data) {
        if (data && data.data) {
            return data.data;
        }
        return null;
    }
    handleError(error) {
        console.error(CORE().codeLogging + 'Request has error', error);
        return of(null);
    }
    static get accountHost() {
        return location.protocol + '//' + location.host;
    }
    static get serverHost() {
        return MainVueApi.CONTEXT._backend;
    }
    static get context() {
        return MainVueApi.CONTEXT;
    }
}
//# sourceMappingURL=main-vue-api.js.map