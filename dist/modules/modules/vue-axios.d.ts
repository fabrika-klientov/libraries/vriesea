import { HttpClientContract } from '../../contracts/http-client.contract';
export declare class VueAxios {
    static install(vue: any, options?: ConstructorOf<HttpClientContract>): void;
}
