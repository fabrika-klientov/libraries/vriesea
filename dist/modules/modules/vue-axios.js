import { MainVueApi } from '../classes/main-vue-api';
export class VueAxios {
    static install(vue, options) {
        vue.prototype.$httpClient = options ? new options() : new MainVueApi();
    }
}
//# sourceMappingURL=vue-axios.js.map