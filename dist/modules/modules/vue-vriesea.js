import Vue from 'vue';
import { defineVrieseaConfig } from '../../configs/core.config';
import { Widget } from '../classes/widget';
import { VueAxios } from './vue-axios';
import { modules as defModules } from '../../storage/modules-state';
import { $store, options as defOptions } from '../../storage/store';
import { VueServiceProvider } from './vue-service-provider';
export class VueVriesea {
    static install(vue, config) {
        vue.prototype.$vrieseaWidget = new Widget(config.widget);
        defineVrieseaConfig(config.config);
        this.registerModules(config.modules);
        this.registerStorageModules(config.storageRoot, config.storageModules);
    }
    static registerModules(cm) {
        var _a, _b;
        if (!cm) {
            return;
        }
        if ((_a = cm.vueApi) === null || _a === void 0 ? void 0 : _a.status) {
            Vue.use(VueAxios, cm.vueApi.additional);
        }
        if ((_b = cm.serviceProvider) === null || _b === void 0 ? void 0 : _b.status) {
            Vue.use(VueServiceProvider, cm.serviceProvider.additional || []);
        }
        // TODO: add another
    }
    static registerStorageModules(root, modules) {
        if ($store) { // do not change state after registered store
            return;
        }
        if (root) {
            for (let key in root) {
                defOptions[key] = root[key];
            }
        }
        if (modules) {
            for (let key in modules) {
                defModules[key] = modules[key];
            }
        }
    }
}
//# sourceMappingURL=vue-vriesea.js.map