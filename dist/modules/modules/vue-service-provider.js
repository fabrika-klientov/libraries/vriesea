import { ServiceProvider } from '../classes/service-provider';
export class VueServiceProvider {
    static install(vue, options) {
        vue.prototype.$vrieseaProvider = new ServiceProvider(options);
    }
}
//# sourceMappingURL=vue-service-provider.js.map