import { ModuleTree, StoreOptions } from 'vuex';
import { VrieseaConfig } from '../../configs/core.config';
import { WidgetModel } from '../../contracts/widget.model';
import { StoreState } from '../../storage/modules-state';
import { RegisterFactory } from '../classes/service-provider';
import { HttpClientContract } from '../../contracts/http-client.contract';
export interface VrieseaModuleConfig {
    widget: WidgetModel;
    config: VrieseaConfig;
    modules?: ConfigModules;
    storageRoot?: StoreOptions<StoreState | any>;
    storageModules?: ModuleTree<any>;
}
interface ConfigModules {
    vueApi?: ConfigModuleStatus<ConstructorOf<HttpClientContract>>;
    serviceProvider?: ConfigModuleStatus<RegisterFactory[]>;
}
interface ConfigModuleStatus<T> {
    status: boolean;
    additional?: T;
}
export declare class VueVriesea {
    static install(vue: any, config: VrieseaModuleConfig): void;
    static registerModules(cm: ConfigModules | undefined): void;
    static registerStorageModules(root: StoreOptions<StoreState | any> | undefined, modules: ModuleTree<any> | undefined): void;
}
export {};
