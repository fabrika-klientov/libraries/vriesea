export interface AmoTimelineModel {
    items: AmoTimelineEventModel<any>[];
    contacts: any[];
    leads: any[];
    companies: any[];
    tasks: any[];
    tasks_results: any[];
    customers: any[];
    catalog_elements: any[];
    account: AmoTimelineAccountModel;
    custom_fields: any[];
    types: AmoTimelineTypeModel[];
}
export interface AmoTimelineEventModel<T> {
    id: number | string;
    type: number;
    data: {
        text: string;
        params: T;
    };
    msec_created_at: number;
    date_create: number;
    element_id: number;
    element_type: number;
    created_by: number;
    date_modify: number;
    modified_by: number;
    responsible_user_id: number;
    _links: any;
    object_type: {
        id: number;
        code: string;
    };
    author_name: string;
    deletable: boolean;
    editable: boolean;
    responsible_user: number;
}
export interface ParamsChangeStatus {
    old_status: number;
    new_status: number;
    loss_reason_id: number;
    old_pipeline: number;
    pipeline: number;
}
export interface ParamsChangeCustomField {
    field_type: number;
    field_id: number;
    subtype_id: any;
    old_value: string;
    new_value: string;
    old_enum_id: any;
    new_enum_id: any;
    is_bonus_points: boolean;
}
export interface ParamsAddSystemNotice {
    service: string;
    icon_url?: string;
}
export interface ParamsChangeResponsible {
    old: number;
    new: number;
}
export interface AmoTimelineAccountModel {
    timezone: string;
    currency: string;
    _links: any;
    _embedded: {
        pipelines: any;
        status_pipeline: any;
        users: any;
        loss_reasons: any;
    };
}
export interface AmoTimelineTypeModel {
    id: number;
    name: string;
    is_present: boolean;
}
