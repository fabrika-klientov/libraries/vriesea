import { Observable } from 'rxjs';
export interface HttpClientContract {
    get(link: string, ...params: any): Observable<any>;
    post(link: string, data: any, ...params: any): Observable<any>;
    patch(link: string, data: any, ...params: any): Observable<any>;
    delete(link: string, ...params: any): Observable<any>;
}
