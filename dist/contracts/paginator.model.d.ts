export interface Paginator<T, U> {
    page: number;
    maxPage: number;
    inPage: number;
    pages: number[];
    total: number;
    data: T[];
    metadata?: U;
}
export interface PaginatorEloquent<T> {
    current_page: number;
    per_page: number;
    from: number;
    to: number;
    last_page: number;
    total: number;
    first_page_url: string;
    last_page_url: string;
    next_page_url: string;
    prev_page_url: string;
    path: string;
    links: any[];
    data: T[];
}
