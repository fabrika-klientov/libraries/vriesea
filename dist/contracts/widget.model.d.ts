import { CustomSettingsParamsModel } from './custom-settings-params.model';
import { Subject } from 'rxjs';
export interface WidgetModel {
    system(): any;
    get_settings(): any;
    render(data: any, params: any): any;
    i18n(key: string): any;
    set_lang(data: any): void;
    set_settings(data: any): void;
    get_accounts_current(): any;
    list_selected(): ListSelectedWidgetModel;
    widgetsOverlay(status: boolean): void;
    add_action(type: string, action: any): void;
    add_source(source_type: string, handler: any): void;
    set_status(status: string): void;
    crm_post(url: string, data?: any, callback?: any, type?: string): void;
    params: ParamsWidgetModel;
    langs: any;
    ns: string;
    aModal?: any;
    render_template(config: any): any;
    $hooks: Subject<{
        type: string;
        data?: any;
    }>;
}
type YN = 'Y' | 'N';
export interface ParamsWidgetModel extends CustomSettingsParamsModel {
    active: YN;
    id: string;
    images_path: string;
    path: string;
    widget_active: YN;
    widget_code: string;
}
export interface ListSelectedWidgetModel {
    selected: ItemSelectedWidgetModel[];
    summary: {
        emails: number;
        items: number;
        phones: number;
    };
}
export interface ItemSelectedWidgetModel {
    id: number;
    type: string;
    emails: any[];
    phones: any[];
}
export {};
