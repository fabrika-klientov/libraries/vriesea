import { AmoBaseEntityV4Model } from './amo-entity.model';
export interface AmoContactV4Model extends AmoBaseEntityV4Model {
    first_name: string;
    last_name: string;
    is_unsorted: boolean;
    _embedded: {
        tags: any[];
        companies: any[];
    };
}
