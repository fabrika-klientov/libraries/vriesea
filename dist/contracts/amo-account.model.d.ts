export interface AmoAccountModel {
    id: number;
    name: string;
    subdomain: string;
    created_at: number;
    created_by: number;
    updated_at: number;
    updated_by: number;
    current_user_id: number;
    country: string;
    currency: string;
    currency_symbol: string;
    customers_mode: string;
    is_unsorted_on: boolean;
    mobile_feature_version: number;
    is_loss_reason_enabled: boolean;
    is_helpbot_enabled: boolean;
    is_technical_account: boolean;
    contact_name_display_order: number;
    amojo_id?: string;
    version?: number;
    entity_names?: any;
    _embedded?: {
        amojo_rights?: AmojoRightsModel;
        users_groups?: UsersGroupModel[];
        task_types?: TaskTypeModel[];
        datetime_settings?: DatetimeSettingsModel;
    };
}
export interface AmojoRightsModel {
    can_direct: boolean;
    can_create_groups: boolean;
}
export interface UsersGroupModel {
    id: number;
    name: string;
}
export interface TaskTypeModel {
    id: number;
    name: string;
    color: string;
    icon_id: number | null;
    code: string;
}
export interface DatetimeSettingsModel {
    date_pattern: string;
    short_date_pattern: string;
    short_time_pattern: string;
    date_format: string;
    time_format: string;
    timezone: string;
    timezone_offset: string;
}
export interface AmoFrontAccountModel {
    amo_messenger: any;
    amojo_enabled: boolean;
    amojo_id: string;
    amojo_rights: any;
    amojo_server: string;
    catalogs_available: boolean;
    cf: {
        [key: number]: AmoFrontCfModel;
    };
    country: string;
    currency: string;
    customers_enabled: boolean;
    date_format: string;
    date_pattern: string;
    helpbot_enabled: boolean;
    id: number;
    invoices: any;
    is_chats_inbox_available: boolean;
    is_contact_name_display_order_first: boolean;
    is_retail_functions_enabled: boolean;
    is_rpa_available: boolean;
    language: string;
    name: string;
    notifications_enabled: boolean;
    paid_from: number;
    paid_till: number;
    pay_type: string;
    predefined_cf: {
        [key: string]: AmoFrontCfModel;
    };
    products: any;
    stand: string;
    subdomain: string;
    suppliers: string;
    talks_auto_close_delay: number;
    tariffName: string;
    time_format: string;
    timezone: string;
    top_level_domain: string;
    unsorted_on: string;
    users: {
        [key: number]: string;
    };
    version: number;
}
export interface AmoFrontCfModel {
    ACCOUNT_ID: number;
    CATALOG_ID: number;
    CODE: string;
    DESCRIPTION: string;
    DISABLED: number;
    ELEMENT_TYPES: any;
    ENTREE_CATALOG: number;
    ENTREE_COMPANY: number;
    ENTREE_CONTACTS: number;
    ENTREE_CUSTOMERS: number;
    ENTREE_DEALS: number;
    ENUMS?: {
        [key: number]: AmoFrontCfEnumModel;
    };
    ID: number;
    MULTIPLE: string;
    NAME: string;
    ORIGIN: string;
    PREDEFINED: string;
    SETTINGS: any;
    SORT: number;
    TYPE_ID: number;
    deleted_at: number;
}
export interface AmoFrontCfEnumModel {
    ID: number;
    SORT: number;
    TOTAL: number;
    VALUE: string;
    code: string | null;
}
