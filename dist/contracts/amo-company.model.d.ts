import { AmoBaseEntityV4Model } from './amo-entity.model';
export interface AmoCompanyV4Model extends AmoBaseEntityV4Model {
    _embedded: {
        tags: any[];
        contacts: any[];
    };
}
