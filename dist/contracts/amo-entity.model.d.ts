import { AmoCfV4Model } from './amo-cf.model';
export interface AmoBaseEntityV4Model {
    id: number;
    name: string;
    responsible_user_id: number;
    group_id: number;
    created_by: number;
    updated_by: number;
    created_at: number;
    updated_at: number;
    closest_task_at: number | null;
    is_deleted: boolean;
    custom_fields_values: AmoCfV4Model[];
    account_id: number;
    _links: any;
    _embedded: {
        tags: any[];
    };
}
export interface FindEntitiesQueryParams<T extends AmoBaseEntityV4Model> {
    query: string;
    filter?: (item: T) => boolean;
    forFieldID?: number;
    forFieldCode?: string;
}
