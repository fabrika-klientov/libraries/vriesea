const path = require('path');
const {DefinePlugin} = require('webpack');
const dotenv = require('dotenv');
const TerserPlugin = require("terser-webpack-plugin");

module.exports = {
    entry: './src/index.ts',
    
    output: {
        filename: 'app.js',
        path: path.resolve(__dirname, '../../../../../../../', 'dist'),
        libraryTarget: 'amd',
    },
    
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.s?[ac]ss$/i,
                use: [
                    'style-loader',
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                ],
            },
        ],
    },
    
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
    
    plugins: [
        new DefinePlugin({
            'process.env': JSON.stringify({...process.env, ...dotenv.config().parsed}),
        }),
    ],
    
    optimization: {
        minimizer: [
            new TerserPlugin({
                terserOptions: {
                    compress: process.env.VUE_APP_DEBUG === 'false' ? {
                        drop_console: true,
                        global_defs: {
                            '@console.log': 'console.debug',
                            '@console.warn': 'console.debug',
                            '@console.error': 'console.debug',
                        },
                    } : {},
                    output: {
                        comments: false,
                    },
                },
            }),
        ],
    },
    
    devtool: false,
    
    mode: 'production',
}
