const TerserPlugin = require('terser-webpack-plugin');
const path = require('path');

module.exports = {
    publicPath: process.env.VUE_APP_STATIC_PATH,
    
    filenameHashing: false,

    configureWebpack: {
        entry: {
            app: './src/index.ts',
        },
        output: {
            libraryTarget: 'amd',
        },
        module: {
            rules: [
                {
                    test: /component\.html$/,
                    loader: 'vue-template-loader',
                    options: {
                        transformToRequire: {
                            img: 'src',
                        },
                    },
                },
                {
                    test: /\.js$/i,
                    use: [
                        {
                            loader: path.resolve('node_modules/@fbkl/vriesea/dist/webpack/loaders/js/element-ui-loader'),
                            options: {
                                appendsClasses: [process.env.VUE_APP_MAIN_SELECTOR.replace(/[.#]/, '')],
                            },
                        },
                    ],
                }
            ],
        },
        optimization: {
            splitChunks: false,
            minimizer: [
                new TerserPlugin({
                    terserOptions: {
                        compress: process.env.VUE_APP_DEBUG === 'false' ? {
                            drop_console: true,
                            global_defs: {
                                '@console.log': 'console.debug',
                                '@console.warn': 'console.debug',
                                '@console.error': 'console.debug',
                            },
                        } : {},
                        output: {
                            comments: false,
                        },
                    },
                }),
            ],
        },
        devtool: false,
    },
    
    css: {
        extract: false,
    },
    
    chainWebpack: config => {
        config.plugins.delete('prefetch-app')
        config.plugins.delete('preload-app')
    },
    
    productionSourceMap: false,
}
// vue inspect --plugins
