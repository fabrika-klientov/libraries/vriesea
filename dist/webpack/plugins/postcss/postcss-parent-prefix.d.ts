import { Container } from 'postcss';
export declare const postcssParentPrefix: (prefix: string, options?: PostcssParentPrefixOptions) => (root: Container) => void;
export default postcssParentPrefix;
export interface PostcssParentPrefixOptions {
    sticky?: boolean;
    replace?: boolean;
    exclude?: (string | RegExp)[];
    only?: (string | RegExp)[];
    skipPrefix?: string;
    workSkipped?: boolean;
    isFilter?: boolean;
    filter?: (selector: string) => boolean;
    filterList?: (string | RegExp)[];
}
