"use strict";
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.postcssParentPrefix = void 0;
var postcssParentPrefix = function (prefix, options) {
    return function (root) {
        root.walkRules(function (rule) {
            if (!rule.selectors) {
                return;
            }
            options = options || {};
            var isFilter = options.isFilter, filter = options.filter, filterList = options.filterList, sticky = options.sticky, only = options.only, exclude = options.exclude, replace = options.replace, workSkipped = options.workSkipped;
            var selectors = rule.selectors
                .filter(function (selector) {
                if (!isFilter) {
                    return true;
                }
                if (filter) {
                    return filter(selector);
                }
                var skippedSelector = prepareSelector(selector, options);
                var workSelector = workSkipped ? skippedSelector : selector;
                if (filterList) {
                    return checkInList(workSelector, filterList);
                }
                return true;
            })
                .map(function (selector) {
                var skippedSelector = prepareSelector(selector, options);
                var workSelector = workSkipped ? skippedSelector : selector;
                if (only) {
                    if (checkInList(skippedSelector, only)) {
                        return doMerge(workSelector, prefix, !!sticky);
                    }
                    return workSelector;
                }
                if (exclude) {
                    if (!checkInList(skippedSelector, exclude)) {
                        return doMerge(workSelector, prefix, !!sticky);
                    }
                    return workSelector;
                }
                return doMerge(workSelector, prefix, !!sticky);
            });
            if (replace) {
                rule.selectors = selectors;
            }
            else {
                rule.selectors = __spreadArray(__spreadArray([], rule.selectors, true), selectors, true);
            }
        });
    };
};
exports.postcssParentPrefix = postcssParentPrefix;
exports.default = exports.postcssParentPrefix;
var prepareSelector = function (selector, _a) {
    var skipPrefix = _a.skipPrefix;
    var selectorSkipped = selector;
    if (skipPrefix && selectorSkipped.indexOf(skipPrefix) === 0) {
        selectorSkipped = selectorSkipped.substring(skipPrefix.length);
    }
    return selectorSkipped;
};
var checkInList = function (selector, list) {
    return list.some(function (item) { return typeof item === 'string' ? item.toUpperCase() === selector.toUpperCase() : item.test(selector); });
};
var doMerge = function (selector, prefix, sticky) {
    return "".concat(prefix).concat(sticky ? '' : ' ').concat(selector);
};
//# sourceMappingURL=postcss-parent-prefix.js.map