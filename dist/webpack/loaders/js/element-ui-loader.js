"use strict";
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.elementUiLoader = void 0;
var loader_utils_1 = require("loader-utils");
var elementUiLoader = function (source) {
    var _a;
    var options = (0, loader_utils_1.getOptions)(this);
    var remainingRequest = (0, loader_utils_1.getRemainingRequest)(this);
    if (!((_a = options.appendsClasses) === null || _a === void 0 ? void 0 : _a.length)) {
        return source;
    }
    if (/\/element-ui\//i.test(remainingRequest)) {
        source = doPopper(source, remainingRequest, options);
        source = doTooltip(source, remainingRequest, options);
        source = doDialog(source, remainingRequest, options);
        source = doColorPicker(source, remainingRequest, options);
        source = doTableFilter(source, remainingRequest, options);
        source = doMessage(source, remainingRequest, options);
        source = doMessageBox(source, remainingRequest, options);
        source = doNotification(source, remainingRequest, options);
        source = doMenuHorizontal(source, remainingRequest, options);
        source = doMenuVertical(source, remainingRequest, options);
        source = doDrawerWrapper(source, remainingRequest, options);
        source = doViewImageWrapper(source, remainingRequest, options);
    }
    return source;
};
exports.elementUiLoader = elementUiLoader;
var doPopper = function (source, path, options) {
    switch (true) {
        case /element-ui\.common\.js$/.test(path):
            source = replaceStaticClass(source, 'el-popper', options.appendsClasses);
            source = replaceVueArrayClasses(source, 'el-popper', options.appendsClasses);
            break;
        case /popover\.js$/.test(path):
        case /select\.js$/.test(path):
        case /autocomplete\.js$/.test(path):
        case /date-picker\.js$/.test(path):
        case /dropdown-menu\.js$/.test(path):
        case /time-picker\.js$/.test(path):
        case /time-select\.js$/.test(path):
            source = replaceStaticClass(source, 'el-popper', options.appendsClasses);
            break;
        case /cascader\.js$/.test(path):
            source = replaceVueArrayClasses(source, 'el-popper', options.appendsClasses);
    }
    return source;
};
var doTooltip = function (source, path, options) {
    switch (true) {
        case /tooltip\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceVueArrayClasses(source, 'el-tooltip__popper', options.appendsClasses);
    }
    return source;
};
var doDialog = function (source, path, options) {
    switch (true) {
        case /dialog\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceStaticClass(source, 'el-dialog__wrapper', options.appendsClasses);
            break;
        case /popup-manager\.js$/.test(path):
            source = source.replace(/(\(modalDom,\s*["'](v-modal)["']\))/, function (full, first, second) { return full.replace(second, __spreadArray(['v-modal'], options.appendsClasses, true).join(' ')); });
    }
    return source;
};
var doColorPicker = function (source, path, options) {
    switch (true) {
        case /color-picker\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceStaticClass(source, 'el-color-dropdown', options.appendsClasses);
    }
    return source;
};
var doTableFilter = function (source, path, options) {
    switch (true) {
        case /table\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceStaticClass(source, 'el-table-filter', options.appendsClasses);
    }
    return source;
};
var doMessage = function (source, path, options) {
    switch (true) {
        case /message\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceVueArrayClasses(source, 'el-message', options.appendsClasses);
    }
    return source;
};
var doMessageBox = function (source, path, options) {
    switch (true) {
        case /message-box\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceStaticClass(source, 'el-message-box__wrapper', options.appendsClasses);
    }
    return source;
};
var doNotification = function (source, path, options) {
    switch (true) {
        case /notification\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceVueArrayClasses(source, 'el-notification', options.appendsClasses);
    }
    return source;
};
var doMenuHorizontal = function (source, path, options) {
    switch (true) {
        case /menu\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceVueObjClasses(source, 'el-menu--horizontal', options.appendsClasses);
    }
    return source;
};
var doMenuVertical = function (source, path, options) {
    switch (true) {
        case /menu\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceVueObjClasses(source, 'el-menu--vertical', options.appendsClasses);
    }
    return source;
};
var doDrawerWrapper = function (source, path, options) {
    switch (true) {
        case /drawer\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceStaticClass(source, 'el-drawer__wrapper', options.appendsClasses);
    }
    return source;
};
var doViewImageWrapper = function (source, path, options) {
    switch (true) {
        case /image\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceStaticClass(source, 'el-image-viewer__wrapper', options.appendsClasses);
    }
    return source;
};
var replaceStaticClass = function (source, existClass, appendsClasses, withExist) {
    if (withExist === void 0) { withExist = true; }
    if (withExist) {
        appendsClasses = __spreadArray([existClass], appendsClasses, true);
    }
    return source.replace(new RegExp("([\"']?staticClass[\"']?:\\s*[\"'].*(\\b".concat(existClass, "\\b).*[\"'],?)"), 'g'), function (full, first, second) { return full.replace(second, appendsClasses.join(' ')); });
};
var replaceVueArrayClasses = function (source, existClass, appendsClasses, withExist) {
    if (withExist === void 0) { withExist = true; }
    if (withExist) {
        appendsClasses = __spreadArray([existClass], appendsClasses, true);
    }
    return source.replace(new RegExp("([\"']?class[\"']?:\\s*\\[.*([\"']".concat(existClass, "[\"']).*],?)"), 'gs'), function (full, first, second) { return full.replace(second, appendsClasses.map(function (item) { return "\"".concat(item, "\""); }).join(', ')); });
};
var replaceVueObjClasses = function (source, existClass, appendsClasses) {
    return source.replace(new RegExp("([\"']?class[\"']?:\\s*{.*([\"'](".concat(existClass, ")[\"']:[^,]+).*},?)"), 'gs'), function (full, first, second) { return full.replace(second, appendsClasses.map(function (item) { return "\"".concat(item, "\": true"); }).join(', ') + ', ' + second); });
};
exports.default = exports.elementUiLoader;
//# sourceMappingURL=element-ui-loader.js.map