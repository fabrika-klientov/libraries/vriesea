export interface ElementUILoaderOptions {
    appendsClasses?: string[];
}
export declare const elementUiLoader: (this: any, source: any) => any;
export default elementUiLoader;
