export declare const VRIESEA_CORE: () => {
    publicEnv: string | undefined;
    server: string;
    intCrimsonServer: string;
    proxyAmoServer: string;
    scriptVersion: string | undefined;
    codeWidget: string;
    api: string | undefined;
    paths: {
        generateKeyLink: string;
        disableLink: string;
        appStatus: string;
    };
    amoPaths: {
        linkCustomFields: string;
        users: string;
        account: string;
        catalogs: string;
        contactsApiV4: string;
        companiesApiV4: string;
        customersApiV4: string;
        customersAjaxV2: string;
        catalogElements: string;
        linkedCatalogElements: string;
        leadPipelines: string;
        customersStatuses: string;
        customersSegments: string;
        managersWithGroups: string;
        usersRoles: string;
        v4Cf: string;
        leadTags: string;
        customerTags: string;
        contactTags: string;
    };
    amoEntities: {
        leads: {
            detail: string;
        };
        customers: {
            detail: string;
        };
        contacts: {
            detail: string;
        };
        companies: {
            detail: string;
        };
    };
    pusher: any;
    userToken: string | undefined;
    coreClass: string;
    coreEl: string;
    codeLogging: string;
    textServer: string;
    linkDescription: string;
    linkShared: string;
    appPath: string;
    frontPath: string;
};
export interface VrieseaConfig {
    server?: string;
    crimsonServer?: string;
    proxyAmoServer?: string;
    userToken?: string;
    codeWidget?: string;
    api?: string;
    pusher?: any;
}
export declare const defineVrieseaConfig: (config: VrieseaConfig) => void;
export declare const getAPP: () => any;
