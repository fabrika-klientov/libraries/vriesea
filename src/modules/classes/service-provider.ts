import {LifeServiceContract} from '../../contracts/life-service.contract';

export interface RegisterFactory {
    class: ConstructorOf<LifeServiceContract>;
    force?: boolean;
}


export class ServiceProvider {

    private $registered: ConstructorOf<LifeServiceContract>[] = [];
    private $contexts: Map<ConstructorOf<LifeServiceContract>, LifeServiceContract> = new Map();

    constructor(list: RegisterFactory[]) {
        list.forEach(async item => {
            this.$registered.push(item.class);
            if (item.force) {
                let service = new item.class();
                await service.up();
                this.$contexts.set(item.class, service);
            }
        });
    }

    public up(): void {
        this.$contexts.forEach(async item => {
            await item.down();
            await item.up();
        });
    }

    public async getService<T extends LifeServiceContract>(className: ConstructorOf<T>, singleton: boolean = true): Promise<T> {
        if (!this.isRegistered(className)) {
            throw new Error(`Service class [${className}] did not register.`);
        }

        if (!singleton) {
            let service = new className();
            await service.up();

            return service;
        }

        if (this.$contexts.has(className)) {
            return this.$contexts.get(className) as T;
        }

        let service = new className();
        await service.up();
        this.$contexts.set(className, service);

        return service;
    }

    protected isRegistered<T extends LifeServiceContract>(className: ConstructorOf<T>): boolean {
        return this.$registered.includes(className);
    }
}
