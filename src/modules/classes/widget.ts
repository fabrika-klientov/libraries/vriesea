import {ParamsWidgetModel, WidgetModel} from '../../contracts/widget.model';
import {Observable} from 'rxjs';

export class Widget {

    private static _CONTEXT: Widget;
    
    constructor(private _instance: WidgetModel) {
        Widget._CONTEXT = this;
    }
    
    public get area(): string {
        return this.system.area;
    }

    public get system(): any {
        return this._instance.system();
    }
    
    public get settings(): any {
        return this._instance.get_settings();
    }

    public setSettings(data: any): void {
        return this._instance.set_settings(data);
    }

    public get account(): any {
        return this._instance.get_accounts_current();
    }

    public get params(): ParamsWidgetModel {
        return this._instance.params;
    }

    public i18n(key: string): any {
        return this._instance.i18n(key);
    }

    public render(data: any, params: any): any {
        return this._instance.render(data, params);
    }

    public renderTemplate(config: any) {
        return this._instance.render_template(config);
    }

    public get modal(): any {
        return this._instance.aModal || null;
    }

    public static get context(): Widget {
        return Widget._CONTEXT;
    }
    
    public static get $hooks(): Observable<{type: string, data?: any}> {
        return Widget._CONTEXT._instance.$hooks.asObservable();
    }
}
