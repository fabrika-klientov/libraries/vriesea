import axios, {AxiosRequestConfig} from 'axios';
import {catchError, from, map, Observable, of} from 'rxjs';
import {VRIESEA_CORE as CORE} from '../../configs/core.config';
import {HttpClientContract} from '../../contracts/http-client.contract';


export class MainVueApi implements HttpClientContract {
    protected readonly _backend: string;
    protected readonly _token: string;
    protected readonly _prefix: string;
    protected static CONTEXT: MainVueApi;

    constructor() {
        this._backend = CORE().server.replace(/\/?$/, '');
        this._prefix = CORE().api!;
        this._token = CORE().userToken!.trim();
        MainVueApi.CONTEXT = this;
    }

    public get(link: string, params: any = {}, headers?: {}, config?: AxiosRequestConfig): Observable<any> {
        return from(axios.get(this.link(link), {headers: {...this.headers(), ...headers}, params, ...config}))
            .pipe(
                map(this.handleResult),
                catchError(this.handleError),
            );
    }

    public post(link: string, data: any, headers?: {}, config?: AxiosRequestConfig): Observable<any> {
        return from(axios.post(this.link(link), data, {headers: {...this.headers(), ...headers}, ...config}))
            .pipe(
                map(this.handleResult),
                catchError(this.handleError),
            );
    }

    public patch(link: string, data: any, headers?: {}, config?: AxiosRequestConfig): Observable<any> {
        return from(axios.patch(this.link(link), data, {headers: {...this.headers(), ...headers}, ...config}))
            .pipe(
                map(this.handleResult),
                catchError(this.handleError),
            );
    }

    public delete(link: string, params: any = {}, headers?: {}, config?: AxiosRequestConfig): Observable<any> {
        return from(axios.delete(this.link(link), {headers: {...this.headers(), ...headers}, params, ...config}))
            .pipe(
                map(this.handleResult),
                catchError(this.handleError),
            );
    }

    public link(suffix: string): string {
        return (suffix.indexOf('http') === 0) ? suffix : (this._backend + this._prefix + suffix);
    }

    public headers(): any {
        return {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + this._token,
        };
    }

    public handleResult(data: any) {
        if (data && data.data) {
            return data.data;
        }
        return null;
    }

    public handleError(error: any): any {
        console.error(CORE().codeLogging + 'Request has error', error);
        return of(null);
    }

    public static get accountHost(): string {
        return location.protocol + '//' + location.host;
    }

    public static get serverHost(): string {
        return MainVueApi.CONTEXT._backend;
    }

    public static get context(): MainVueApi {
        return MainVueApi.CONTEXT;
    }
}
