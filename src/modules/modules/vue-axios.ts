import {MainVueApi} from '../classes/main-vue-api';
import {HttpClientContract} from '../../contracts/http-client.contract';

export class VueAxios {

    static install(vue, options?: ConstructorOf<HttpClientContract>): void {
        vue.prototype.$httpClient = options ? new options() : new MainVueApi();
    }
}
