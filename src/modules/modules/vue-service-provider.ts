import {ServiceProvider} from '../classes/service-provider';

export class VueServiceProvider {

    static install(vue, options): void {
        vue.prototype.$vrieseaProvider = new ServiceProvider(options);
    }
}
