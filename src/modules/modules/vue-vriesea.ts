import Vue from 'vue';
import {ModuleTree, StoreOptions} from 'vuex';
import {defineVrieseaConfig, VrieseaConfig} from '../../configs/core.config';
import {WidgetModel} from '../../contracts/widget.model';
import {Widget} from '../classes/widget';
import {VueAxios} from './vue-axios';
import {modules as defModules, StoreState} from '../../storage/modules-state';
import {$store, options as defOptions} from '../../storage/store';
import {RegisterFactory} from '../classes/service-provider';
import {VueServiceProvider} from './vue-service-provider';
import {HttpClientContract} from '../../contracts/http-client.contract';

export interface VrieseaModuleConfig {
    widget: WidgetModel;
    config: VrieseaConfig;
    modules?: ConfigModules;
    storageRoot?: StoreOptions<StoreState | any>;
    storageModules?: ModuleTree<any>;
}

interface ConfigModules {
    vueApi?: ConfigModuleStatus<ConstructorOf<HttpClientContract>>;
    serviceProvider?: ConfigModuleStatus<RegisterFactory[]>;
}

interface ConfigModuleStatus<T> {
    status: boolean;
    additional?: T;
}


export class VueVriesea {

    static install(vue, config: VrieseaModuleConfig): void {
        vue.prototype.$vrieseaWidget = new Widget(config.widget);
        defineVrieseaConfig(config.config);

        this.registerModules(config.modules);
        this.registerStorageModules(config.storageRoot, config.storageModules);
    }

    static registerModules(cm: ConfigModules | undefined) {
        if (!cm) {
            return;
        }

        if (cm.vueApi?.status) {
            Vue.use(VueAxios, cm.vueApi.additional);
        }

        if (cm.serviceProvider?.status) {
            Vue.use(VueServiceProvider, cm.serviceProvider.additional || []);
        }

        // TODO: add another
    }

    static registerStorageModules(root: StoreOptions<StoreState | any> | undefined, modules: ModuleTree<any> | undefined) {
        if ($store) { // do not change state after registered store
            return;
        }

        if (root) {
            for (let key in root) {
                defOptions[key] = root[key];
            }
        }

        if (modules) {
            for (let key in modules) {
                defModules[key] = modules[key];
            }
        }
    }
}
