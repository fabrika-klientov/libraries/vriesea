import {VRIESEA_CORE as CORE} from '../../configs/core.config';
import {Widget} from '../../modules/classes/widget';

export class PushBox {

    private _widget: Widget;
    private _script: string;

    constructor() {
        this._widget = Widget.context;
        this.script();
    }

    protected script() {
        let scriptVersion = CORE().scriptVersion;
        // @MERCURY(START_REPLACE_SEARCH)
        if (CORE().publicEnv) {
            this._script = this._widget.params.path + `/dist/front/js/app.js?v=${scriptVersion}`;
        } else {
            this._script = CORE().frontPath + '/js/app.js';
        }
        // @MERCURY(START_REPLACE_VALUE)
        // this._script = this._widget.params.path + `/dist/front/js/app.js?v=${scriptVersion}`;
        // @MERCURY(END_REPLACE_VALUE)
        // @MERCURY(END_REPLACE_SEARCH)
    }

    public init() {
        requirejs([this._script], (module) => {
            module.Front.run();
        });
    }

    public dp() {
        requirejs([this._script], (module) => {
            module.Front.dp();
        });
    }

    public another(entry: string, ...params: any) {
        requirejs([this._script], (module) => {
            module.Front[entry](...params);
        });
    }
}
