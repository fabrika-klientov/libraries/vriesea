import {Widget} from '../../modules/classes/widget';
import {VRIESEA_CORE as CORE} from '../../configs/core.config';

export class RightBox {

    private _widget: Widget;

    constructor() {
        this._widget = Widget.context;
    }

    public init() {
        const container = document.querySelector('.card-widgets__elements');

        if (container) {
            this._widget.renderTemplate(
                {
                    caption: {
                        class_name: `head-${CORE().codeWidget}`,
                    },
                    body: `<div class="${CORE().coreClass}"><${CORE().coreEl} /></div>`,
                    render: ``,
                },
            );

            container
                .querySelector(`div[data-code="${this._widget.params.widget_code}"]`)!
                .classList.add(`fk-${CORE().codeWidget}-front-app`);
        }
    }
}
