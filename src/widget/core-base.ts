import {Subject} from 'rxjs';
import {Poppy} from '@fbkl/poppy';
import {Pine} from '@fbkl/pine';

import {InjectTextsService} from './services/inject-texts.service';
import {PushBox} from './core/push-box';
import {RightBox} from './core/right-box';
import {Widget} from '../modules/classes/widget';
import {AdvancedWidgetSettingsContract, WidgetContract, WidgetSettingsContract} from '../contracts/widget.contract';
import {getAPP, VRIESEA_CORE as CORE} from '../configs/core.config';
import {MainVueApi} from '../modules/classes/main-vue-api';
import {dpSettingsBox} from '../helpers/amo-interface.helper';

export abstract class CoreBase implements WidgetContract, WidgetSettingsContract, AdvancedWidgetSettingsContract {

    protected _status: boolean;
    protected _widget: Widget;
    protected _pushBox: PushBox;

    protected constructor(instance) {
        this._widget = new Widget(instance);
        this._pushBox = new PushBox();
        let refreshed = !!window[this.code()];
        let $hooks = window[this.code()]?.$hooks || new Subject();
        window[this.code()] = instance;
        window[this.code()].$hooks = $hooks;
        if (refreshed) {
            window[this.code()].$hooks.next({type: 'refreshed', instance});
        }
        this._status = this._widget.params.active === 'Y';
    }

    protected abstract code(): string;


    render(): void {
        window[this.code()].$hooks.next({type: 'render'});

        if (!CORE().publicEnv) {
            new Poppy().init();
        }

        new MainVueApi();

        if (!this._status) {
            return;
        }

        // TODO: (as Ex.:) override this method and add
        // if (['lcard'].includes(this._widget.area)) {
        //     this.doFront();
        //     this.initWidget();
        // }
    }

    init(): void {
        window[this.code()].$hooks.next({type: 'init'});
    }

    bindActions(): void {
        window[this.code()].$hooks.next({type: 'bindActions'});
    }

    destroy(): void {
        window[this.code()].$hooks.next({type: 'destroy'});
    }

    settings(data: any): void {
        window[this.code()].$hooks.next({type: 'settings'});

        this.miniSettings();

        if (!this._status) {
            return;
        }

        if (!CORE().publicEnv) {
            new InjectTextsService().settingsInject();
        }
    }

    onSave(data: any): void {
        if (data.active === 'N') {
            this._widget.settings.phone = null;
            let key = this._widget.settings.token_key;

            MainVueApi.context
                .post(`${CORE().intCrimsonServer}${CORE().paths.disableLink}`, {key})
                .subscribe(result => {});
        }
    }

    advancedSettings(): void {
        const settings = this._widget.settings;
        const code = settings.widget_code;
        const _user_token_value = (settings.token_key || '').trim();
        const _user_admin = this._widget.system.amouser_id;

        $('#work-area-' + code).html(
            `<div id="app-advanced-settings-widget" class="app-advanced-settings-widget">
                <div class="app-inner">
                    <app-component></app-component>
                </div>
            </div>`
        );

        let scriptVersion = CORE().scriptVersion;
        // @MERCURY(START_REPLACE_SEARCH)
        let script;
        if (CORE().publicEnv) {
            script = this._widget.params.path + `/dist/app/js/app.js?v=${scriptVersion}`;
        } else {
            script = CORE().appPath + '/js/app.js';
        }
        // @MERCURY(START_REPLACE_VALUE)
        // let script = this._widget.params.path + `/dist/app/js/app.js?v=${scriptVersion}`;
        // @MERCURY(END_REPLACE_VALUE)
        // @MERCURY(END_REPLACE_SEARCH)

        requirejs([script], (module) => {
            module.App.run({_user_token_value, _user_admin});
        });
    }

    protected doFront() {
        this._pushBox.init();
    }

    protected doDp() {
        this._pushBox.dp();
    }

    protected initWidget() {
        new RightBox().init();
    }

    protected loadingDp() {
        let box = dpSettingsBox();
        box.css({minHeight: '32px', marginBottom: '1rem'});
        box.find('.widget_settings_block__item_field').addClass('hidden');
        box.addClass('fk-page-loading');
    }

    protected miniSettings() {
        let code = this._widget.settings.widget_code;
        let token_key = this._widget.settings.token_key;
        let phone = this._widget.settings.phone;

        let modalBody = $(`.modal.${code} .modal-body`),
            widgetSettingsFields = modalBody.find('.widget_settings_block .widget_settings_block__fields');

        let token_keyInput = widgetSettingsFields.find('input[name="token_key"]');
        let token_keyBox = token_keyInput.closest('.widget_settings_block__item_field');

        let phoneInput = widgetSettingsFields.find('input[name="phone"]');
        let phoneBox = phoneInput.closest('.widget_settings_block__item_field');

        token_keyBox.addClass('hidden');

        phoneInput.val(phone || getAPP().constant('user')?.personal_mobile || '');

        widgetSettingsFields.find(`#save_${code}`)
            .removeClass('button-input-disabled')
            .addClass('button-input_blue');

        widgetSettingsFields.prepend(this._widget.i18n('widget.settings_description'));

        widgetSettingsFields.find('.widget_settings_block__controls').append('<div id="pine-pay"></div>');
        modalBody.find('.widget_settings_block').prepend('<div id="pine-bar"></div>');

        widgetSettingsFields.addClass('fk-page-loading');

        let accountData: any = {
            domain: this._widget.system.domain,
            account: this._widget.account.account.id,
            code: CORE().codeWidget,
        };

        MainVueApi.context
            .get(`${CORE().intCrimsonServer}${CORE().paths.generateKeyLink}`, accountData)
            .subscribe(result => {
                widgetSettingsFields.removeClass('fk-page-loading');
                token_keyInput.val(result.data.key);
                accountData = {...accountData, ...result.data, widgetParams: this._widget.params};

                new Pine('#pine-bar', MainVueApi.context).initBar(accountData);
                if (accountData.pricePeriods?.length) {
                    new Pine('#pine-pay', MainVueApi.context).withStyles().init(accountData);
                }
            });
    }
}
