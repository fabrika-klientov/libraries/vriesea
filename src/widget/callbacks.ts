import {CoreBase} from './core-base';

export const factoryCallbacks = <T extends CoreBase>(factory: () => T): any => {
    let $core: T;

    return {
        render: () => {
            if (!$core) {
                $core = factory();
            }
            $core.render();

            return true;
        },

        init: () => {
            $core.init();

            return true;
        },

        bind_actions: () => {
            $core.bindActions();

            return true;
        },

        settings: (data) => {
            if ($core.settings) {
                $core.settings(data);
            }

            return true;
        },

        onSave: (data) => {
            if ($core.onSave) {
                $core.onSave(data);
            }
            return true;
        },

        destroy: () => {
            if ($core.destroy) {
                $core.destroy();
            }
        },

        contacts: {
            selected: () => {
                if (($core as any).onSelectContacts) {
                    ($core as any).onSelectContacts();
                }
            },
        },

        leads: {
            selected: () => {
                if (($core as any).onSelectLeads) {
                    ($core as any).onSelectLeads();
                }
            },
        },

        customers: {
            selected: () => {
                if (($core as any).onSelectCustomers) {
                    ($core as any).onSelectCustomers();
                }
            }
        },

        tasks: {
            selected: () => {
                if (($core as any).onSelectTasks) {
                    ($core as any).onSelectTasks();
                }
            },
        },

        dpSettings: () => {
            if (($core as any).dpSettings) {
                ($core as any).dpSettings();
            }

            return true;
        },

        advancedSettings: () => {
            $core.advancedSettings();
        },

        loadPreloadedData: async () => {
            if (($core as any).loadPreloadedData) {
                return ($core as any).loadPreloadedData();
            }

            return [];
        },

        loadElements: async () => {
            if (($core as any).loadElements) {
                return ($core as any).loadElements();
            }

            return [];
        },

        linkCard: async () => {
            if (($core as any).linkCard) {
                return ($core as any).linkCard();
            }

            return [];
        },

        searchDataInCard: async () => {
            if (($core as any).searchDataInCard) {
                return ($core as any).searchDataInCard();
            }

            return [];
        },
    };
};
