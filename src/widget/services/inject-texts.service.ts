import {MainVueApi} from '../../modules/classes/main-vue-api';
import {VRIESEA_CORE as CORE} from '../../configs/core.config';

export class InjectTextsService {

    protected lang: string = 'ru';

    public settingsInject() {
        this.injectDescription();
        this.injectShortDescription();
        this.injectName();
    }

    public injectDescription() {
        MainVueApi.context.get(this.getLink('description')).subscribe(data => {
            if (data && data.length) {
                $('.widget_settings_block__descr').html(data);
            }
        });
    }

    public injectShortDescription() {
        MainVueApi.context.get(this.getLink('short_description')).subscribe(data => {
            if (data && data.length) {
                $('.widget_settings_block__head-desc').html(data);
            }
        });
    }

    public injectName() {
        MainVueApi.context.get(this.getLink('name')).subscribe(data => {
            if (data && data.length) {
                $('.widget_settings_block__title').html(data);
            }
        });
    }
    
    
    protected getLink(type: string) {
        return CORE().textServer + CORE().linkDescription
            .replace('{{code}}', CORE().codeWidget)
            .replace('{{type}}', type)
            .replace('{{lang}}', this.lang);
    }
}
