import {AmoBaseEntityV4Model} from './amo-entity.model';

export interface AmoCustomerV4Model extends AmoBaseEntityV4Model {
    next_price: number;
    next_date: number;
    periodicity: number;
    ltv: number;
    purchases_count: number;
    average_check: number;
    _embedded: {
        tags: any[];
        contacts: any[];
        companies: any[];
        segments: any[];
        catalog_elements: any[];
    };
}


export interface AmoCustomerAjaxV2Model {
    id: number;
    date_create: string;
    date_modify: string;
    created_by: number;
    modified_by: number;
    account_id: number;
    main_user_id: number;
    name: {
        id: number;
        text: string;
        url: string;
    };
    deleted: boolean;
    next_price: number;
    periodicity: number;
    next_date: {
        date: number;
        failed: boolean;
    };
    task_last_date: any;
    status_id: number;
    ltv: number;
    purchases_count: number;
    average_check: number;
    custom_fields: any[];
    tags: any;
    main_contact_id: any;
    linked: any;
    segments: any[];
    main_user: string;
    editor: string;
    creator: string;
    element_type: number;
    lead_mark: string;
    lead_mark_days: string;
    date_of_nearest_task: string;
}
