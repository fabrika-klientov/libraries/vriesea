export interface AmoLeadPipelineModel {
    id: number;
    name: string;
    value: number;
    label: string;
    is_main: boolean;
    sort: number;
    is_unsorted_on: boolean;
    is_archive: boolean;
    leads: number;
    statuses: AmoLeadStatusPipelineModel[];
}

export interface AmoLeadStatusPipelineModel {
    id: number;
    name: string;
    color: string;
    sort: number;
    editable: string;
    type: number;
    pipeline_id: number;
    unique: string; // own
}


export interface AmoCustomerStatusModel {
    id: number;
    name: string;
    sort: number;
    is_default: boolean;
    conditions: any[];
    color: string;
    type: number;
    account_id: number;
}


export interface AmoCustomerSegmentModel {
    id: number;
    name: string;
    created_at: string;
    updated_at: string;
    account_id: number;
    color: string;
    available_products_price_types: any[];
    customers_count: number;
    custom_fields_values: any[];
}
