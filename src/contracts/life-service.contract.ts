export interface LifeServiceContract {
    up(): Promise<void>;
    down(): Promise<void>;
}
