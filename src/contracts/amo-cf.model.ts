export interface AmoCfModel {
    id: number;
    name: string;
    type_id: number;
    sort: number;
    description: string;
    code: string;
    disabled: boolean;
    catalog_id: any;
    deleted_at: any;
    enums_names?: any;
    enums?: CFEnumModel[];
    deletable: boolean;
    visible: boolean;
    required: boolean;
    element_type: number;
    element_code: keyof AmoCfGroupEntities; // own
    sortable: boolean;
    groupable: boolean;
    entityWithId: string; // own
}

export interface AmoCfValueModel {
    id: number;
    name: string;
    type_id: number;
    code: string;
    values: { value: any; enum?: number }[];
}

export interface CFEnumModel {
    id: number;
    sort: number;
    total: number
    value: string;
    code: string | null;
}

export interface AmoCfGroupModel {
    id: string;
    name: string;
    element_code: keyof AmoCfGroupEntities; // own
    fields: number[] | string[];
    entityWithId: string; // own
}

export interface AmoCfGroupEntities {
    contacts: AmoCfModel[];
    companies: AmoCfModel[];
    leads: AmoCfModel[];
    customers: AmoCfModel[];
    segments: AmoCfModel[];
    [key: string]: AmoCfModel[];
}

export interface AmoCfGroupGroups {
    contacts: AmoCfGroupModel[];
    companies: AmoCfGroupModel[];
    leads: AmoCfGroupModel[];
    customers: AmoCfGroupModel[];
    segments: AmoCfGroupModel[];
    [key: string]: AmoCfGroupModel[];
}


export interface FactoryCF {
    entity: keyof AmoCfGroupEntities;
    list: FactoryCFItem[];
}

interface FactoryCFItem {
    name: string;
    type: string;
    sort?: number;
    required_statuses?: {
        pipeline_id: number;
        status_id: number;
    };
    enums?: {value: string; sort?: number}[];
}


export interface AmoCfV4Model {
    field_id: number;
    field_name:	string;
    field_code: string | null;
    field_type: string;
    values: { value: any; enum_id?: number; enum_code?: string; }[];
}
