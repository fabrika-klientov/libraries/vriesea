export interface WidgetContract {
    render(): void;
    init(): void;
    bindActions(): void;
    destroy(): void;
}

export interface WidgetSettingsContract {
    settings(data: any): void;
    onSave(data: any): void;
}

export interface WidgetDpContract {
    dpSettings(): void;
}

export interface WidgetSdkCardContract {
    loadPreloadedData(): Promise<any[]>;
    loadElements(): Promise<any[]>;
    linkCard(): Promise<any[]>;
    searchDataInCard(): Promise<any[]>;
}

export interface AdvancedWidgetSettingsContract {
    advancedSettings(): void;
}

export interface SelectContactsContract {
    onSelectContacts(): void;
}

export interface SelectLeadsContract {
    onSelectLeads(): void;
}

export interface SelectCustomersContract {
    onSelectCustomers(): void;
}

export interface SelectTasksContract {
    onSelectTasks(): void;
}

export interface SuperContract extends
    WidgetContract,
    WidgetSettingsContract,
    WidgetDpContract,
    AdvancedWidgetSettingsContract,
    SelectContactsContract,
    SelectLeadsContract,
    SelectCustomersContract,
    SelectTasksContract { }
