export interface ResponseModels<T> {
    operation: string;
    status: boolean;
    data: T[];
}

export interface ResponseModel<T> {
    operation: string;
    status: boolean;
    data: T;
}
