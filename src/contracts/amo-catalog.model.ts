import {AmoCfValueModel} from './amo-cf.model';

export interface AmoCatalogModel {
    id: number;
    name: string;
    date_create: number;
    date_modify: number;
    created_by: number;
    modified_by: number;
    account_id: number;
    sort: number;
    type: string;
    can_add_elements: boolean;
    can_show_in_cards: boolean;
    can_link_multiple: boolean;
    widgets: any[];
    sdk_widget_code: string | null;
    can_be_deleted: boolean;
}

export interface AmoCatalogElementModel {
    id: number;
    date_create: number;
    date_modify: number;
    created_by: number;
    modified_by: number;
    catalog_id: number;
    name: string;
    deleted: boolean;
    external_id: number;
    custom_fields: AmoCfValueModel[];
    account_id: any;
    is_linkable: boolean;
}

export interface AmoLinkedElementModel {
    from_entity_id: number;
    from_entity_type: number;
    to_entity_id: number;
    to_entity_type: number;
    metadata: {
        quantity: number;
        price_id: number;
    };
    link_id: number;
    element: AmoCatalogElementModel;
}
