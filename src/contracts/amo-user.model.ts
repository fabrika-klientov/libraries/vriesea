import {AmoEntity} from './amo-instances.types';

export interface AmoUserModel {
    id: number;
    name: string;
    uuid: string;
    last_name: string;
    full_name: string;
    email: string;
    lang: string;
    groups: any[];
}

export interface AmoManagerModel {
    id: number;
    title: string;
    option: string;
    active: boolean;
    login: string;
    status: string;
    is_admin: string;
    free_user: string;
    amojo_id: string;
    amo_profile_id: string;
    avatar: string;
    group: string;
}

export interface AmoGroupModel {
    id: string;
    title: string;
}


export interface AmoUserRoleModel {
    id: number;
    name: string;
    rights: {
        leads: {
            view: string;
            edit: string;
            add: string;
            delete: string;
            export: string;
        };
        contacts: {
            view: string;
            edit: string;
            add: string;
            delete: string;
            export: string;
        };
        companies: {
            view: string;
            edit: string;
            add: string;
            delete: string;
            export: string;
        };
        tasks: {
            edit: string;
            delete: string;
        };
        mail_access: boolean;
        catalog_access: boolean;
        status_rights: AmoUserRoleStatusRightsModel[];
    };
}

export interface AmoUserRoleStatusRightsModel {
    entity_type: AmoEntity;
    pipeline_id: number;
    status_id: number;
    rights: {
        edit: string;
        view: string;
        delete: string;
    };
}
