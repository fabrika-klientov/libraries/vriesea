export interface AmoTagModel {
    id: number;
    name: string;
    total: number;
}
