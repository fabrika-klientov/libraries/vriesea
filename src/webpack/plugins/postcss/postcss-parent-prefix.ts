import {Container} from 'postcss';


export const postcssParentPrefix = (prefix: string, options?: PostcssParentPrefixOptions) => {
    return (root: Container) => {
        root.walkRules((rule) => {
            if (!rule.selectors) {
                return;
            }

            options = options || {};
            let {isFilter, filter, filterList, sticky, only, exclude, replace, workSkipped} = options;

            let selectors = rule.selectors
                .filter(selector => {
                    if (!isFilter) {
                        return true;
                    }

                    if (filter) {
                        return filter(selector);
                    }

                    let skippedSelector = prepareSelector(selector, options!);
                    let workSelector = workSkipped ? skippedSelector : selector;

                    if (filterList) {
                        return checkInList(workSelector, filterList);
                    }

                    return true;
                })
                .map(selector => {
                    let skippedSelector = prepareSelector(selector, options!);
                    let workSelector = workSkipped ? skippedSelector : selector;

                    if (only) {
                        if (checkInList(skippedSelector, only)) {
                            return doMerge(workSelector, prefix, !!sticky);
                        }

                        return workSelector;
                    }

                    if (exclude) {
                        if (!checkInList(skippedSelector, exclude)) {
                            return doMerge(workSelector, prefix, !!sticky);
                        }

                        return workSelector;
                    }

                    return doMerge(workSelector, prefix, !!sticky);
                });

            if (replace) {
                rule.selectors = selectors;
            } else {
                rule.selectors = [...rule.selectors, ...selectors];
            }
        })
    };
};

export default postcssParentPrefix;

const prepareSelector = (selector: string, {skipPrefix}: PostcssParentPrefixOptions): string => {
    let selectorSkipped = selector;

    if (skipPrefix && selectorSkipped.indexOf(skipPrefix) === 0) {
        selectorSkipped = selectorSkipped.substring(skipPrefix.length);
    }

    return selectorSkipped;
};

const checkInList = (selector: string, list: (string | RegExp)[]): boolean => {
    return list.some(item => typeof item === 'string' ? item.toUpperCase() === selector.toUpperCase() : item.test(selector));
}

const doMerge = (selector: string, prefix: string, sticky: boolean): string => {
    return `${prefix}${sticky ? '' : ' '}${selector}`;
};

export interface PostcssParentPrefixOptions {
    sticky?: boolean;
    replace?: boolean;
    exclude?: (string | RegExp)[];
    only?: (string | RegExp)[];
    skipPrefix?: string;
    workSkipped?: boolean;
    isFilter?: boolean;
    filter?: (selector: string) => boolean;
    filterList?: (string | RegExp)[];
}
