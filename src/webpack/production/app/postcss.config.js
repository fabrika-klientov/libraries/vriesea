const discardComments = require('postcss-discard-comments');
const {postcssParentPrefix} = require('../../plugins/postcss/postcss-parent-prefix')
const {POSTCSS_PARENT_ELEMENTS_STiCKY, POSTCSS_PARENT_ELEMENTS_ONLY} = require('../../variables.config')

module.exports = {
    plugins: [
        postcssParentPrefix(
            process.env.VUE_APP_MAIN_SELECTOR,
            {only: POSTCSS_PARENT_ELEMENTS_ONLY, replace: true},
        ),
        postcssParentPrefix(
            process.env.VUE_APP_MAIN_SELECTOR,
            {
                sticky: true,
                only: POSTCSS_PARENT_ELEMENTS_STiCKY,
                skipPrefix: `${process.env.VUE_APP_MAIN_SELECTOR} `,
                workSkipped: true,
                isFilter: true,
                filterList: POSTCSS_PARENT_ELEMENTS_STiCKY,
            },
        ),
        discardComments,
    ],
}
