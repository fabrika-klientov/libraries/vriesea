import { getOptions, getRemainingRequest } from 'loader-utils';

export interface ElementUILoaderOptions {
    appendsClasses?: string[];
}


export const elementUiLoader = function (this: any, source) {
    const options: ElementUILoaderOptions = getOptions(this);
    const remainingRequest = getRemainingRequest(this);

    if (!options.appendsClasses?.length) {
        return source;
    }

    if (/\/element-ui\//i.test(remainingRequest)) {
        source = doPopper(source, remainingRequest, options);
        source = doTooltip(source, remainingRequest, options);
        source = doDialog(source, remainingRequest, options);
        source = doColorPicker(source, remainingRequest, options);
        source = doTableFilter(source, remainingRequest, options);
        source = doMessage(source, remainingRequest, options);
        source = doMessageBox(source, remainingRequest, options);
        source = doNotification(source, remainingRequest, options);
        source = doMenuHorizontal(source, remainingRequest, options);
        source = doMenuVertical(source, remainingRequest, options);
        source = doDrawerWrapper(source, remainingRequest, options);
        source = doViewImageWrapper(source, remainingRequest, options);
    }

    return source;
}

const doPopper = (source: string, path: string, options: ElementUILoaderOptions): string => {
    switch (true) {
        case /element-ui\.common\.js$/.test(path):
            source = replaceStaticClass(source, 'el-popper', options.appendsClasses);
            source = replaceVueArrayClasses(source, 'el-popper', options.appendsClasses);
            break;

        case /popover\.js$/.test(path):
        case /select\.js$/.test(path):
        case /autocomplete\.js$/.test(path):
        case /date-picker\.js$/.test(path):
        case /dropdown-menu\.js$/.test(path):
        case /time-picker\.js$/.test(path):
        case /time-select\.js$/.test(path):
            source = replaceStaticClass(source, 'el-popper', options.appendsClasses);
            break;

        case /cascader\.js$/.test(path):
            source = replaceVueArrayClasses(source, 'el-popper', options.appendsClasses);
    }

    return source;
}

const doTooltip = (source: string, path: string, options: ElementUILoaderOptions): string => {
    switch (true) {
        case /tooltip\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceVueArrayClasses(source, 'el-tooltip__popper', options.appendsClasses);
    }

    return source;
}

const doDialog = (source: string, path: string, options: ElementUILoaderOptions): string => {
    switch (true) {
        case /dialog\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceStaticClass(source, 'el-dialog__wrapper', options.appendsClasses);
            break;

        case /popup-manager\.js$/.test(path):
            source = source.replace(
                /(\(modalDom,\s*["'](v-modal)["']\))/,
                (full, first, second) => full.replace(second, ['v-modal', ...options.appendsClasses!].join(' ')),
            );
    }

    return source;
}

const doColorPicker = (source: string, path: string, options: ElementUILoaderOptions): string => {
    switch (true) {
        case /color-picker\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceStaticClass(source, 'el-color-dropdown', options.appendsClasses);
    }

    return source;
}

const doTableFilter = (source: string, path: string, options: ElementUILoaderOptions): string => {
    switch (true) {
        case /table\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceStaticClass(source, 'el-table-filter', options.appendsClasses);
    }

    return source;
}

const doMessage = (source: string, path: string, options: ElementUILoaderOptions): string => {
    switch (true) {
        case /message\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceVueArrayClasses(source, 'el-message', options.appendsClasses);
    }

    return source;
}

const doMessageBox = (source: string, path: string, options: ElementUILoaderOptions): string => {
    switch (true) {
        case /message-box\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceStaticClass(source, 'el-message-box__wrapper', options.appendsClasses);
    }

    return source;
}

const doNotification = (source: string, path: string, options: ElementUILoaderOptions): string => {
    switch (true) {
        case /notification\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceVueArrayClasses(source, 'el-notification', options.appendsClasses);
    }

    return source;
}

const doMenuHorizontal = (source: string, path: string, options: ElementUILoaderOptions): string => {
    switch (true) {
        case /menu\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceVueObjClasses(source, 'el-menu--horizontal', options.appendsClasses);
    }

    return source;
}

const doMenuVertical = (source: string, path: string, options: ElementUILoaderOptions): string => {
    switch (true) {
        case /menu\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceVueObjClasses(source, 'el-menu--vertical', options.appendsClasses);
    }

    return source;
}

const doDrawerWrapper = (source: string, path: string, options: ElementUILoaderOptions): string => {
    switch (true) {
        case /drawer\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceStaticClass(source, 'el-drawer__wrapper', options.appendsClasses);
    }

    return source;
}

const doViewImageWrapper = (source: string, path: string, options: ElementUILoaderOptions): string => {
    switch (true) {
        case /image\.js$/.test(path):
        case /element-ui\.common\.js$/.test(path):
            source = replaceStaticClass(source, 'el-image-viewer__wrapper', options.appendsClasses);
    }

    return source;
}


const replaceStaticClass = (source: string, existClass: string, appendsClasses, withExist = true) => {
    if (withExist) {
        appendsClasses = [existClass, ...appendsClasses];
    }

    return source.replace(
        new RegExp(`(["']?staticClass["']?:\\s*["'].*(\\b${existClass}\\b).*["'],?)`, 'g'),
        (full, first, second) => full.replace(second, appendsClasses.join(' ')),
    );
}

const replaceVueArrayClasses = (source: string, existClass: string, appendsClasses, withExist = true) => {
    if (withExist) {
        appendsClasses = [existClass, ...appendsClasses];
    }

    return source.replace(
        new RegExp(`(["']?class["']?:\\s*\\[.*(["']${existClass}["']).*],?)`, 'gs'),
        (full, first, second) => full.replace(second, appendsClasses.map(item => `"${item}"`).join(', ')),
    );
}

const replaceVueObjClasses = (source: string, existClass: string, appendsClasses) => {
    return source.replace(
        new RegExp(`(["']?class["']?:\\s*{.*(["'](${existClass})["']:[^,]+).*},?)`, 'gs'),
        (full, first, second) => full.replace(second, appendsClasses.map(item => `"${item}": true`).join(', ') + ', ' + second),
    );
}

export default elementUiLoader;
