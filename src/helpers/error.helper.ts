export class ErrorHelper {
    protected replaceMap: Map<string | RegExp, string> = new Map();

    constructor(
        replaceMap?: Map<string | RegExp, string> | { [key: string]: string } | [string | RegExp, string][],
        public defError: string = 'Неизвестная ошибка',
    ) {
        if (!replaceMap) {
            return;
        }

        if (replaceMap instanceof Map) {
            this.replaceMap = replaceMap;
            return;
        }

        switch (typeof replaceMap) {
            case 'object':
                this.replaceMap = new Map(
                    replaceMap.length ? (replaceMap as [string | RegExp, string][]) : Object.entries(replaceMap)
                );
                return;
        }
    }

    public handle(errorData: any, replaceMap?: Map<string | RegExp, string>, ignoreTopReplaceMap?: boolean): string[] {
        if (!errorData) {
            return [this.defError];
        }

        return this.doReplacing(this.doErrors(errorData), replaceMap, ignoreTopReplaceMap);
    }

    protected doErrors(errorData: any): string[] {
        let result: string[] = [];

        switch (typeof errorData) {
            case 'string':
                result.push(errorData);
                break;

            case 'object':
                if (errorData.length) {
                    result.push(...errorData);
                    break;
                }

                for (let key in errorData) {
                    result.push(...errorData[key]);
                }
        }

        return result;
    }

    protected doReplacing(
        errors: string[],
        replaceMap?: Map<string | RegExp, string>,
        ignoreTopReplaceMap?: boolean,
    ): string[] {
        if (!this.replaceMap.size && !replaceMap?.size) {
            return errors;
        }

        let localEntries: [string | RegExp, string][] = Array.from(replaceMap?.entries() || []);
        let topEntries: [string | RegExp, string][] = Array.from(this.replaceMap.entries());

        return errors
            .map(item => {
                if (localEntries.length) {
                    return localEntries.reduce((result, [pattern, value]) => result.replace(pattern, value), item);
                }

                return item;
            })
            .map(item => {
                if (!ignoreTopReplaceMap && topEntries.length) {
                    return topEntries.reduce((result, [pattern, value]) => result.replace(pattern, value), item);
                }

                return item;
            });
    }
}
