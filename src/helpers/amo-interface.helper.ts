import {Subject} from 'rxjs';
import {Widget} from '../modules/classes/widget';
import {MainVueApi} from '../modules/classes/main-vue-api';
import {getAPP, VRIESEA_CORE} from '../configs/core.config';


export const hideInMenu = () => {
    const widgetCode: string = Widget.context.settings.widget_code;
    $('body').append(`<style>div.tips-item[data-id="${widgetCode}"]{display:none !important;}</style>`);
};


export const hideOnListMultiActions = () => {
    $('#widgets_block').css({display: 'none'});
    $('#card_widgets_overlay').remove();
};


export const dpSettingsBox = (code?: string): any => {
    let dpModal = $(`.digital-pipeline__short-task_widget-style_${code || Widget.context.settings.widget_code}`)
        .parent()
        .parent()
        .find('[data-action="send_widget_hook"]');

    let form = dpModal.find('.digital-pipeline__edit-forms');

    return form.find('#widget_settings__fields_wrapper');
};


export const advancedSettingsLink = (): string => {
  return MainVueApi.accountHost + '/settings/widgets/' + Widget.context.settings.widget_code;
};


export const isExpandedWidgetRight = (): boolean => {
    let head = document.querySelector(`.head-${VRIESEA_CORE().codeWidget}`);
    if (!head) {
        return false;
    }

    let arrow = head.querySelector('.card-widgets__widget__caption__arrow');
    if (!arrow) {
        return false;
    }

    return arrow.classList.contains('widgets__widget__caption__arrow_top');
};


export const checkAmoListPaginateChange = (countCheckMs: number = 10000, difCheckMs: number = 200): Subject<boolean> => {
    let subject: Subject<boolean> = new Subject();

    let checkModel = () => getAPP().data.current_list?.models?.[0];
    let beforeCheckModel = checkModel();

    let checkPage = () => checkModel() !== beforeCheckModel;

    let timeInterval: any = null;
    let clInt = () => {
        if (timeInterval) {
            clearInterval(timeInterval);
            timeInterval = null;
        }
    };

    let eventPush = () => {
        clInt();

        let fixed = 0;
        timeInterval = setInterval(() => {
            if (fixed > countCheckMs) {
                clInt();
                subject.next(false);
            }

            if (checkPage()) {
                subject.next(true);
                beforeCheckModel = checkModel();
                clInt();
            }

            fixed += difCheckMs;
        }, difCheckMs);
    };

    let paginator = document.getElementById('list_pagination');
    if (paginator) {
        paginator.addEventListener('click', eventPush, false);
    }

    document.addEventListener('keyup', ev => {
        if (!ev.ctrlKey) {
            return;
        }

        switch (ev.code) {
            case 'ArrowLeft':
            case 'ArrowRight':
                eventPush();
        }
    }, false);

    return subject;
};
