import {AmoBaseEntityV4Model, FindEntitiesQueryParams} from '../contracts/amo-entity.model';
import {AmoCfV4Model} from '../contracts/amo-cf.model';


export const filterAmoEntitiesByQuery = <T extends AmoBaseEntityV4Model>(
    result: T[],
    {query, filter, forFieldID, forFieldCode}: FindEntitiesQueryParams<T>,
): T[] => {
    if (filter) {
        result = result.filter(filter);
    } else if (forFieldID) {
        result = result.filter(item => {
            let cf = item.custom_fields_values.find(one => one.field_id === forFieldID);
            if (!cf) {
                return false;
            }

            return filterAmoEntityCFValues(cf, query);
        });
    } else if (forFieldCode) {
        result = result.filter(item => {
            let cf = item.custom_fields_values.find(one => one.field_code === forFieldCode);
            if (!cf) {
                return false;
            }

            return filterAmoEntityCFValues(cf, query);
        });
    }

    return result;
}

export const filterAmoEntityCFValues = (cf: AmoCfV4Model, query: string): boolean => {
    let upperQuery = query.toUpperCase();

    return cf.values.some(one => {
        switch (typeof one.value) {
            case 'string':
                return one.value.toUpperCase().indexOf(upperQuery) !== -1;
            case 'number':
                return one.value === +query;
            case 'boolean':
                return one.value === !!query;
            default:
                return false;
        }
    });
}
