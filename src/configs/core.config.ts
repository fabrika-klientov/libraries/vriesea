let _CONFIG: VrieseaConfig = {};

export const VRIESEA_CORE = () => ({
    publicEnv: process.env.VUE_APP_AMO_PUBLIC_BUILD,
    server: _CONFIG.server || process.env.VUE_APP_URL!,
    intCrimsonServer: _CONFIG.crimsonServer || process.env.VUE_APP_INT_URL!,
    proxyAmoServer: _CONFIG.proxyAmoServer || process.env.VUE_APP_AMO_PROXY_URL || 'https://proxy-amo.bpmcenter.pro',
    scriptVersion: process.env.VUE_APP_SCRIPT_VERSION,
    codeWidget: _CONFIG.codeWidget!,
    api: _CONFIG.api,

    paths: {
        generateKeyLink: '/v1/generate',
        disableLink: '/v1/disable',
        appStatus: '/api/v0/status',
    },

    amoPaths: {
        linkCustomFields: '/ajax/settings/custom_fields',
        users: '/v3/users',
        account: '/api/v4/account',
        catalogs: '/ajax/v1/catalogs/list',
        contactsApiV4: '/api/v4/contacts',
        companiesApiV4: '/api/v4/companies',
        customersApiV4: '/api/v4/customers',
        customersAjaxV2: '/ajax/v2/customers/list',
        catalogElements: '/ajax/v1/catalog_elements/list',
        linkedCatalogElements: '/ajax/leads/{lead}/catalog/{catalog}/elements',
        leadPipelines: '/ajax/v1/pipelines/list',
        customersStatuses: '/api/v4/customers/statuses',
        customersSegments: '/api/v4/customers/segments',
        managersWithGroups: '/ajax/get_managers_with_group',
        usersRoles: '/api/v4/roles',
        v4Cf: '/api/v4/{entity}/custom_fields',
        leadTags: '/ajax/v3/leads/tags',
        customerTags: '/ajax/v3/customers/tags',
        contactTags: '/ajax/v3/contacts/tags',
    },

    amoEntities: {
        leads: {
            detail: '/leads/detail/',
        },
        customers: {
            detail: '/customers/detail/',
        },
        contacts: {
            detail: '/contacts/detail/',
        },
        companies: {
            detail: '/companies/detail/',
        },
    },

    pusher: {
        // host: 'https://pusher.bpmcenter.pro',
        broadcaster: 'pusher', // socket.io
        withoutInterceptors: true,
        forceTLS: false,
        disableStats: true,
        auth: {
            headers: {
                Authorization: `Basic ${_CONFIG.userToken}`,
                'X-Auth-Box': _CONFIG.codeWidget,
            },
        },
        wsHost: process.env.VUE_APP_PUSHER_SOCKET_URI || 'pusher2.bpmcenter.pro',
        wssHost: process.env.VUE_APP_PUSHER_SOCKET_URI || 'pusher2.bpmcenter.pro',
        wsPort: +(process.env.VUE_APP_PUSHER_SOCKET_POST || 80),
        wssPort: +(process.env.VUE_APP_PUSHER_SOCKET_SECURE_PORT || 443),
        namespace: '',
        key: null, // should be replaced by deep config
        ...(_CONFIG.pusher || {}),
    },

    userToken: _CONFIG.userToken,
    coreClass: `fk-${_CONFIG.codeWidget || 'unresolved'}`,
    coreEl: `${_CONFIG.codeWidget || 'unresolved'}-front-component`,
    codeLogging: `#${(_CONFIG.codeWidget || 'VRIESEA').toUpperCase()} LIB:: `,
    textServer: process.env.VUE_APP_TEXT_CRIMSON_URI || process.env.VUE_APP_JSTORAGE_URI || 'https://text.bpmcenter.pro',
    linkDescription: '/texts/service.{{code}}.{{type}}/{{lang}}',
    linkShared: '/texts/shared.{{code}}.{{type}}/{{lang}}',
    appPath: process.env.VUE_APP_STATIC_HOST! + process.env.VUE_APP_STATIC_PATH_APP!,
    frontPath: process.env.VUE_APP_STATIC_HOST! + process.env.VUE_APP_STATIC_PATH_FRONT!,
});

export interface VrieseaConfig {
    server?: string;
    crimsonServer?: string;
    proxyAmoServer?: string;
    userToken?: string;
    codeWidget?: string;
    api?: string;
    pusher?: any;
}

export const defineVrieseaConfig = (config: VrieseaConfig) => {
    _CONFIG = {..._CONFIG, ...config};
};


let _APP;
export const getAPP = () => {
    if (_APP) {
        return _APP;
    }

    try {
        _APP = APP || AMOCRM;
    } catch (error) {
        console.error(VRIESEA_CORE().codeLogging + 'Neither constant APP nor AMOCRM exist', error)
    }

    return _APP;
};
