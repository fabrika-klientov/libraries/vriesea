import {Store, StoreOptions} from 'vuex';
import {getModule, VuexModule} from 'vuex-module-decorators';
import {modules, StoreState} from './modules-state';


const options: StoreOptions<StoreState> = {
    state: {
    },

    mutations: {
    },

    actions: {
    },

    getters: {
    },

    modules,
};


let $store: Store<any>;


function initializeStore(storeInstance: Store<any>) {
    $store = storeInstance
}

function cleanStore() {
    if (!$store) {
        return
    }

    for (let key in modules) {
        let module = getModule<any>(modules[key] as any, $store)
        if (module?.fullClean) {
            module.fullClean()
        }
    }
}

export function getVrieseaModule<M extends VuexModule>(moduleClass: ConstructorOf<M>): M {
    return getModule(moduleClass, $store)
}


export {$store, options, initializeStore, cleanStore}
