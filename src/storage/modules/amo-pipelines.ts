import {VuexModule, Module, Mutation, Action} from 'vuex-module-decorators';
import {catchError, lastValueFrom, map, of} from 'rxjs';
import {MainVueApi} from '../../modules/classes/main-vue-api';
import {VRIESEA_CORE as CORE} from '../../configs/core.config';
import {
    AmoCustomerSegmentModel,
    AmoCustomerStatusModel,
    AmoLeadPipelineModel,
    AmoLeadStatusPipelineModel,
} from '../../contracts/amo-pipelines.model';


@Module({
    name: 'AmoPipelines',
    namespaced: true,
})
export default class AmoPipelines extends VuexModule {
    private $leadPipelines: AmoLeadPipelineModel[] = [];
    private $customersStatuses: AmoCustomerStatusModel[] = [];
    private $customersSegments: AmoCustomerSegmentModel[] = [];


    @Mutation
    public setLeadPipelines(data: any) {
        let result: AmoLeadPipelineModel[] = [];
        for (let key in data) {
            if (!data.hasOwnProperty(key)) {
                continue;
            }

            let statuses: AmoLeadStatusPipelineModel[] = [];
            for (let id in data[key].statuses || {}) {
                if (!data[key].statuses.hasOwnProperty(id)) {
                    continue;
                }

                let status = data[key].statuses[id];
                status.unique = `${key}_${id}`;

                statuses.push(status);
            }

            result.push({...data[key], statuses});
        }

        this.$leadPipelines = result;
    }

    @Mutation
    public setCustomersStatuses(data: any) {
        this.$customersStatuses = data || [];
    }

    @Mutation
    public setCustomersSegments(data: any) {
        this.$customersSegments = data || [];
    }


    @Action
    public async loadLeadPipelines(): Promise<AmoLeadPipelineModel[]> {
        if (this.$leadPipelines.length) {
            return this.$leadPipelines;
        }

        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost + CORE().amoPaths.leadPipelines,
                    {},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<any, AmoLeadPipelineModel[]>(response => {
                        this.setLeadPipelines(response.response.pipelines);

                        return this.$leadPipelines;
                    }),
                ),
        );
    }

    @Action
    public async loadCustomerStatuses(): Promise<AmoCustomerStatusModel[]> {
        if (this.$customersStatuses.length) {
            return this.$customersStatuses;
        }

        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost + CORE().amoPaths.customersStatuses,
                    {},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<any, AmoCustomerStatusModel[]>(response => {
                        this.setCustomersStatuses(response?._embedded?.statuses);

                        return this.$customersStatuses;
                    }),
                    catchError(() => of([])),
                ),
        );
    }

    @Action
    public async loadCustomerSegments(): Promise<AmoCustomerSegmentModel[]> {
        if (this.$customersStatuses.length) {
            return this.$customersSegments;
        }

        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost + CORE().amoPaths.customersSegments,
                    {},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<any, AmoCustomerSegmentModel[]>(response => {
                        this.setCustomersSegments(response?._embedded?.segments);

                        return this.$customersSegments;
                    }),
                    catchError(() => of([])),
                ),
        );
    }


    public get leadPipelines(): AmoLeadPipelineModel[] {
        return this.$leadPipelines;
    }

    public get leadStatusesPipelineFor(): (pipelineId: number, withUnsorted?: boolean) => AmoLeadStatusPipelineModel[] {
        return (pipelineId, withUnsorted = false) => {
            let pipeline = this.$leadPipelines.find(item => +item.id === pipelineId);

            return (pipeline?.statuses || []).filter(item => (withUnsorted || item.type !== 1));
        };
    }

    public get statusForLeadIdStatusId(): (statusId: number, pipelineId?: number) => AmoLeadStatusPipelineModel | null {
        return (statusId, pipelineId) => {
            let pipelines = this.$leadPipelines;
            if (pipelineId) {
                pipelines = pipelines.filter(item => +item.id === pipelineId);
            }

            return pipelines
                .reduce(
                    (result: AmoLeadStatusPipelineModel[], item) => [...result, ...item.statuses],
                    [],
                )
                .find(item => +item.id === statusId) || null;
        };
    }

    public get customerStatuses(): AmoCustomerStatusModel[] {
        return this.$customersStatuses;
    }

    public get customerSegments(): AmoCustomerSegmentModel[] {
        return this.$customersSegments;
    }
}
