import {Module, Action} from 'vuex-module-decorators';
import {lastValueFrom, map} from 'rxjs';
import {MainVueApi} from '../../modules/classes/main-vue-api';
import {VRIESEA_CORE as CORE} from '../../configs/core.config';
import {AmoCfGroupEntities, AmoCfModel, FactoryCF} from '../../contracts/amo-cf.model';
import AmoCf from './amo-cf';


@Module({
    name: 'AmoCfProxy',
    namespaced: true,
})
export default class AmoCfProxy extends AmoCf {

    @Action
    public async loadCF(force: boolean = false): Promise<AmoCfGroupEntities> {
        if (!force && (this.$cf.leads.length || this.$cf.contacts.length || this.$cf.companies.length)) {
            return this.$cf;
        }

        return lastValueFrom(
            MainVueApi.context
                .get(CORE().proxyAmoServer + CORE().amoPaths.linkCustomFields)
                .pipe(
                    map<any, AmoCfGroupEntities>(response => {
                        this.setCF(response.response.params.fields);
                        this.setGroups(response.response.params.groups);

                        return this.$cf;
                    }),
                )
        );
    }

    @Action
    public async storeCF(factory: FactoryCF): Promise<AmoCfModel[]> {
        if (!factory.list.length) {
            return [];
        }

        throw new Error('Not implement in Crimson proxy');
    }
}
