import {VuexModule, Module, Mutation, Action} from 'vuex-module-decorators';
import {lastValueFrom, map} from 'rxjs';
import {MainVueApi} from '../../modules/classes/main-vue-api';
import {VRIESEA_CORE as CORE} from '../../configs/core.config';
import {AmoContactV4Model} from '../../contracts/amo-contact.model';
import {FindEntitiesQueryParams} from '../../contracts/amo-entity.model';
import {filterAmoEntitiesByQuery} from '../../helpers/amo-entities.helper';


@Module({
    name: 'AmoContacts',
    namespaced: true,
})
export default class AmoContacts extends VuexModule {

    @Action
    public async findByIdApiV4(id: number): Promise<AmoContactV4Model | null> {
        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost + CORE().amoPaths.contactsApiV4 + `/${id}`,
                    {},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<AmoContactV4Model, AmoContactV4Model>(response => {
                        return response || null;
                    }),
                ),
        );
    }

    @Action
    public async findByIdsApiV4(id: number[]): Promise<AmoContactV4Model[]> {
        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost + CORE().amoPaths.contactsApiV4,
                    {id},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<any, AmoContactV4Model[]>(response => {
                        return response?._embedded?.contacts || [];
                    }),
                ),
        );
    }

    @Action
    public async findByQueryApiV4(data: FindEntitiesQueryParams<AmoContactV4Model>): Promise<AmoContactV4Model[]> {
        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost + CORE().amoPaths.contactsApiV4,
                    {query: data.query},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<any, AmoContactV4Model[]>(response => {
                        return filterAmoEntitiesByQuery(response?._embedded?.contacts || [], data);
                    }),
                ),
        );
    }
}
