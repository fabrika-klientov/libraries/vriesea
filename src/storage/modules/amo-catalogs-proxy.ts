import {Module, Action} from 'vuex-module-decorators';
import {lastValueFrom, map} from 'rxjs';
import {MainVueApi} from '../../modules/classes/main-vue-api';
import {VRIESEA_CORE as CORE} from '../../configs/core.config';
import {AmoCatalogElementModel, AmoCatalogModel} from '../../contracts/amo-catalog.model';
import AmoCatalogs from './amo-catalogs';


@Module({
    name: 'AmoCatalogsProxy',
    namespaced: true,
})
export default class AmoCatalogsProxy extends AmoCatalogs {

    @Action
    public async loadCatalogs(): Promise<AmoCatalogModel[]> {
        if (this.$catalogs.length) {
            return this.$catalogs;
        }

        return lastValueFrom(
            MainVueApi.context
                .get(CORE().proxyAmoServer + CORE().amoPaths.catalogs)
                .pipe(
                    map<any, AmoCatalogModel[]>(response => {
                        this.setCatalogs(response.response?.catalogs || {});

                        return this.$catalogs;
                    }),
                ),
        );
    }

    @Action
    public async loadCatalogElements(catalog_id: number): Promise<AmoCatalogElementModel[]> {
        if (this.$elements[catalog_id]?.length) {
            return this.$elements[catalog_id];
        }

        throw new Error('Not implement in Crimson proxy');
    }
}
