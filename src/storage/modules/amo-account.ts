import {VuexModule, Module, Mutation, Action} from 'vuex-module-decorators';
import {lastValueFrom, map} from 'rxjs';
import {MainVueApi} from '../../modules/classes/main-vue-api';
import {getAPP, VRIESEA_CORE as CORE} from '../../configs/core.config';
import {AmoAccountModel, AmoFrontAccountModel, AmoFrontCfModel, TaskTypeModel} from '../../contracts/amo-account.model';


@Module({
    name: 'AmoAccount',
    namespaced: true,
})
export default class AmoAccount extends VuexModule {

    private $account: AmoAccountModel | null = null;


    @Mutation
    public setAccount(account: AmoAccountModel) {
        this.$account = account;
    }


    @Action
    public async loadAccount(): Promise<AmoAccountModel | null> {
        if (this.$account) {
            return this.$account;
        }

        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost + CORE().amoPaths.account,
                    {
                        with: [
                            'task_types',
                            'amojo_id',
                            'amojo_rights',
                            'users_groups',
                            'version',
                            'entity_names',
                            'datetime_settings',
                        ].join(','),
                    },
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<AmoAccountModel, AmoAccountModel | null>(response => {
                        this.setAccount(response);

                        return this.$account;
                    }),
                ),
        );
    }


    public get account(): AmoAccountModel | null {
        return this.$account;
    }

    public get taskTypes(): TaskTypeModel[] {
        return this.$account?._embedded?.task_types || [];
    }


    public get frontAccount(): AmoFrontAccountModel {
        return getAPP().constant('account');
    }

    public get accountCurrency(): string | null {
        return this.frontAccount?.currency || null;
    }

    public get cf(): { [key: number]: AmoFrontCfModel } {
        return this.frontAccount?.cf;
    }

    public get cfForId(): (id: number) => AmoFrontCfModel {
        return id => (this.cf?.[id] || null);
    }

    public get predefinedCF(): { [key: string]: AmoFrontCfModel } {
        return this.frontAccount?.predefined_cf;
    }

    public get predefinedCFForCode(): (code: string) => AmoFrontCfModel {
        return code => (this.predefinedCF?.[code] || null);
    }
}
