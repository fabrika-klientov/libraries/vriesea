import {VuexModule, Module, Mutation, Action} from 'vuex-module-decorators';
import {lastValueFrom, map} from 'rxjs';
import {MainVueApi} from '../../modules/classes/main-vue-api';
import {VRIESEA_CORE as CORE} from '../../configs/core.config';
import {AmoGroupModel, AmoManagerModel, AmoUserModel, AmoUserRoleModel} from '../../contracts/amo-user.model';


@Module({
    name: 'AmoUsers',
    namespaced: true,
})
export default class AmoUsers extends VuexModule {
    protected $users: AmoUserModel[] = [];
    protected $roles: AmoUserRoleModel[] = [];
    protected $managers: AmoManagerModel[] = [];
    protected $groups: AmoGroupModel[] = [];


    @Mutation
    public setUsers(users: AmoUserModel[]) {
        this.$users = users;
    }

    @Mutation
    public setRoles(roles: AmoUserRoleModel[]) {
        this.$roles = roles || [];
    }

    @Mutation
    public setManagers(managers: any) {
        let result: AmoManagerModel[] = [];
        for (let id in managers) {
            if (!managers.hasOwnProperty(id)) {
                continue;
            }

            let manager = managers[id];
            manager.id = +manager.id;

            result.push(manager);
        }

        this.$managers = result;
    }

    @Mutation
    public setGroups(groups: any) {
        let result: AmoGroupModel[] = [];
        for (let id in groups) {
            if (!groups.hasOwnProperty(id)) {
                continue;
            }

            result.push({id, title: groups[id]});
        }

        this.$groups = result;
    }


    @Action
    public async loadUsers(): Promise<AmoUserModel[]> {
        if (this.$users.length) {
            return this.$users;
        }

        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost + CORE().amoPaths.users,
                    {limit: -1, page: 1, 'filter[active]': 'Y'},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<any, AmoUserModel[]>(response => {
                        this.setUsers(response._embedded.items);

                        return this.$users;
                    }),
                ),
        );
    }

    @Action
    public async loadRoles(): Promise<AmoUserRoleModel[]> {
        if (this.$roles.length) {
            return this.$roles;
        }

        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost + CORE().amoPaths.usersRoles,
                    {},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<any, AmoUserRoleModel[]>(response => {
                        this.setRoles(response?._embedded?.roles);

                        return this.$roles;
                    }),
                ),
        );
    }

    @Action
    public async loadManagersWithGroups(): Promise<AmoManagerModel[]> {
        if (this.$managers.length) {
            return this.$managers;
        }

        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost + CORE().amoPaths.managersWithGroups,
                    {free_users: 'Y'},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<any, AmoManagerModel[]>(response => {
                        this.setManagers(response.managers);
                        this.setGroups(response.groups);

                        return this.$managers;
                    }),
                ),
        );
    }


    public get users(): AmoUserModel[] {
        return this.$users;
    }

    public get roles(): AmoUserRoleModel[] {
        return this.$roles;
    }

    public get managers(): AmoManagerModel[] {
        return this.$managers;
    }

    public get groups(): AmoGroupModel[] {
        return this.$groups;
    }

    public get managersForGroup(): (id: string) => AmoManagerModel[] {
        return id => this.$managers.filter(item => item.group === id);
    }
}
