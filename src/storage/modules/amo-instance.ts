import Vue from 'vue';
import {Module, Mutation, Action} from 'vuex-module-decorators';
import {filter, lastValueFrom, map, Observable, Subject, takeUntil} from 'rxjs';
import {AmoEntity} from '../../contracts/amo-instances.types';
import {Widget} from '../../modules/classes/widget';
import {getAPP, VRIESEA_CORE as CORE} from '../../configs/core.config';
import {MainVueApi} from '../../modules/classes/main-vue-api';
import {CleanModule} from './clean.module';
import {AmoCfGroupEntities} from '../../contracts/amo-cf.model';
import {
    ENTITY_CODES,
    entityConvert,
    getCardEntityId,
    getCardEntityName,
    getLiveValue
} from '../../helpers/amo-instance.helper';
import {AmoLinkedElementModel} from '../../contracts/amo-catalog.model';
import {AmoTimelineModel} from '../../contracts/amo-timeline.model';

type LinkedAmo = {
    leads?: Linked[];
    contacts?: Linked[];
    companies?: Linked[];
    customers?: Linked[];
};

interface Linked {
    id: number;
    name: string;
}


@Module({
    name: 'AmoInstance',
    namespaced: true,
})
export default class AmoInstance extends CleanModule {

    private $entity: AmoEntity | null = null;
    private $entityName: string | null = null;
    private $entityId: number | null = null;
    private $linked: LinkedAmo = {};
    private $linkedElements: { [key: number]: AmoLinkedElementModel[] } = {};

    private $live: any = {};
    private $prevLive: any = {};
    private $immediate: boolean = false;
    private $liveSubject: Subject<{ oldVal: any; newVal: any }> = new Subject();


    @Mutation
    public setEntity(data: AmoEntity | null) {
        this.$entity = data;
    }

    @Mutation
    public setEntityName(data: string) {
        this.$entityName = data;
    }

    @Mutation
    public setEntityId(data: number) {
        this.$entityId = data;
    }

    @Mutation
    public setLinked(data: LinkedAmo) {
        this.$linked = data;
    }

    @Mutation
    public addLinkedElements({catalogId, data}: { catalogId: number; data: any[]; }) {
        let result: AmoLinkedElementModel[] = [];
        for (let key in data) {
            let item = data[key];
            item.element = item._embedded?.catalog_element || null;
            delete item._embedded;

            result.push(item);
        }

        this.$linkedElements[catalogId] = result;
    }

    @Mutation
    public addLinkedElement({catalogId, data}: { catalogId: number; data: AmoLinkedElementModel; }) {
        if (!this.$linkedElements[catalogId]) {
            this.$linkedElements[catalogId] = [];
        }

        this.$linkedElements[catalogId].push(data);
    }

    @Mutation
    public removeLinkedElement({catalogId, data}: { catalogId: number; data: AmoLinkedElementModel; }) {
        if (!this.$linkedElements[catalogId]) {
            this.$linkedElements[catalogId] = [];
        }

        let index = this.$linkedElements[catalogId].indexOf(data);
        if (index !== -1) {
            this.$linkedElements[catalogId].splice(index, 1);
        }
    }

    @Mutation
    public pushLive(data: { oldVal: any; newVal: any }) {
        let oldVal = this.$prevLive;
        this.$prevLive = JSON.parse(JSON.stringify(this.$live));
        if (!this.$immediate) {
            this.$immediate = true;
            return;
        }

        this.$liveSubject.next({oldVal, newVal: data.newVal});
    }

    @Mutation
    public clean() {
        this.$entity = null;
        this.$entityId = null;
        this.$linked = {};
        this.$linkedElements = {};
    }

    @Mutation
    public cleanLive() {
        this.$live = {};
        this.$prevLive = {};
        this.$immediate = false;
    }


    @Action
    public async initInstance(withLinked: boolean = true): Promise<boolean> {
        this.setEntity(entityConvert(Widget.context.area));

        try {
            this.setEntityId(getCardEntityId());
            this.setEntityName(getCardEntityName(this.$entity!));

            if (!withLinked) {
                return true;
            }

            let linked = await this.loadLinked();

            let linkedFor = type => {
                let list = linked[type] || [];
                return list.map(({id, name}) => ({id, name}));
            };

            this.setLinked({
                leads: linkedFor(ENTITY_CODES.LEADS),
                contacts: linkedFor(ENTITY_CODES.CONTACTS),
                companies: linkedFor(ENTITY_CODES.COMPANIES),
                customers: linkedFor(ENTITY_CODES.CUSTOMERS),
            });
        } catch (e) {
            console.warn(CORE().codeLogging, e);
            return false;
        }

        return true;
    }

    @Action
    public loadTimeline(props: any = {}): Promise<AmoTimelineModel> {
        return lastValueFrom(
            MainVueApi.context
                .get(MainVueApi.accountHost + `/v3/${this.$entity}/${this.$entityId}/timeline`, props)
                .pipe(
                    map(result => {
                        return result?._embedded || null;
                    }),
                ),
        );
    }

    @Action
    protected async loadLinked(): Promise<any> {
        let result = await this.loadTimeline();

        let data = {};
        if (result) {
            for (let type in result) {
                if ([ENTITY_CODES.LEADS, ENTITY_CODES.CONTACTS, ENTITY_CODES.COMPANIES, ENTITY_CODES.CUSTOMERS].includes(type)) {
                    data[type] = [];
                    for (let id in result[type]) {
                        if (!result[type].hasOwnProperty(id)) continue;
                        data[type].push(result[type][id]);
                    }
                }
            }
        }

        return data;
    }

    @Action
    public async loadLinkedElements({catalogId, force}: { catalogId: number; force?: boolean; }): Promise<AmoLinkedElementModel[]> {
        if (!force && this.$linkedElements[catalogId]) {
            return this.$linkedElements[catalogId];
        }

        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost +
                    CORE().amoPaths.linkedCatalogElements
                        .replace('{lead}', `${this.$entityId}`)
                        .replace('{catalog}', `${catalogId}`),
                    {before_id: 0, limit: 100, with: 'catalog_element'},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map(result => {
                        this.addLinkedElements({catalogId, data: result?._embedded?.links || []});
                        return this.$linkedElements[catalogId];
                    }),
                ),
        );
    }

    @Action
    public initLive() {
        // TODO: patch this -> if current card is not `lead`
        const hp = type => (getAPP().data.current_card.linked_forms?.form_models?.models
            .find(item => item.attributes?.ELEMENT_TYPE == type && item.attributes?.ID)?.attributes || null);

        Vue.set(this.$live, 'leads', getAPP().data.current_card.model.attributes);
        Vue.set(this.$live, 'contacts', hp(1));
        Vue.set(this.$live, 'companies', hp(3));
    }

    @Action
    public fullClean(): void {
        super.fullClean();
        this.clean();
        this.cleanLive();
    }


    public get entity(): AmoEntity | null {
        return this.$entity;
    }

    public get entityName(): string | null {
        return this.$entityName;
    }

    public get entityId(): number | null {
        return this.$entityId;
    }

    public get linked(): LinkedAmo {
        return this.$linked;
    }

    public get linkedFor(): (entity: keyof LinkedAmo) => Linked[] {
        return entity => (this.$linked[entity] || []);
    }

    public get linkedFirstFor(): (entity: keyof LinkedAmo) => Linked | null {
        return entity => (this.$linked[entity]!.slice(0, 1).shift() || null);
    }

    public get linkedElements(): { [key: number]: AmoLinkedElementModel[] } {
        return this.$linkedElements;
    }

    public get linkedElementsFor(): (catalogId: number) => AmoLinkedElementModel[] {
        return catalogId => (this.$linkedElements[catalogId] || []);
    }


    public get live(): Observable<{ oldVal: any; newVal: any }> {
        return this.$liveSubject
            .asObservable()
            .pipe(
                takeUntil(this.unsubscribeEvent),
            );
    }

    public get liveField(): <T>(entity: keyof AmoCfGroupEntities, id: number | string) => Observable<T> {
        return (entity, id) => {
            return this.live
                .pipe(
                    filter(({oldVal, newVal}) => oldVal[entity] && newVal[entity]),
                    filter(({oldVal, newVal}) => {
                        let current = getLiveValue(newVal[entity], id);
                        let old = getLiveValue(oldVal[entity], id);

                        if (!current || !old) {
                            return current !== old;
                        }

                        return JSON.stringify(current) !== JSON.stringify(old);
                    }),
                    map(({oldVal, newVal}) => getLiveValue(newVal[entity], id)),
                );
        };
    }

    public get fieldValue(): <T>(entity: keyof AmoCfGroupEntities, id: number | string) => T | null {
        return (entity, id) => {
            let model = this.$live[entity];
            if (!model) {
                return null;
            }

            return getLiveValue(model, id);
        };
    }
}
