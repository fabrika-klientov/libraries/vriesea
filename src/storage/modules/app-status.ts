import {VuexModule, Module, Mutation, Action, getModule} from 'vuex-module-decorators';
import {lastValueFrom, map, Observable, ReplaySubject} from 'rxjs';
import {MainVueApi} from '../../modules/classes/main-vue-api';
import {VRIESEA_CORE as CORE} from '../../configs/core.config';
import TextLocales from './text-locales';
import {$store} from '../store';

export type StatusAppType = 'api_key_invalid'
    | 'api_key_not_active'
    | 'customers_service_not_exist'
    | 'customers_service_code_invalid'
    | 'periods_not_exist'
    | 'periods_refused'
    | 'periods_trial_less'
    | 'periods_trial_lifetime'
    | 'periods_before_refused'
    | 'status_success_1'
    | 'status_success_2'
    | 'status_success_3'
    | 'status_fail_1'
    | 'status_fail_2'
    | 'status_fail_3'
    | 'ok';

export const APP_STATUS = {
    API_KEY_INVALID: 'api_key_invalid',
    API_KEY_NOT_ACTIVE: 'api_key_not_active',
    CUSTOMERS_SERVICE_NOT_EXIST: 'customers_service_not_exist',
    CUSTOMERS_SERVICE_CODE_INVALID: 'customers_service_code_invalid',
    PERIODS_NOT_EXIST: 'periods_not_exist',
    PERIODS_REFUSED: 'periods_refused',
    PERIODS_TRIAL_LESS: 'periods_trial_less',
    PERIODS_TRIAL_LIFETIME: 'periods_trial_lifetime',
    PERIODS_BEFORE_REFUSED: 'periods_before_refused',
    OK: 'ok',

    STATUS_SUCCESS_RESERVED_1: 'status_success_1',
    STATUS_SUCCESS_RESERVED_2: 'status_success_2',
    STATUS_SUCCESS_RESERVED_3: 'status_success_3',

    STATUS_FAIL_RESERVED_1: 'status_fail_1',
    STATUS_FAIL_RESERVED_2: 'status_fail_2',
    STATUS_FAIL_RESERVED_3: 'status_fail_3',
};

export const SUCCESS_STATUSES = [
    APP_STATUS.OK,
    APP_STATUS.PERIODS_BEFORE_REFUSED,
    APP_STATUS.PERIODS_REFUSED,
    APP_STATUS.PERIODS_NOT_EXIST,
    APP_STATUS.PERIODS_TRIAL_LESS,
    APP_STATUS.PERIODS_TRIAL_LIFETIME,

    APP_STATUS.STATUS_SUCCESS_RESERVED_1,
    APP_STATUS.STATUS_SUCCESS_RESERVED_2,
    APP_STATUS.STATUS_SUCCESS_RESERVED_3,
];

export interface StatusAppModel {
    type: 'success' | 'warning' | 'info' | 'error';
    code: StatusAppType;
    additional?: {
        period_start?: string;
        period_end?: string;
    };
}

export interface TodoAction {
    type: string;
    tab: string;
    additional?: any;
}


@Module({
    name: 'AppStatus',
    namespaced: true,
})
export default class AppStatus extends VuexModule {

    private $appStatus: StatusAppModel | null = null;
    private $errorMessage: string | null = null;
    private $todo: ReplaySubject<TodoAction | null> = new ReplaySubject(1);


    @Mutation
    public setStatusApp(status: StatusAppModel): void {
        this.$appStatus = status;
    }

    @Mutation
    public setErrorMessage(data: string): void {
        this.$errorMessage = data;
    }


    @Action
    public loadStatus(): Promise<StatusAppModel> {
        return lastValueFrom(
            MainVueApi.context
                .get(CORE().server.replace(/\/?$/, '') + CORE().paths.appStatus + `/${CORE().userToken}`)
                .pipe(
                    map<any, any>(result => {
                        this.setStatusApp(result);

                        return result;
                    }),
                ),
        );
    }

    @Action
    public async messageError(type: string): Promise<any> {
        let response = await getModule(TextLocales, $store).messageError(type);
        this.setErrorMessage(response);

        return response;
    }

    @Action
    public pushTodo(data: TodoAction | null) {
        this.$todo.next(data);
    }


    public get statusApp(): StatusAppModel | null {
        return this.$appStatus;
    }

    public get isResolved(): boolean {
        return !!this.$appStatus && SUCCESS_STATUSES.includes(this.$appStatus.code);
    }

    public get errorMessage(): string | null {
        return this.$errorMessage;
    }

    public get todo(): Observable<TodoAction | null> {
        return this.$todo.asObservable();
    }
}
