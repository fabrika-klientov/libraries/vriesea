import {VuexModule, Module, Mutation, Action, getModule} from 'vuex-module-decorators';
import {lastValueFrom, map} from 'rxjs';
import {MainVueApi} from '../../modules/classes/main-vue-api';
import {VRIESEA_CORE as CORE} from '../../configs/core.config';
import {AmoCatalogElementModel, AmoCatalogModel} from '../../contracts/amo-catalog.model';
import {$store} from '../store';
import AmoCf from './amo-cf';
import {AmoCfModel} from '../../contracts/amo-cf.model';

interface ResponseCatalogElements {
    response: {
        catalog_elements: AmoCatalogElementModel[];
        pagination: {
            pages: {
                current: number;
                page_size: number;
                total: number;
            };
            total: number;
            available_rows_count: number[];
            selected_rows_count_value: number;
        };
        errors: any[];
        server_time: number;
    };
}


@Module({
    name: 'AmoCatalogs',
    namespaced: true,
})
export default class AmoCatalogs extends VuexModule {

    protected $catalogs: AmoCatalogModel[] = [];
    protected $elements: { [key: number]: AmoCatalogElementModel[] } = {};


    @Mutation
    public setCatalogs(data: AmoCatalogModel[]) {
        let catalogs: AmoCatalogModel[] = [];
        for (let key in data) {
            if (!data.hasOwnProperty(key)) {
                continue;
            }

            catalogs.push(data[key]);
        }

        this.$catalogs = catalogs;
    }

    @Mutation
    public addCatalogElements({idCatalog, list}: { idCatalog: number; list: AmoCatalogElementModel[] }) {
        this.$elements[idCatalog] = list;
    }

    @Mutation
    public clearCatalogElements(idCatalog?: number) {
        if (idCatalog) {
            delete this.$elements[idCatalog];
            return;
        }

        this.$elements = {};
    }


    @Action
    public async loadCatalogs(): Promise<AmoCatalogModel[]> {
        if (this.$catalogs.length) {
            return this.$catalogs;
        }

        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost + CORE().amoPaths.catalogs,
                    {},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<any, AmoCatalogModel[]>(response => {
                        this.setCatalogs(response.response?.catalogs || {});

                        return this.$catalogs;
                    }),
                ),
        );
    }

    @Action
    public async loadCatalogElements(catalog_id: number): Promise<AmoCatalogElementModel[]> {
        if (this.$elements[catalog_id]?.length) {
            return this.$elements[catalog_id];
        }

        let page = 1;
        let first = await this.loadPaginateCatalogElements({catalog_id, page});

        let result: AmoCatalogElementModel[] = [];
        result.push(...first.response.catalog_elements);
        if (first.response.pagination.pages.total > page) {
            let list: ResponseCatalogElements[] = await Promise.all(
                [...Array(first.response.pagination.pages.total).keys()]
                    .slice(2)
                    .map(item => this.loadPaginateCatalogElements({catalog_id, page: item})),
            );

            result.push(
                ...list
                    .map(item => item.response.catalog_elements)
                    .reduce((acc, item) => [...acc, ...item], []),
            );
        }

        this.addCatalogElements({idCatalog: catalog_id, list: result});

        return this.$elements[catalog_id] || [];
    }

    @Action
    protected loadPaginateCatalogElements({catalog_id, page}: {catalog_id: number, page: number}): Promise<ResponseCatalogElements> {
        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost + CORE().amoPaths.catalogElements,
                    {catalog_id, json: 1, page},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<ResponseCatalogElements, ResponseCatalogElements>(response => {
                        return response;
                    }),
                ),
        );
    }


    public get catalogs(): AmoCatalogModel[] {
        return this.$catalogs;
    }

    public get cfFor(): (catalog: AmoCatalogModel) => AmoCfModel[] {
        let amoCf = getModule(AmoCf, $store);
        return catalog => amoCf.customFieldsFor(catalog.id);
    }

    public get elementsFor(): (catalogId: number) => AmoCatalogElementModel[] {
        return catalogId => (this.$elements[catalogId] || []);
    }
}
