import {VuexModule, Module, Mutation, Action} from 'vuex-module-decorators';
import {lastValueFrom, map} from 'rxjs';
import {VRIESEA_CORE as CORE} from '../../configs/core.config';
import {AmoTagModel} from '../../contracts/amo-tag.model';
import {MainVueApi} from '../../modules/classes/main-vue-api';


@Module({
    name: 'AmoTags',
    namespaced: true,
})
export default class AmoTags extends VuexModule {

    private $leadTags: AmoTagModel[] = [];
    private $customerTags: AmoTagModel[] = [];
    private $contactTags: AmoTagModel[] = [];


    @Mutation
    public setLeadTags(data: any) {
        let list: AmoTagModel[] = [];
        for (let key in data) {
            if (!data.hasOwnProperty(key)) {
                continue;
            }

            list.push(data[key]);
        }

        this.$leadTags = list;
    }

    @Mutation
    public setCustomerTags(data: any) {
        let list: AmoTagModel[] = [];
        for (let key in data) {
            if (!data.hasOwnProperty(key)) {
                continue;
            }

            list.push(data[key]);
        }

        this.$customerTags = list;
    }

    @Mutation
    public setContactTags(data: any) {
        let list: AmoTagModel[] = [];
        for (let key in data) {
            if (!data.hasOwnProperty(key)) {
                continue;
            }

            list.push(data[key]);
        }

        this.$contactTags = list;
    }


    @Action
    public async loadLeadTags(): Promise<AmoTagModel[]> {
        if (this.$leadTags.length) {
            return this.$leadTags;
        }

        return lastValueFrom(
            MainVueApi.context
                .get(MainVueApi.accountHost + CORE().amoPaths.leadTags, {limit: '-1'}, {
                    'X-Requested-With': 'XMLHttpRequest',
                })
                .pipe(
                    map<any, AmoTagModel[]>(result => {
                        if (result?._embedded?.items) {
                            this.setLeadTags(result._embedded.items);

                            return this.$leadTags;
                        }

                        return [];
                    }),
                ),
        );
    }

    @Action
    public async loadCustomerTags(): Promise<AmoTagModel[]> {
        if (this.$customerTags.length) {
            return this.$customerTags;
        }

        return lastValueFrom(
            MainVueApi.context
                .get(MainVueApi.accountHost + CORE().amoPaths.customerTags, {limit: '-1'}, {
                    'X-Requested-With': 'XMLHttpRequest',
                })
                .pipe(
                    map<any, AmoTagModel[]>(result => {
                        if (result?._embedded?.items) {
                            this.setCustomerTags(result._embedded.items);

                            return this.$customerTags;
                        }

                        return [];
                    }),
                ),
        );
    }

    @Action
    public async loadContactTags(): Promise<AmoTagModel[]> {
        if (this.$contactTags.length) {
            return this.$contactTags;
        }

        return lastValueFrom(
            MainVueApi.context
                .get(MainVueApi.accountHost + CORE().amoPaths.contactTags, {limit: '-1'}, {
                    'X-Requested-With': 'XMLHttpRequest',
                })
                .pipe(
                    map<any, AmoTagModel[]>(result => {
                        if (result?._embedded?.items) {
                            this.setContactTags(result._embedded.items);

                            return this.$contactTags;
                        }

                        return [];
                    }),
                ),
        );
    }

    @Action
    public async loadTags(): Promise<AmoTagModel[]> {
        return Promise.all([
            this.loadLeadTags(),
            this.loadCustomerTags(),
            this.loadContactTags(),
        ])
            .then(([leadT, customerT, contactT]) => [
                ...leadT,
                ...customerT,
                ...contactT,
            ]);
    }


    public get leadTags(): AmoTagModel[] {
        return this.$leadTags;
    }

    public get customerTags(): AmoTagModel[] {
        return this.$customerTags;
    }

    public get contactTags(): AmoTagModel[] {
        return this.$contactTags;
    }
}
