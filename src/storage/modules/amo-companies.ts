import {VuexModule, Module, Mutation, Action} from 'vuex-module-decorators';
import {lastValueFrom, map} from 'rxjs';
import {MainVueApi} from '../../modules/classes/main-vue-api';
import {VRIESEA_CORE as CORE} from '../../configs/core.config';
import {AmoCompanyV4Model} from '../../contracts/amo-company.model';
import {FindEntitiesQueryParams} from '../../contracts/amo-entity.model';
import {filterAmoEntitiesByQuery} from '../../helpers/amo-entities.helper';


@Module({
    name: 'AmoCompanies',
    namespaced: true,
})
export default class AmoCompanies extends VuexModule {

    @Action
    public async findByIdApiV4(id: number): Promise<AmoCompanyV4Model | null> {
        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost + CORE().amoPaths.companiesApiV4 + `/${id}`,
                    {},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<AmoCompanyV4Model, AmoCompanyV4Model>(response => {
                        return response || null;
                    }),
                ),
        );
    }

    @Action
    public async findByIdsApiV4(id: number[]): Promise<AmoCompanyV4Model[]> {
        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost + CORE().amoPaths.companiesApiV4,
                    {id},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<any, AmoCompanyV4Model[]>(response => {
                        return response?._embedded?.companies || [];
                    }),
                ),
        );
    }

    @Action
    public async findByQueryApiV4(data: FindEntitiesQueryParams<AmoCompanyV4Model>): Promise<AmoCompanyV4Model[]> {
        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost + CORE().amoPaths.companiesApiV4,
                    {query: data.query},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<any, AmoCompanyV4Model[]>(response => {
                        return filterAmoEntitiesByQuery(response?._embedded?.companies || [], data);
                    }),
                ),
        );
    }
}
