import {Module, Action} from 'vuex-module-decorators';
import {lastValueFrom, map} from 'rxjs';
import {MainVueApi} from '../../modules/classes/main-vue-api';
import {VRIESEA_CORE as CORE} from '../../configs/core.config';
import {AmoManagerModel, AmoUserModel, AmoUserRoleModel} from '../../contracts/amo-user.model';
import AmoUsers from './amo-users';


@Module({
    name: 'AmoUsersProxy',
    namespaced: true,
})
export default class AmoUsersProxy extends AmoUsers {

    @Action
    public async loadUsers(): Promise<AmoUserModel[]> {
        if (this.$users.length) {
            return this.$users;
        }

        return lastValueFrom(
            MainVueApi.context
                .get(
                    CORE().proxyAmoServer + CORE().amoPaths.users,
                    {limit: -1, page: 1, 'filter[active]': 'Y'},
                )
                .pipe(
                    map<any, AmoUserModel[]>(response => {
                        this.setUsers(response._embedded.items);

                        return this.$users;
                    }),
                ),
        );
    }

    @Action
    public async loadRoles(): Promise<AmoUserRoleModel[]> {
        if (this.$roles.length) {
            return this.$roles;
        }

        throw new Error('Not implement in Crimson proxy');
    }

    @Action
    public async loadManagersWithGroups(): Promise<AmoManagerModel[]> {
        if (this.$managers.length) {
            return this.$managers;
        }

        return lastValueFrom(
            MainVueApi.context
                .get(CORE().proxyAmoServer + CORE().amoPaths.managersWithGroups, {free_users: 'Y'})
                .pipe(
                    map<any, AmoManagerModel[]>(response => {
                        this.setManagers(response.managers);
                        this.setGroups(response.groups);

                        return this.$managers;
                    }),
                ),
        );
    }
}
