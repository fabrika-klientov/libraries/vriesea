import {VuexModule, Module, Action} from 'vuex-module-decorators';
import {lastValueFrom, map, zip} from 'rxjs';
import {VRIESEA_CORE as CORE} from '../../configs/core.config';
import {MainVueApi} from '../../modules/classes/main-vue-api';


@Module({
    name: 'TextLocales',
    namespaced: true,
})
export default class TextLocales extends VuexModule {

    public $lang: string = 'ru';
    public $texts: {[key: string]: string} = {
        info_description: '<p>Описание</p>',
    };

    @Action
    public initText(): Promise<any> {
        return lastValueFrom(
            zip(
                MainVueApi.context
                    .get(this.link('description'))
                    .pipe(
                        map<any, any>(result => {
                            this.$texts.info_description = result;
                            return true;
                        }),
                    ),
                ),
            );
    }

    @Action
    public messageError(type: string): Promise<any> {
        return lastValueFrom(MainVueApi.context.get(this.sharedLink('messages', type)));
    }

    public get link(): (type: string) => string {
        return (type: string) => CORE().textServer + CORE().linkDescription
            .replace('{{code}}', CORE().codeWidget)
            .replace('{{type}}', type)
            .replace('{{lang}}', this.$lang);
    }

    public get sharedLink(): (code: string, type: string) => string {
        return (code: string, type: string) => CORE().textServer + CORE().linkShared
            .replace('{{code}}', code)
            .replace('{{type}}', type)
            .replace('{{lang}}', this.$lang);
    }

    public get texts(): {[key: string]: string} {
        return this.$texts;
    }

    public get text(): (key: string) => string {
        return key => (this.$texts[key] || '');
    }
}
