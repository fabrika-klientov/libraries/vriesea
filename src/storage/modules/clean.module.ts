import {Action, VuexModule} from 'vuex-module-decorators';
import {Subject} from 'rxjs';
import {FullCleanContract} from '../../contracts/full-clean.contract';


export abstract class CleanModule extends VuexModule implements FullCleanContract {

    protected unsubscribeEvent: Subject<void> = new Subject(); // ex.: use as takeUntil


    @Action
    public fullClean(): void {
        this.unsubscribeEvent.next();
    }
}
