import {VuexModule, Module, Mutation, Action} from 'vuex-module-decorators';
import {from, lastValueFrom, map, Observable, of, switchMap} from 'rxjs';
import {MainVueApi} from '../../modules/classes/main-vue-api';
import {VRIESEA_CORE as CORE} from '../../configs/core.config';
import {
    AmoCfGroupEntities,
    AmoCfGroupGroups,
    AmoCfGroupModel,
    AmoCfModel,
    CFEnumModel,
    FactoryCF,
} from '../../contracts/amo-cf.model';

const cfRejectedId = ['budget', 'comp_name', 'cont_name', 'catalog_element_name'];


@Module({
    name: 'AmoCf',
    namespaced: true,
})
export default class AmoCf extends VuexModule {

    protected $cf: AmoCfGroupEntities = {
        leads: [],
        contacts: [],
        companies: [],
        customers: [],
        segments: [],
    };
    protected $groups: AmoCfGroupGroups = {
        leads: [],
        contacts: [],
        companies: [],
        customers: [],
        segments: [],
    };


    @Mutation
    public setCF(data: any) {
        let result: any = {};
        for (let key in data) {
            if (!data.hasOwnProperty(key)) {
                continue;
            }

            result[key] = [];
            for (let id in data[key]) {
                if (!data[key].hasOwnProperty(id)) {
                    continue;
                }

                let item: AmoCfModel = data[key][id];
                if (cfRejectedId.indexOf(`${item.id}`) !== -1) {
                    continue;
                }

                if (item.enums) {
                    let enums: CFEnumModel[] = [];
                    for (let j in item.enums) {
                        if (!item.enums.hasOwnProperty(j)) {
                            continue;
                        }
                        enums.push(item.enums[j]);
                    }

                    item.enums = enums;
                }

                item.element_code = key as keyof AmoCfGroupEntities;
                item.entityWithId = `${key}_${id}`;

                result[key].push(item);
            }
        }

        this.$cf = result;
    }

    @Mutation
    public setGroups(data: any) {
        let result: any = {};
        for (let key in data) {
            if (!data.hasOwnProperty(key)) {
                continue;
            }

            result[key] = [];
            if (!data[key]?.length) {
                continue;
            }

            result[key] = data[key].map(item => {
                switch (item.id) {
                    case 'default':
                        item.name = 'Основное';
                        break;
                    case 'statistic':
                        item.name = 'Статистика';
                }

                // amo bag if c.field deleted or moved
                if (item.fields && !item.fields.length) {
                    const fields: any[] = [];
                    for (let index in item.fields) {
                        if (!item.fields.hasOwnProperty(index)) {
                            continue;
                        }
                        fields.push(item.fields[index]);
                    }
                    item.fields = fields;
                }

                item.element_code = key as keyof AmoCfGroupEntities;
                item.entityWithId = `${key}_${item.id}`;

                return item;
            });
        }

        this.$groups = result;
    }


    @Action
    public async loadCF(force: boolean = false): Promise<AmoCfGroupEntities> {
        if (!force && (this.$cf.leads.length || this.$cf.contacts.length || this.$cf.companies.length)) {
            return this.$cf;
        }

        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost + CORE().amoPaths.linkCustomFields,
                    {},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<any, AmoCfGroupEntities>(response => {
                        this.setCF(response.response.params.fields);
                        this.setGroups(response.response.params.groups);

                        return this.$cf;
                    }),
                )
        );
    }

    @Action
    public async storeCF(factory: FactoryCF): Promise<AmoCfModel[]> {
        if (!factory.list.length) {
            return [];
        }

        return lastValueFrom(
            MainVueApi.context
                .post(
                    MainVueApi.accountHost + CORE().amoPaths.v4Cf.replace('{entity}', factory.entity as string),
                    factory.list,
                )
                .pipe(
                    switchMap<any, Observable<AmoCfModel[]>>(response => {
                        let stored = response._embedded?.custom_fields || [];
                        if (!stored.length) {
                            return of([]);
                        }

                        return from(this.loadCF(true))
                            .pipe(
                                map(
                                    () => this.customFieldsFor(factory.entity).filter(item => stored.some(one => one.id === item.id))
                                ),
                            );
                    }),
                )
        );
    }


    public get customFields(): AmoCfGroupEntities {
        return this.$cf;
    }

    public get customFieldsFor(): (entity: keyof AmoCfGroupEntities, typeId?: number) => AmoCfModel[] {
        return (entity: keyof AmoCfGroupEntities, typeId?: number) => {
            let list = this.$cf[entity];
            if (typeId && list) {
                return list.filter(item => item.type_id === typeId);
            }

            return list;
        };
    }

    public get groups(): AmoCfGroupGroups {
        return this.$groups;
    }

    public get groupsFor(): (entity: keyof AmoCfGroupEntities) => AmoCfGroupModel[] {
        return (entity: keyof AmoCfGroupEntities) => (this.$groups[entity] || []);
    }

    public get customFieldsForEntityGroup(): (entity: keyof AmoCfGroupEntities, group: AmoCfGroupModel) => AmoCfModel[] {
        return (entity: keyof AmoCfGroupEntities, group: AmoCfGroupModel) => {
            let cfList = this.customFieldsFor(entity);
            if (!cfList?.length) {
                return [];
            }

            return cfList.filter(item => group.fields.some(one => +one === +item.id));
        };
    }
}
