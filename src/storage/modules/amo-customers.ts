import {VuexModule, Module, Action} from 'vuex-module-decorators';
import {lastValueFrom, map} from 'rxjs';
import {MainVueApi} from '../../modules/classes/main-vue-api';
import {VRIESEA_CORE as CORE} from '../../configs/core.config';
import {FindEntitiesQueryParams} from '../../contracts/amo-entity.model';
import {filterAmoEntitiesByQuery} from '../../helpers/amo-entities.helper';
import {AmoCustomerAjaxV2Model, AmoCustomerV4Model} from '../../contracts/amo-customer.model';


@Module({
    name: 'AmoCustomers',
    namespaced: true,
})
export default class AmoCustomers extends VuexModule {

    @Action
    public async findById(id: number): Promise<AmoCustomerAjaxV2Model | null> {
        return lastValueFrom(
            MainVueApi.context
                .post(
                    MainVueApi.accountHost + CORE().amoPaths.customersAjaxV2,
                    {useFilter: 'Y', filter: {id}},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<any, AmoCustomerAjaxV2Model>(response => {
                        return response?.items?.[0] || null;
                    }),
                ),
        );
    }

    @Action
    public async findByIdApiV4(id: number): Promise<AmoCustomerV4Model | null> {
        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost + CORE().amoPaths.customersApiV4 + `/${id}`,
                    {},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<AmoCustomerV4Model, AmoCustomerV4Model>(response => {
                        return response || null;
                    }),
                ),
        );
    }

    @Action
    public async findByIdsApiV4(id: number[]): Promise<AmoCustomerV4Model[]> {
        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost + CORE().amoPaths.customersApiV4,
                    {id},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<any, AmoCustomerV4Model[]>(response => {
                        return response?._embedded?.contacts || [];
                    }),
                ),
        );
    }

    @Action
    public async findByQueryApiV4(data: FindEntitiesQueryParams<AmoCustomerV4Model>): Promise<AmoCustomerV4Model[]> {
        return lastValueFrom(
            MainVueApi.context
                .get(
                    MainVueApi.accountHost + CORE().amoPaths.contactsApiV4,
                    {query: data.query},
                    {'X-Requested-With': 'XMLHttpRequest'},
                )
                .pipe(
                    map<any, AmoCustomerV4Model[]>(response => {
                        return filterAmoEntitiesByQuery(response?._embedded?.contacts || [], data);
                    }),
                ),
        );
    }
}
