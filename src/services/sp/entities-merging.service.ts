import {Observable, Subject, takeUntil} from 'rxjs';
import {LifeServiceContract} from '../../contracts/life-service.contract';
import {AmoEntity} from '../../contracts/amo-instances.types';
import {entityConvert, getCardEntityName} from '../../helpers/amo-instance.helper';
import {Widget} from '../../modules/classes/widget';
import {getAPP} from "../../configs/core.config";

export interface EntitiesMergeEvent {
    type: 'merge';
    entity: AmoEntity;
    listMerging: number[];
    resultId: number;
    resultName: string;
}

export interface EntitiesUnmergeEvent {
    type: 'unmerge';
    entity: AmoEntity;
    resultId: number;
    resultName: string;
}


export class EntitiesMergingService implements LifeServiceContract {

    private $merge: Subject<EntitiesMergeEvent> = new Subject();
    private $unmerge: Subject<EntitiesUnmergeEvent> = new Subject();

    protected unsubscribeEvent: Subject<void> = new Subject();

    protected mergeListener = () => {
        let modal = document.querySelector('.modal-entity-merge');
        if (!modal) {
            return;
        }

        let form = modal.querySelector('.merge-form__container form');
        if (!form) {
            return;
        }

        let listMerging = Array.from(form.querySelectorAll<HTMLInputElement>('input[name="id[]"]'))
            .map(item => +item.value);

        let resultId = Array.from(form.querySelectorAll<HTMLInputElement>('input[name="result_element[NAME]"]'))
            .filter(item => item.checked)
            .map(item => item.closest<HTMLInputElement>('div[data-field-code="NAME"]')!)
            .filter(item => item)
            .map(item => item.getAttribute('data-contact-id')!)
            .map(item => +item)
            .shift();

        let resultName = form.querySelector<HTMLSpanElement>('.merge-form__table-headcol .form-result[data-for="NAME"] span')?.innerText;

        let entity = entityConvert(Widget.context.area);
        entity = entity === 'contacts' ? (modal.classList.contains('modal-contacts-merge') ? entity : 'companies') : entity;

        if (listMerging.length && resultId && entity && resultName) {
            this.$merge.next({
                type: 'merge',
                entity,
                listMerging,
                resultId,
                resultName,
            });
        }
    };

    protected unmergeListener = () => {
        let saveBtn = document.querySelector('.modal-body .modal-body__actions .modal-body__actions__save');
        if (!saveBtn) {
            return;
        }

        saveBtn.addEventListener('click', () => {
            let entity = entityConvert(Widget.context.area);
            let resultId = +getAPP().data.current_card.id;

            if (resultId && entity) {
                this.$unmerge.next({
                    type: 'unmerge',
                    entity,
                    resultId,
                    resultName: getCardEntityName(),
                });
            }
        });
    };

    constructor() {
    }

    public async down(): Promise<void> {
        this.unsubscribeEvent.next();

        let jBody = $('body');
        jBody.off('click', '.modal-entity-merge .modal-body__actions__save', this.mergeListener);
        jBody.off('click', '.feed-note-wrapper-merge .feed-note__action-button', this.unmergeListener);
    }

    public async up(): Promise<void> {
        this.doMergeListen();
        this.doUnmergeListen();
    }

    public mergeObserver(contextual: boolean = true): Observable<EntitiesMergeEvent> {
        if (contextual) {
            return this.getObservable<EntitiesMergeEvent>(this.$merge);
        }

        return this.$merge.asObservable();
    }

    public unmergeObserver(contextual: boolean = true): Observable<EntitiesUnmergeEvent> {
        if (contextual) {
            return this.getObservable<EntitiesUnmergeEvent>(this.$unmerge);
        }

        return this.$unmerge.asObservable();
    }

    private doMergeListen() {
        $('body').on('click', '.modal-entity-merge .modal-body__actions__save', this.mergeListener);
    };

    private doUnmergeListen() {
        $('body').on('click', '.feed-note-wrapper-merge .feed-note__action-button', this.unmergeListener);
    }

    private getObservable<T>(subject: Subject<T>): Observable<T> {
        return subject
            .asObservable()
            .pipe(
                takeUntil(this.unsubscribeEvent),
            );
    }
}
