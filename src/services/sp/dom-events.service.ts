import {LifeServiceContract} from '../../contracts/life-service.contract';
import {VRIESEA_CORE} from '../../configs/core.config';

const MAP_EVENT = {
    BODY_CLICK: 'body.click',
    RIGHT_WIDGET_HEAD_CLICK: 'right_widget_head.click',
};


export class DomEventsService implements LifeServiceContract {
    private $singletons: Map<string, ((event?: Event, additional?: any) => any)[]> = new Map();
    private $contexts: Map<string, ((event?: Event, additional?: any) => any)[]> = new Map();

    protected bodyClickSingletonListener = ev => this.work(MAP_EVENT.BODY_CLICK, ev, null, true);
    protected bodyClickContextListener = ev => this.work(MAP_EVENT.BODY_CLICK, ev);

    protected rightWidgetHeadClickContextListener = ev => {
        let arrow = ev.currentTarget.querySelector('.card-widgets__widget__caption__arrow');
        setTimeout(() => {
            if (arrow) {
                this.work(
                    MAP_EVENT.RIGHT_WIDGET_HEAD_CLICK,
                    ev,
                    {expanded: arrow.classList.contains('widgets__widget__caption__arrow_top')},
                );
                return;
            }

            this.work(MAP_EVENT.RIGHT_WIDGET_HEAD_CLICK, ev);
        });
    };


    constructor() {
        this.initialize();
    }

    protected initialize() {
        this.singletonListeners();
    }

    public async down(): Promise<void> {
        this.removeContextualListeners();
        this.$contexts.clear();
    }

    public async up(): Promise<void> {
        this.contextualListeners();
    }


    protected singletonListeners() {
        document.body.addEventListener('click', this.bodyClickSingletonListener, false);
    }

    protected contextualListeners() {
        document.body.addEventListener('click', this.bodyClickContextListener, false);

        let headOfRightWidget = document.querySelector(`.head-${VRIESEA_CORE().codeWidget}`);
        if (headOfRightWidget) {
            headOfRightWidget.addEventListener('click', this.rightWidgetHeadClickContextListener, false);
        }
    }

    protected removeSingletonListeners() {
        document.body.removeEventListener('click', this.bodyClickSingletonListener, false);
    }

    protected removeContextualListeners() {
        document.body.removeEventListener('click', this.bodyClickContextListener, false);

        let headOfRightWidget = document.querySelector(`.head-${VRIESEA_CORE().codeWidget}`);
        if (headOfRightWidget) {
            headOfRightWidget.removeEventListener('click', this.rightWidgetHeadClickContextListener, false);
        }
    }


    public addClickBodyListener(callback: (event?: Event) => any, singleton: boolean = false) {
        this.registerListener(MAP_EVENT.BODY_CLICK, callback, singleton);
    }

    public removeClickBodyListener(callback: (event?: Event) => any, singleton: boolean = false) {
        this.unregisterListener(MAP_EVENT.BODY_CLICK, callback, singleton);
    }

    public addClickHeadWidgetRightListener(callback: (event?: Event, additional?: {expanded: boolean}) => any) {
        this.registerListener(MAP_EVENT.RIGHT_WIDGET_HEAD_CLICK, callback, false);
    }

    public removeClickHeadWidgetRightListener(callback: (event?: Event, additional?: {expanded: boolean}) => any) {
        this.unregisterListener(MAP_EVENT.RIGHT_WIDGET_HEAD_CLICK, callback, false);
    }


    protected registerListener(code: string, callback: (event?: Event, additional?: any) => any, singleton: boolean) {
        let map = this.getMap(singleton);

        if (!map.has(code)) {
            map.set(code, []);
        }

        map.get(code)!.push(callback);
    }

    protected unregisterListener(code: string, callback: (event?: Event, additional?: any) => any, singleton: boolean) {
        let map = this.getMap(singleton);

        if (!map.has(code)) {
            return;
        }

        let list = map.get(code)!;
        let index = list.indexOf(callback);
        if (index !== -1) {
            list.splice(index, 1);
        }
    }

    private work(code: string, ev: Event, additional: any = null, singleton: boolean = false) {
        let map = this.getMap(singleton);
        if (map.has(code)) {
            map.get(code)!.forEach(callback => callback(ev, additional));
        }
    }

    private getMap(singleton: boolean): Map<string, ((event?: Event, additional?: any) => any)[]> {
        switch (singleton) {
            case true:
                return this.$singletons;
            case false:
            default:
                return this.$contexts;
        }
    }
}
