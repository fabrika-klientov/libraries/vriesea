import {LifeServiceContract} from '../../contracts/life-service.contract';
import {getAPP} from "../../configs/core.config";


export class AmoUploadFilesService implements LifeServiceContract {
    private _fnWarning: (title: string, description: string) => void = () => {};
    private _fnError: (title: string, description: string) => void = () => {};

    constructor() {
    }

    public async down(): Promise<void> {
    }

    public async up(): Promise<void> {
    }

    public set fnWarning(value: (title: string, description: string) => void) {
        this._fnWarning = value;
    }

    public set fnError(value: (title: string, description: string) => void) {
        this._fnError = value;
    }

    public async push<T>(
        chunks: T[][],
        fnLoadFile: (file: T) => Promise<any>,
        fnResolveName: (file: T) => string,
        type: 'sendMail' | 'sendChat' = 'sendMail',
    ) {
        if (!chunks.length) {
            this._fnWarning('Внимание', 'Сначала выберите один или несколько файлов');
            return;
        }

        let switcher: HTMLElement;
        switch (type) {
            case 'sendMail':
                switcher = document.querySelector('.js-switcher-email:not(.hidden)') as HTMLElement;
                break;
            default:
            case 'sendChat':
                switcher = document.querySelector('.js-switcher-chat:not(.hidden)') as HTMLElement;
        }

        if (!switcher) {
            this._fnWarning('Внимание', 'Отсутствуют или не выбраны каналы для отправки файлов');
            return;
        }

        try {
            for (let chunk of chunks) {
                await this.executeFiles<T>(chunk, switcher, fnLoadFile, fnResolveName);
            }
        } catch (e: any) {
            this._fnError('Ошибка', e.message);
        }
    }

    protected async executeFiles<T>(
        files: T[],
        switcher: HTMLElement,
        fnLoadFile: (file: T) => Promise<any>,
        fnResolveName: (file: T) => string,
    ) {
        let results = await Promise.all(files.map(fnLoadFile));

        let feed = document.querySelector('.notes-wrapper')?.querySelector('.feed-compose');
        if (feed?.classList.contains('minimized')) {
            switcher.click();
        }

        this.executeToEmail(
            results
                .map((blob, index) => ({blob, file: files[index]}))
                .filter(({blob, file}) => {
                    if (blob) {
                        blob.name = fnResolveName(file);
                        return true;
                    }
                    this._fnError('Ошибка', `Не удалось загрузить файл ${fnResolveName(file)} с Google drive`);

                    return false;
                }),
        );
    }

    protected executeToEmail(data: any[]) {
        if (!data.length) {
            return;
        }

        let active_view: any = getAPP()?.data?.card_page?.notes?._compose?._active_view;
        if (!active_view?.file_uploader) {
            throw new Error('Не удалось прикрепить файлы');
        }

        let attachments: any = {};
        if (active_view._attachments && typeof active_view._attachments === 'object') {
            for (let key in active_view._attachments) {
                if (active_view._attachments.hasOwnProperty(key) &&
                    active_view._attachments[key].name &&
                    data.some(file => active_view._attachments[key].name != file.blob.name)) {
                    attachments[key] = active_view._attachments[key];
                }
            }
        }
        active_view._attachments = attachments;

        let files_queue: any[] = [];
        if (active_view.file_uploader._files_queue && Array.isArray(active_view.file_uploader._files_queue)) {
            active_view.file_uploader._files_queue.forEach(item => {
                if (item.name && data.some(file => item.name != file.blob.name)) {
                    files_queue.push(item);
                }
            });

            active_view.file_uploader._files_queue = files_queue;
        }

        if (!active_view.file_uploader.onFileApiElDrop) {
            throw new Error('Не удалось прикрепить файлы');
        }

        active_view.file_uploader.onFileApiElDrop(data.map(item => item.blob));
    }
}
