import {Observable, Subject, takeUntil, throwError} from 'rxjs';
import Echo from 'laravel-echo';
import {Channel, PresenceChannel} from 'laravel-echo/dist/channel';
import clientSocketIO from 'socket.io-client';
import clientPusher from 'pusher-js';
import {LifeServiceContract} from '../../contracts/life-service.contract';
import {VRIESEA_CORE as CORE} from '../../configs/core.config';

export type channelType = 'presence' | 'private' | 'public';


export class PusherService implements LifeServiceContract {

    private readonly $pusher: Echo;

    private $contextChannels: string[] = [];

    private $globalSubjects: Map<string, Subject<any>[]> = new Map();
    private $contextSubjects: Map<string, Subject<any>[]> = new Map();

    protected unsubscribeEvent: Subject<void> = new Subject();
    protected unsubscribeContextEvent: Subject<void> = new Subject();

    constructor() {
        let config = CORE().pusher;
        let authEndpoint = `${CORE().server.replace(/\/?$/, '')}/broadcasting/auth`;

        let client: any = null;
        switch (config.broadcaster) {
            case 'pusher':
                client = new clientPusher(config.key, {...config, authEndpoint});
                break;
            case 'socket.io':
                client = clientSocketIO;
        }

        this.$pusher = new Echo({client, authEndpoint, ...config});
    }

    public async down(): Promise<void> {
        this.$contextChannels.forEach(item => this.$pusher.leave(item));
        this.$contextSubjects.forEach(item => item.forEach(one => one.unsubscribe()));
        this.unsubscribeContextEvent.next();
        this.$contextChannels = [];
        this.$contextSubjects.clear();
    }

    public async up(): Promise<void> {
    }

    public async fullDown(): Promise<void> {
        await this.down();
        this.$globalSubjects.forEach(item => item.forEach(one => one.unsubscribe()));
        this.unsubscribeEvent.next();
        this.$globalSubjects.clear();
    }

    public get pusher(): Echo {
        return this.$pusher;
    }

    public channel(channel: string, type: channelType = 'public'): Channel | PresenceChannel | null {
        switch (type) {
            case 'public':
                return this.$pusher.channel(channel);
            case 'presence':
                return this.$pusher.join(channel);
            case 'private':
                return this.$pusher.private(channel);
            default:
                return null;
        }
    }

    public leaveChannel(channel): void {
        this.$pusher.leave(channel);
    }

    public listen<T>(channel: string, event: string, type: channelType = 'public'): Observable<T> {
        return this._listen(channel, event, type, false);
    }

    public contextChannel(channel: string, type: channelType = 'public'): Channel | PresenceChannel | null {
        let regChannel = this.channel(channel, type);
        if (!regChannel) {
            return null;
        }

        if (!this.$contextChannels.includes(channel)) {
            this.$contextChannels.push(channel);
        }

        return regChannel;
    }

    public contextListen<T>(channel: string, event: string, type: channelType = 'public'): Observable<T> {
        return this._listen(channel, event, type, true);
    }

    private _listen<T>(channel: string, event: string, type: channelType, context: boolean): Observable<T> {
        let regChannel = this.channel(channel, type);
        if (!regChannel) {
            return throwError(() => new Error(`The channel [${channel}] can\'t register.`));
        }

        switch (type) {
            case 'public':
            case 'presence':
            case 'private':
                let subject: Subject<T> = new Subject();
                PusherService.addToSubjectsList(
                    context ? this.$contextSubjects : this.$globalSubjects,
                    `${channel}::${event}`,
                    subject,
                );

                let cb = data => {
                    if (subject.closed) {
                        (regChannel as Channel).stopListening(event, cb);
                        return;
                    }

                    subject.next(data);
                };
                (regChannel as Channel).listen(event, cb);

                return this.getObservable<T>(subject, context);
            default:
                return throwError(() => new Error(`The type of channel [${type}] can\'t resolve.`));
        }
    }

    private getObservable<T>(subject: Subject<T>, context: boolean): Observable<T> {
        return subject
            .asObservable()
            .pipe(
                takeUntil(context ? this.unsubscribeContextEvent : this.unsubscribeEvent),
            );
    }

    protected static addToSubjectsList(list: Map<string, Subject<any>[]>, key: string, subject: Subject<any>): void {
        if (!list.has(key)) {
            list.set(key, []);
        }

        list.get(key)!.push(subject);
    }
}
