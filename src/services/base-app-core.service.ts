import Vue from 'vue';
import {Widget} from '../modules/classes/widget';
import {cleanStore} from '../storage/store';
import {ServiceProvider} from '../modules/classes/service-provider';
import {BaseAppCoreStatusesService} from './base-app-core-statuses.service';


export abstract class BaseAppCoreService extends BaseAppCoreStatusesService {

    protected renderTypeHook: string = 'render';
    protected accessTypeHooks: string[] = ['render'];
    protected renderDeepWorkflowTypeHook: string | null = null;
    protected accessDeepWorkflowTypeHooks: string[] = [];

    public init() {
        this.clean();
        this.checkStatus();
    }

    protected async successStatus() {
        await this.loadAccess();

        Widget.$hooks.subscribe(this._init.bind(this));
        this._init({type: this.renderTypeHook});

        if (this.renderDeepWorkflowTypeHook) {
            Widget.$hooks.subscribe(this._initDeepWorkflow.bind(this));
            this._initDeepWorkflow({type: this.renderDeepWorkflowTypeHook});
        }

        this._instanceSuccessNext();
    }

    protected errorStatus(message: string) {
        Widget.$hooks.subscribe(this._initDeny.bind(this));
        this._initDeny({type: this.renderTypeHook});

        if (this.renderDeepWorkflowTypeHook) {
            Widget.$hooks.subscribe(this._initDenyDeepWorkflow.bind(this));
            this._initDenyDeepWorkflow({type: this.renderDeepWorkflowTypeHook});
        }

        this._instanceErrorNext();
    }

    protected _init(event: {type: string} | null = null) {
        if (!this.accessTypeHooks.includes(event?.type!)) {
            return;
        }

        this.deepClean();

        this._initNext();
    }

    protected _initDeny(event: {type: string} | null = null) {
        if (!this.accessTypeHooks.includes(event?.type!)) {
            return;
        }

        this._initDenyNext();
    }

    protected _initDeepWorkflow(event: {type: string} | null = null) {
        if (!this.accessDeepWorkflowTypeHooks.includes(event?.type!)) {
            return;
        }

        this.deepWorkflowClean();

        this._initDeepWorkflowNext();
    }

    protected _initDenyDeepWorkflow(event: {type: string} | null = null) {
        if (!this.accessDeepWorkflowTypeHooks.includes(event?.type!)) {
            return;
        }

        this._initDenyDeepWorkflowNext();
    }


    /**
     * Initialize once >> only for success status
    * */
    protected _instanceSuccessNext() {
        // TODO: override
    }

    /**
     * Initialize once >> only for error status
     * */
    protected _instanceErrorNext() {
        // TODO: override
    }

    /**
     * For main flow
     * Initialized (every times) if call hook includes in the `accessTypeHooks` >> only for success status
     * */
    protected _initNext() {
        // TODO: override
    }

    /**
     * For main flow
     * Initialized (every times) if call hook includes in the `accessTypeHooks` >> only for error status
     * */
    protected _initDenyNext() {
        // TODO: override
    }

    /**
     * For deep flow
     * Initialized (every times) if call hook includes in the `accessDeepWorkflowTypeHooks` >> only for success status
     * */
    protected _initDeepWorkflowNext() {
        // TODO: override
    }

    /**
     * For deep flow
     * Initialized (every times) if call hook includes in the `accessDeepWorkflowTypeHooks` >> only for error status
     * */
    protected _initDenyDeepWorkflowNext() {
        // TODO: override
    }

    /**
     * Async top loaders before ready all flows
     * */
    protected abstract loadAccess(): Promise<void>;

    /**
     * Top level clean
     * */
    protected clean() {
    }

    /**
     * For main flow
     * Initialized (every times) before `_initNext` method >> only for success status
     * */
    protected deepClean() {
        cleanStore();
        let sp = (Vue.prototype.$vrieseaProvider as ServiceProvider);
        if (sp) {
            sp.up();
        }
    }

    /**
     * For deep flow
     * Initialized (every times) before `_initDeepWorkflowNext` method >> only for success status
     * */
    protected deepWorkflowClean() {
        // TODO: override if need
    }
}
