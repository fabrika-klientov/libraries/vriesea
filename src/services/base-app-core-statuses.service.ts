import {getModule} from 'vuex-module-decorators';
import {VRIESEA_CORE as CORE} from '../configs/core.config';
import AppStatus from '../storage/modules/app-status';
import {$store} from '../storage/store';


export abstract class BaseAppCoreStatusesService {

    protected appStatus = getModule(AppStatus, $store);

    protected async checkStatus() {
        await this.beforeCheckStatus();

        let status = await this.appStatus.loadStatus();
        if (status.type === 'error') {
            let message = await this.appStatus.messageError(status.code);
            console.warn(CORE().codeLogging + message);

            this.errorStatus(message);
            return;
        }

        this.successStatus();
    }

    protected successStatus() {
    }

    protected errorStatus(message: string) {
    }

    protected async beforeCheckStatus() {
    }
}
