import Vue, { ComponentOptions } from 'vue';
import {Widget} from '../dist/modules/classes/widget';
import {ServiceProvider} from '../dist/modules/classes/service-provider';
import {HttpClientContract} from '../dist/contracts/http-client.contract';

declare module 'vue/types/options' {
    interface ComponentOptions<V extends Vue> {
        style?: any;
    }
}

declare module 'vue/types/vue' {
    interface Vue {
        $httpClient: HttpClientContract;
        $vrieseaWidget: Widget;
        $vrieseaProvider: ServiceProvider;
    }
}
