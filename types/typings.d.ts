declare const $: any;

declare const AMOCRM: any; // @deprecated
declare const APP: any;

declare function requirejs(src: string[], callback: any): any;

declare type ConstructorOf<C> = {
    new (...args: any[]): C;
};
